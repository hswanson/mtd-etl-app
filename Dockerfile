#Base Container Image (start from existing image, ubuntu for example, and add needed components to run application)
FROM python:3.9.18-bookworm

#RUN executes a command, useradd creates a new user, default is root but not good practice to run application as root
RUN useradd -ms /bin/bash etlmodule

#Default directory where the application will be installed, any new commands get applied here
WORKDIR /home/etlmodule

#----------INSTALL ALL DEPENDENCIES, CREATE VIRTUAL ENV, INSTALL WEB SERVER---------------#
#Transfers files from your machine to the container file system: COPY source destination
COPY requirements.txt requirements.txt

#gets the db seed
COPY BASE_DB_DATA.json BASE_DB_DATA.json 

#Create a virtual environment
RUN python -m venv venv
#install requirements in virtual environment
RUN venv/bin/pip install -r requirements.txt

#install webserver
RUN venv/bin/pip install gunicorn
#-----------------------------------------------------------------------------------------#

#-----------TRANSFER ALL IMPORTANT FILES-------------#
#Transfer application folder
COPY application application
#Transfer db migrations directory
COPY migrations migrations

COPY etlup etlup
COPY MTDdb MTDdb
#Transfer main app file and config with shell script
COPY mtd_etl_server.py config.py mtddb.py etldb.py boot.sh ./
#chmod for windows, to set executable of boot shell script
RUN chmod +x boot.sh
#----------------------------------------------------#

#Set environment variable to use the flask command
ENV FLASK_APP mtd_etl_server.py

#Sets teh owner of all the directories and files in /home/etlmodules to be etlmodules
RUN chown -R etlmodule:etlmodule ./
#makes this user by default for any more instructions when the container is started
USER etlmodule

#Configures the port that this container will be using for the web server
EXPOSE 5000
#default command that should be executed when the container is started
ENTRYPOINT ["./boot.sh"]

# >>>>>>>>Basic stating boot shell script for reference<<<<<<<<<<
#!/bin/bash
# source venv/bin/activate
# exec gunicorn -b :5000 --access-logfile - --error-logfile - microblog:app