#!/bin/bash
source venv/bin/activate
#run migration script!
flask db upgrade
exec gunicorn -b :5000 --access-logfile - --error-logfile - mtd_etl_server:app

#b option tells gunicorn to listen to requests at port 5000
#mtd_etl_server:app tells gunicorn to look in the "mtd_etl_server" module (or file) and find the variable named "app"