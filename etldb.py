#!/usr/bin/env python
import click
from application import app, db
from application import db_models as mdl
from application.construction.upload import ConstrUploader, gen_uuid
from etlup.src.etlup.construction.adapters import ConstrModel, ConstrArrModel
from datetime import datetime, timezone
from config import the_config
import json
from io import BytesIO
import os
import random
import numpy as np
import string
from typing import Union, List
from functools import partial
import psycopg2.errors as dbError
from tqdm import tqdm

app.app_context().push()

def now_utc() -> datetime:
    return datetime.now(timezone.utc)

def clear_cloud_files():
    for file in mdl.File.query:
        file.delete_remote()
    #causes wierd infinite loop?
    db.session.close()

def add_base_db_data():
    user = mdl.User.get_by_username('root')
    with open('BASE_DB_DATA.json', 'r') as f:  # Replace with the path to your JSON file
        data = json.load(f)

    for comp_type in data['component_type']:
        
        component_type = mdl.ComponentType(
            name_long=comp_type['name_long'], 
            name_abbv=comp_type['name_abbv'], 
            required_keys=comp_type['required_keys'], 
            description=comp_type['description'],
            mtddb_zz_int = comp_type.get('mtddb_zz_int'),
            user_created=user)
        db.session.add(component_type)

    for mod_type in data['module_type']:
        module_type = mdl.ModuleType(
            name_long=mod_type['name_long'], 
            name_abbv=mod_type['name_abbv'], 
            number_of_components=mod_type['number_of_components'],
            description=mod_type['description'],
            mtddb_zz_int = mod_type.get('mtddb_zz_int'),
            user_created=user
        )
        db.session.add(module_type)
    for loc in data['location']:
        location = mdl.Location(
            name_long=loc['name_long'], 
            name_abbv=loc['name_abbv'], 
            description=loc['description'],
        )
        db.session.add(location)
    db.session.commit()

def check_size(test_data):
    file_bytes = BytesIO(json.dumps(test_data, indent=4).encode('utf-8'))
    file_length = file_bytes.seek(0, os.SEEK_END)
    file_bytes.seek(0, os.SEEK_SET)
    return file_length

def make_rand_arr(nbytes):
    """random array nbytes long of numbers between 0 and 7"""
    return list(bytearray( map(random.getrandbits, (3,)*(nbytes//7)) ))

def make_rand_str(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def make_subassemblies(n_subs: int):
    subassemblies = []
    #get the component types

    fake_sub_type = mdl.ComponentType.get_by_either('Fake Subassembly')
    fake_lgad_type = mdl.ComponentType.get_by_either('Fake LGAD')
    fake_etroc_type = mdl.ComponentType.get_by_either('Fake ETROC')
    print(f"Creating {n_subs} Subassemblies, ETROCs, and LGADs...")
    for i in tqdm(range(n_subs)):
        subassy = mdl.Component(
            serial_number = "FAKE_SUBASSEMBLY"+str(i).zfill(4),
            component_type = fake_sub_type,
            vendor = 'me',
            user_created_id = 1,
            meta_data = {'Bond': make_rand_str(size=100)}
        )
        db.session.add(subassy)

        lgad = mdl.Component(
            serial_number = "FAKE_LGAD"+str(i).zfill(4),
            component_type = fake_lgad_type,
            vendor = 'me',
            user_created_id = 1,
            meta_data = {'Geometry': f"{random.randint(1, 16)}x{random.randint(1, 16)}"}
        )
        lgad.parent = subassy
        db.session.add(lgad)

        etroc = mdl.Component(
            serial_number = "FAKE_ETROC"+str(i).zfill(4),
            component_type = fake_etroc_type,
            vendor = 'me',
            user_created_id = 1,
            meta_data = {'Geometry': f"{random.randint(1, 16)}x{random.randint(1, 16)}"}
        )
        etroc.parent = subassy
        db.session.add(etroc)

        subassemblies.append(subassy)

    db.session.commit()
    print('')
    return subassemblies

def rand_comp_test(component, test_size):
   return {
        "component": component.serial_number,
        "type": f"Fake ETL Test {str(random.randint(1,5))}",
        "measurement_date": "2023-01-01T12:00:00+01:00",
        "location": "BU",
        "user_created": "hswanson",
        "version": "0.0",
        "data": {
            "a_silly_integer": random.randint(0,999),
            "a_silly_string": make_rand_str(size=12), 
            "a_silly_array": make_rand_arr(test_size)
        }
    }

def rand_mod_test(module, component, test_size: int):
    return {
        "module": module.serial_number,
        "component": component.serial_number,
        "component_pos": component.component_pos, 
        "type": f"Fake ETL Test {str(random.randint(6,10))}",
        "measurement_date": "2023-01-01T12:00:00+01:00",
        "location": "BU",
        "user_created": "hswanson",
        "version": "0.0",
        "data": {
            "crazy_name": make_rand_str(size=12),
            "crazy_array": make_rand_arr(test_size//2), 
            "another_crazy_array": make_rand_arr(test_size//2),
        }
    }

def constr_chunk_upload(all_constr_generators: list, test_data_size: int, max_file_length: int=5*1024*1024, put_data_in_cache:bool=True) -> None: #MB
    """
    Uploads constructions but splits up the upload so that they only get uploaded in "max_file_length" chunks
    """     
    # uploader objects
    def calc_file_apprx_file_size(test_size, n_tests):
        #replaces this slow way to calculate
        # def calc_file_length(constrs):
        #     #mimic the output look alike so {uuid: cloud_data}
        #     _temp = {gen_uuid(): constr.data.model_dump() for constr in constrs}
        #     file_bytes = BytesIO(json.dumps(_temp, indent=4).encode('utf-8'))
        #     file_length = file_bytes.seek(0, os.SEEK_END)
        #     file_bytes.seek(0, os.SEEK_SET) 
        #     return file_length   

        #calculated by doing some testing and fits... ONLY FOR CURR FAKE data
        return 2.143*test_size*n_tests+158
    
    constr_uploaders = []
    all_db_objs = []
    # going to need to loop through constrs and group by size
    constrs_buffer = []
    print(f"Creating a chunk of {len(all_constr_generators)} tests...")
    for t_gen in tqdm(all_constr_generators):
        constrs_validated = ConstrModel.validate_python(t_gen())
        constrs_buffer.append(constrs_validated)
        if calc_file_apprx_file_size(test_data_size, len(constrs_buffer)) > max_file_length: #MB
            if len(constrs_buffer) == 1:
                # if there is just one, just upload that once
                constrs_to_upload = constrs_buffer
            else:
                # Otherwise it spilt over so we just want everything but the last one that made it spill over
                constrs_to_upload = constrs_buffer[:-1]

            #Upload everything except the last one
            constr_up = ConstrUploader(constrs_to_upload, put_data_in_cache=put_data_in_cache)
            all_db_objs.append( constr_up )
            constr_uploaders.append(constr_up)

            #reset buffer
            constrs_buffer = [constrs_to_upload[-1]] #works for both cases above either [] or [constr_spilt]

    # if it spills over right at the end then the loop exits with still one constr in the buffer
    if len(constrs_buffer) > 0:
        print("End of loop, uploading last chunk...")
        constr_up = ConstrUploader(constrs_buffer, put_data_in_cache=put_data_in_cache)
        constr_uploaders.append(constr_up)

    print("Uploading to db and to cloud!")
    for cup in tqdm(constr_uploaders):
        cup.db_commit()
    return all_db_objs


#-----------------------------DELETE CLI ARGS------------------------#
@click.group()
def delete_db():
    pass

@delete_db.command(help="Attempts to drop all tables and create all tables.")
@click.option('--keep_users', is_flag=True, help="Wipe the database but keep all users")
def wipe_all(keep_users):
    if keep_users:
        user_data = [{
            'username': user.username, 
            'email': user.email,
            'site': user.site,
            'is_admin': user.is_admin,
            'is_active': user.is_active,
            'created_at': user.created_at
            } for user in mdl.User.get_all()]

        clear_cloud_files() #has to be before close
        #now full wipe, first close session from above query!
        db.session.close()
        db.drop_all()
        db.create_all()

        #should somehow figure out a test if the below will work before dropping all!
        add_base_db_data()

        for data in user_data:
            user = mdl.User(
                username=data['username'],
                password_hash=data['password_hash'],
                email=data['email'],
                site=data['site'],
                is_admin=data['is_admin'],
                is_active=data['is_active'],
                has_logged_in=data['has_logged_in'],
                created_at = data['created_at']
            )
            db.session.add(user)
        db.session.commit()
        click.echo("Successfully cleared database and kept users.")
    else:
        choice = click.prompt("Are you sure you want to wipe the database and lose all users (y/n)?")
        if choice.lower() in ['y', 'yes']:
            clear_cloud_files()
            click.echo("cleared cloud")
            db.drop_all()
            click.echo('Dropped all tables')
            db.create_all()
            click.echo('Recreated all tables')
            db.session.commit()
            click.echo("Complete database wipe, add the root user using 'add-root' command and to configure the db with the data use the 'add-base' command.")
        else:
            click.echo("Aborting...")

@delete_db.command(help="Empties a construction table based on an option")
@click.option('-t','--table', 
              multiple=True, 
              type=click.Choice(['test', 'assembly'], case_sensitive=False),
              help="the table to empty")
def wipe_table(table: tuple):
    #if multiple it returns table as a tuple
    def delete_remote_files_and_query(obj_type):
        for obj in obj_type.query:
            obj.file.delete_remote()
        obj_type.query.delete()
    for t in table:
        click.echo(f"Wiping {t}!")
        if t=='assembly':
            delete_remote_files_and_query(mdl.Assembly)
        elif t=='test':
            delete_remote_files_and_query(mdl.Test)
        else:
            raise NotImplementedError(f"Sorry the given table ({t}) is not implemented for deletion")
    db.session.commit()

@delete_db.command(help="Empty the cloud of all files.")
def wipe_cloud():
    choice = click.prompt("Are you sure you want to delete all the files in the cloud pertaining to this database (y/n)?")
    if choice.lower() in ['y', 'yes']:
        clear_cloud_files()
        click.echo("Cleared all cloud files.")
    else:
        click.echo("Aborting...")
    


@delete_db.command(help="Wipes all data from Fake stress test")
def wipe_fake():
    #Fake Production
    fake_mod_type = mdl.ModuleType.get_by_either("Fake Production")

    print(f"Deleting {len(fake_mod_type.modules)} modules and all it's tests, assemblies, and components (as well as component info too)")
    for mod in tqdm(fake_mod_type.modules):
        db.session.delete(mod)
        db.session.commit()

    print("Checking orphaned files...")
    orphaned_files = db.session.query(mdl.File)\
        .outerjoin(mdl.File.assemblies).outerjoin(mdl.File.tests)\
        .filter(mdl.Assembly.id.is_(None), mdl.Test.id.is_(None)).all()
    
    #delete all orphaned files
    print("Deleting orphaned files...")
    for f in tqdm(orphaned_files):
        db.session.delete(f)
    db.session.commit()

@delete_db.command(help="Wipes all parts of a spefic type")
@click.option('--comptype', help='Wipes a all components of this compoent type', type=str)
@click.option('--modtype', help='Wipes all modules of this module type', type=str)
def wipe_parttype(comptype, modtype):
    if comptype:
        print(f"Deleting {comptype}...")
        comptype = mdl.ComponentType.get_by_either(comptype)
        for comp in tqdm(comptype.components):
            db.session.delete(comp)
            db.session.commit()
    elif modtype:
        modtype = mdl.ModuleType.get_by_either(modtype)
        for mod in tqdm(modtype.modules):
            db.session.delete(mod)
            db.session.commit()

#-----------------------------UPDATE TO DB CLI-------------------------------#

@click.group()
def update_db():
    pass

@update_db.command(help="Changes the name of a test or assembly type")
@click.option('--test_name', help='Old name of the type', type=str)
@click.option('--new_test_name', help='New name of the type', type=str)
def change_test_name(test_name, new_test_name):
    tests_to_change = db.session.execute(
        db.select(mdl.Test).where(mdl.Test.type == test_name)
    ).scalars().all()
    if not tests_to_change:
        print(f"No tests found with the name {test_name}")
        return
    for test in tests_to_change:
        test.type = new_test_name

    db.session.commit()
#------------------------------ADDING TO DB CLI------------------------------#

@click.group()
def insert_db():
    pass

@insert_db.command(help="Adds all the data from 'BASE_DB_DATA.json', does not work if it is partially uploaded.")
@click.option('--init_db', is_flag=True, help="Will also create all the tables for the database, used for fresh installation")
def add_base(init_db):
    if init_db:
        db.create_all()
    add_base_db_data()
    click.echo("Added base data for app")
    db.session.commit()

@insert_db.command(help="Add arbitrarily large data for stress testing in the database")
@click.option('--row_size', help='Size of each row in kilobytes.', type=float)
@click.option('--n_modules', help='Number of modules to fill the db with', type=int)
@click.option('--n_tests_per', help='Number of tests to fill the db with', type=int)
@click.option('--upload_chunk_size', default=5, help='Json File size in MB', type=float)
@click.option('--put_data_in_cache', is_flag=True, help='If to put full data into the db', type=bool)
def stress_test(row_size, n_modules, n_tests_per, upload_chunk_size, put_data_in_cache):
    row_size = int(row_size * 1000) #KB to bytes
    upload_chunk_size = int(upload_chunk_size*1024*1024)

    #make etroc and lgads and put them into subassemblies
    subs = make_subassemblies(n_modules * 4)   

    mods = []
    fake_mod_type = mdl.ModuleType.get_by_either("Fake Production")
    fake_pcb_type = mdl.ComponentType.get_by_either('Fake Module PCB')
    location_choices = [mdl.Location.get_by_id(i) for i in range (1,5)]

    print(f"Creating {n_modules} modules and module pcbs (and associating all components)...")
    for i in tqdm(range(n_modules)):
        mod = mdl.Module(
            serial_number = "FAKE_MOD"+str(i).zfill(4),
            module_type = fake_mod_type,
            assembly_location = random.choice(location_choices),
            user_created_id = 1,
        )
        mod_pcb = mdl.Component(
            serial_number = "FAKE_MOD_PCB"+str(i).zfill(4),
            component_type = fake_pcb_type,
            vendor = 'me',
            user_created_id = 1,
        )
        mod_pcb.module = mod

        comp_1 = subs[4*i + 0]
        comp_2 = subs[4*i + 1]
        comp_3 = subs[4*i + 2]
        comp_4 = subs[4*i + 3]

        comp_1.module = mod
        comp_2.module = mod
        comp_3.module = mod
        comp_4.module = mod

        comp_1.component_pos = 1
        comp_2.component_pos = 2
        comp_3.component_pos = 3
        comp_4.component_pos = 4
        mod_pcb.component_pos = 0

        db.session.add(mod_pcb)
        db.session.add(mod)
        mods.append(mod)

    db.session.commit()
    print('')

    #------------------UPLOAD THE TESTS------------------#
    all_tests = []
    print("Generating test partial functions...")
    for i, m in tqdm(enumerate(mods)):

        # Module and Subasssembly Tests        
        for mod_comp in m.components:
            if mod_comp.component_type != fake_pcb_type: #no tests for pcb...
                all_tests += [partial(rand_mod_test, m, mod_comp, row_size) for _ in range(n_tests_per)]
                all_tests += [partial(rand_comp_test, mod_comp, row_size)   for _ in range(n_tests_per)]

                #ETROC and LGAD tests
                for c in mod_comp.components:
                    all_tests += [partial(rand_comp_test, c, row_size) for _ in range(n_tests_per)]

        if i%100 == 0 and i > 0:
            with db.session.no_autoflush:
                constr_chunk_upload(all_tests, row_size, max_file_length=upload_chunk_size, put_data_in_cache=put_data_in_cache)
            all_tests = []

    print('')
    #just in case
    db.session.flush()
    with db.session.no_autoflush:
        constr_chunk_upload(all_tests, row_size, max_file_length=upload_chunk_size, put_data_in_cache=put_data_in_cache)

@insert_db.command(help="Sync current location of all parts based on last shipment")
def sync_current_location():
    from application.shipments.logistics import calc_current_location
    for part in tqdm(mdl.Module.query.all() + mdl.Component.query.all()):
        part.current_location = calc_current_location(part)
    db.session.commit()
    

cli = click.CommandCollection(sources=[delete_db, insert_db])
if __name__ == '__main__':
    cli()
