from application import app
from application import db

#these lines add to the Flask shell to make it easy for testing scripts
#and interacting with the database
from application import db_models as mdl
@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': mdl.User}