# ETL Construction Web Application

This repository contains all code for the Endcap Timing Layer (ETL) construction web application. It aims to track all the parts that make up ETL up and til it is integrated into CMS. 

**Tech Stack**: The application here is a Flask Web App that uses a SQLAlchemy to interface with a PostgreSQL Database. And, JSON data from tests and assembly steps are validated using Pydantic.

**Services**: The web app is hosted on [CERN PaaS](https://paas.docs.cern.ch/) and the database(s) is hosted on [CERN Database on Demand](https://dbod-user-guide.web.cern.ch/). Also, [CERNbox](https://cernbox.docs.cern.ch/web/) is used for the cloud storage of files. 

## Getting Started with Developement
This chapter will explain how you can get set up to develop on your local machine. 

#### Before You Start
If you are new to web developement, and even if you are not, it is worth checking out the framework laid out in this tutorial by [Miguel Grinberg](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) because this web app follows the exact structure. I recommend working through Chapters 1-5, 7 and 15. To save redudancy, I will assume you worked through it and try to point out areas that require knowledge from the tutorial.

### Local Setup

### Database Setup
All that is needed is to set up an empty PostgreSQL database and the software will handle the creation of the schema. PostgreSQL is needed because the software uses some specific PostgreSQL functionality. To install on an Ubuntu system you can run, 
```
sudo apt install postgresql
```
For other operating systems see the [postgresql manual](https://www.postgresql.org/download/).

Log in as the `postgres` user,

```
sudo -u postgres psql
```
This should give you an output something like this:
```
psql (12.19 (Ubuntu 12.19-0ubuntu0.20.04.1))
Type "help" for help.

postgres=# 
```
We need to set the password for the user `postgres` by running,
```
ALTER USER postgres PASSWORD 'mynewpassword';
```
Make sure you take note of this password as it will be needed to make the connection to the database (I would avoid using fancy characters like @ because then you will have to escape them in the database URI). Now create the database by doing,
```
CREATE DATABASE ETL_database;
```
If you then run `\list` you should see the database name in the list. If you do you can now logout by just typing `quit`. 

Note, the case matters on the database name, whatever it is here is what will be used in the database URI below.

### App Setup

#### Install
To get started you first need to clone the repository in your local environment. 
```
git clone ssh://git@gitlab.cern.ch:7999/hswanson/mtd-etl-app.git
```
Note, in order for this line to work you need to set up ssh with gitlab. You can refer to the [gitlab documentation](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair) to do this.

Next you need to install the packages this application uses. As always it is recommended to set up a [virtual environment](https://docs.python.org/3/library/venv.html) before you run this line,
```
pip install -r requirements.txt
```
Notes, 
* If you are installing on a mac you may need to install the package called magic (a library for reading the the first few bits of a file to see the file type) manually.
* Python 3.9 is the oldest supported version of python for these dependencies. Next we need to set up some configuration variables for the application this includes for example the URI the of the PostgreSQL database. 

A dockerfile does also exist to build an image to run the application in a docker container (TODO) if sorting out dependency issues is too much of a headache.

#### App Config

Inside the project directory (at the same level as the requirements.txt script for example), you need to create this python file to house some [environment variables](https://www.geeksforgeeks.org/environment-variables-in-linux-unix/), for linux,

```
touch env_personal.py
```
*IMPORTANT*: This will house potentially sensitive data, so make sure this file is not pushed to gitlab!

For documentation purposes the following is contained the `env_personal.py` but for fresh setup purposes only `SQLALCHEMY_DATABASE_URI` is needed. 

```
SQLALCHEMY_DATABASE_URI = "postgresql://postgres:mynewpassword@localhost:5432/etl_database
#used for remote upload
WEBDAV_LOGIN = ... 
WEBDAV_PASSWORD = ...
#used for emailing users 
EMAIL_ADDRESS = ... 
EMAIL_APP_PASSWORD = ...
```
### Testing Connection and Seed Data

Alright, lets see if you have a connection to the database by added the base data to start running the app.
```
python etldb.py add-base --init_db
```
Voila, your database should now have data. You will also probably want to be able to sign in when you fire up the server so you should create the root user,

```
python etldb.py add-root
```

### Running the Flask App

This can simply be done by running this line which also puts it in debug mode (this allows you to make code changes and the server will restart with the necessary changes),
```
python flask --app mtd_etl_server run --debug
```
If this complains and returns this `* Tip: There are .env or .flaskenv files present. Do "pip install python-dotenv" to use them.` you can try this line to fire up the server,

```
python -m flask run
```

### Database Migrations
If you decide a schema needs to be changed in the database you need to have database migrations set up. An easy method to do this is by running
```
flask db upgrade
```
Or if it is complaining about the import `python -m flask db upgrade`.

This will create the needed database to track migrations. You just have to make sure upon any schema, changes pulled from git, (as decided by the file `db_models.py`) that you run flask db upgrade so that the migration scripts can run and catch your database up to date. 

If you make your own changes to `db_models.py` you need to generate a migration script,
```
flask db migrate -m "added mycolumn to tests"
```
review it carefully such that you can upgrade or downgrade it seemlessly. Then once satisfied,
```
flask db upgrade
```
As before if it is complaining about imports you need `python -m` in front of the flask command. 

## Application Architecture
If you wish to understand how the web app works I will be explaining all the core areas of the application here. Again, it is assumed you have the knowledge from this [tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world). Like in the tutorial, I have my app/application folder with the housed blueprints that logically organize different core areas of the web application. 

To avoid confusion by "application" I mean all code inside this repository. So the tracked python package ETLup, Command line interfaces, the application folder , migrations, etc... 

#### Database Models
The entire schema is defined in `application/db_models.py` by using [SQLAlchemy](https://docs.sqlalchemy.org/en/20/). Any updates to the schema by editing the `db_models.py` requires a database migration. See the database migrations set up in the installation for details. 

#### ETLup
This is a standalone python package. You will not be able to publish but you can push changes that can be published. It is inteaded to help with the uploads to the database.

### Blueprints
The Flask app uses blueprints to organize different sections. Each subdirectory within the main application directory represents a blueprint, helping to separate and manage unrelated areas of the web app.

## Hosting with CERN Services

### CERN Platform as a Service
This link https://webservices-portal.web.cern.ch/paas/ allows you to create a PaaS project at CERN (Web Services is part of a larger CERN service to facilitate web services). Hayden Swanson (me) is the owner, it is an official project so ownership is reassigned if I were to leave. The egroup's name is *cms-etl-construction-web-app* and can be searched here https://groups-portal.web.cern.ch/.

Here is the main page of the PaaS. You can see two *Deployments* one for the staging and one for the production web app. 
![PaaS Main Page](docs/image-1.png)

If you click on a deployment you can see the *Pods*, *Builds*, *Services* and *Routes*. Each have a key function to making the website work. I won't explain how each of them work but simply highlight what they are commonly used for.
![alt text](docs/image-2.png)

#### Deployment
Editing deployment allows you to add environment variables to the application. So you can change the database URI for example, see the `config.py` file for all the environment variables. Our deployments are configured for a docker container that is built from the `Dockerfile` that is in this repository. The `Dockerfile` is a set of instructions to build and run the image. 

#### Pods
The pod is where the docker container runs and it is here where you can get terminal access to run the my Command Line Interfaces inside the container. Logs from the server also written out here. 

To run the CLI's you need to start the virtual environment by executing `. venv/bin/activate` in the pod terminal. You can use the `--help` flags to navigate and learn how to use the CLI's.

#### Builds
Our build type is a docker container and is configured from the deployment to look at a specific branch from the gitlab repo for the most up to date code from that branch. Webhooks are implemented such that when pushes are made to that branch it triggers a new build ([see here for rough instructions](https://docs.google.com/document/d/1oX5g4cbULFPXSD-oQd0V0KgImd7JttCtG3u5dARQdtA/edit)). 

#### Services
If you click the services you can get the Service YAML config and in it one part will be:
``` 
 ports:
   - name: https
     protocol: TCP
     port: 8000
     targetPort: 5000
```
The Dockerfile exposes port 5000 and the WSGI server is now listening on that port by running these commands:
`EXPOSE 5000` and `exec gunicorn -b :5000 --access-logfile - --error-logfile - mtd_etl_server:app`

So the target port needs to match this port. Port 8000 is what is open to the outside (important for routes, see next). 

#### Routes
Biggest thing here is that the route ports match service ports. If you click on routes and go to actions in top right, edit route:
It should auto to 8000 -> 5000 for the target route, if not set target route to https or the name from above in the yaml file. Now your app should be exposed to the outside world! If it is blocked makes sure the all IP's are not whitelisted in PaaS.

### CERN Database on Demand
- [CERN Database on Demand Guide](https://dbod-user-guide.web.cern.ch/)
- [Reason for using DBOD with CERN PaaS](https://paas.docs.cern.ch/2._Deploy_Applications/Databases/1-databases/).
- [DBOD Dashboard](https://dbod.web.cern.ch/pages/dashboard)

The dashboard is used for database backup and restoration. 

After the database getting approved you get an email with login credentials to psql into the db. Then one should follow the procedure here (needed to change password): https://dbod-user-guide.web.cern.ch/getting_started/PostgreSQL/postgresql/ . I was given a username and temporary password then ran this line:
`psql -h dbod-etl-construction-prod.cern.ch -p 6600 -U admin -c '\password' `
I was then prompted to enter the temporary password, then the new password for the database

#### Configuration

2 Databases, one for the staging branch of the repository and a prod instance for the master branch of the repository. 
- The resources can be found here: https://auth-resources.web.cern.ch/my 
- DBOD PROD instance name: etl_construction_staging
- DBOD PROD instance name: etl_construction_prod
- **Egroup**: *cms-etl-construction-web-app*

A prod instance is used for the staging because backups and restoration requires manual intervention by a DBOD maintainer. 

#### Database Connections
These of course require passwords that are saved by me in a password manager. Below is a manual login and should hopefully rarely be used (typically the CLI inside PaaS is used). 
- Staging
    - `psql -h dbod-etl-construction-staging.cern.ch -p 6601 -U admin`
- Production
    - `psql -h dbod-etl-construction-prod.cern.ch -p 6600 -U admin`

Also for note the URI for postgresql database look like:
`postgresql://[username]:[password]@[host]:[port]/[database]`

For example the staging URI from  above would be:
`postgresql://admin:[password]@dbod-etl-construction-staging.cern.ch:6601/admin`

### CERNbox
This holds the files for the website. The Egroups for CERNbox are 
`cernbox-project-etl-construction-app-storage-admins` and
`cernbox-project-etl-construction-app-storage-writers`


## MTD Database Upload

To be done

## Workflow

The repository has 3 main branches: dev, staging, master/production. Work is done in a developer branch which when an implementation is satisfactory, it is merged into the staging branch. This is automatically hosted for testing on a test instance of the web app and database. After that passes inspection, the staging branch can be merged into the production branch which is also automatically hosted and connected to its own production database.



