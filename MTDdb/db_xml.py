from application import app
from application import db_models as mdl
from lxml import etree
import os
from typing import Union, List
app.app_context().push()

def is_subassembly(part) -> bool:
    return part.__tablename__ == 'component' and len(part.components) == 2
    
def is_base_component(part) -> bool:
    return part.__tablename__ == 'component' and len(part.components)==0

def is_module(part) -> bool:
    return part.__tablename__ == 'module'

class PartOrganizer:
    def __init__(self):
        #list of sqlalchemy objects
        self.base_components = []
        self.subassemblies = []
        self.modules = []

    def add_part(self, part: Union[mdl.Component, mdl.Module]):
        """
        Give sqlalchemy part object (Module and Component) and it stages it for XML upload along with its sibling parts
        """
        if part is None:
            raise ValueError("Part is None does not exist in the database")
        elif is_module(part):
            self.modules.append(part)
            for comp in part.components:
                if is_subassembly(comp):
                    self.subassemblies.append(comp)
                    self.base_components += comp.components
                elif is_base_component(comp):
                    self.base_components.append(comp)
        elif is_subassembly(part):
            self.subassemblies.append(part)
            self.base_components += part.components
        elif is_base_component(part): #base_component = (ETROC, LGAD, Module PCB)
            self.base_components.append(part)
        else:
            raise ValueError("Unable to handle part, is not from module table or is a component with non standard amount of subcomponents. 2 subcomponents is an subassembly, 0 subcomponents is either an ETROC/LGAD/Module PCB")
        
    def add_all(self, parts: List[Union[mdl.Component, mdl.Module]]):
        for part in parts:
            self.add_part(part)

def parts_to_xml(parts: List[Union[mdl.Component, mdl.Module]]):
    if not parts:
        raise ValueError("No parts for upload")
    root_elem = etree.Element('ROOT', nsmap={'xsi':"http://www.w3.org/2001/XMLSchema-instance"})
    parts_elem = etree.SubElement(root_elem, 'PARTS')
    for part in parts:
        part_elem = part.to_mtddb_xml()
        parts_elem.append(part_elem)
    return etree.tostring(root_elem, encoding='UTF-8', xml_declaration=True, standalone=True, pretty_print=True)

def write_xml_to_folder(folder_path, filename, xml_str):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    with open(os.path.join(folder_path, filename+'.xml'), 'wb') as f:
        f.write(xml_str)

# for mod in mdl.Module.get_all():
#     print(mod.serial_number)
#     make_xml_files(mod.serial_number, "/home/hayden/Desktop/mtddb_xml/test_modules")


#lets make connection to MTD and send over XML files.
#Architecure will just be mass migration and marked as sync upon migration

#use test database --int2r


#mtdcdb.writeToDB()

#can check results with rhapi
#ssh -f -N -L 8113:dbloader-mtd.cern.ch:8113 {username}@lxplus.cern.ch

#        cmd.append('-oProxyJump=' + user + '@lxtunnel.cern.ch')
#        sshTargetHost = 'dbloader-mtd.cern.ch'


#initiate session?
# ssh -M -p 50022 -N -f hswanson@localhost
#OR
# ssh -f -N -L 50022:dbloader-mtd.cern.ch:22 -L 8113:dbloader-mtd.cern.ch:8113 hswanson@lxtunnel.cern.ch


#with proxy
#scp -P 50022 -oNoHostAuthenticationForLocalhost=yes xmlfilename.xml hswanson@localhost:/home/dbspool/spool/mtd/int2r/xmlfilename.xml
#no proxy
# scp -oProxyJump=hswanson@lxtunnel.cern.ch -oNoHostAuthenticationForLocalhost=yes xmlfilename.xml hswanson@dbloader-mtd.cern.ch:/home/dbspool/spool/mtd/int2r/xmlfilename.xml

#QUERY
# python3 ./rhapi.py --url=http://localhost:8113 "SELECT p.* from mtd_int2r.parts p where (p.KIND_OF_PART = 'ETROC' or p.KIND_OF_PART = 'LGAD' or p.KIND_OF_PART = 'Module PCB') and p.BARCODE NOT LIKE 'PRE%' and p.BARCODE NOT LIKE 'FK%'"

#-----------------------------

#with proxy
#ssh -p 50022 -oProxyJump=hswanson@lxtunnel.cern.ch -oNoHostAuthenticationForLocalhost=yes hswanson@dbloader-mtd.cern.ch test -f /home/dbspool/logs/mtd/int2r/xmlfilename.xml && echo Done! || echo .;

#no proxy
#ssh -oProxyJump=hswanson@lxtunnel.cern.ch -oNoHostAuthenticationForLocalhost=yes hswanson@localhost test -f /home/dbspool/logs/mtd/int2r/xmlfilename.xml && echo Done! || echo .;

#public private key pair, for sign in with, lxtunnel, 
#try in the paas machine, see if you can move it over,

#to query do 


#Terminate session
#ssh -O exit -p 50022 hswanson@localhost



#WORKING LINES

#initiate session
# ssh -f -N -L 50022:dbloader-mtd.cern.ch:22 -L 8113:dbloader-mtd.cern.ch:8113 hswanson@lxtunnel.cern.ch
#query parts
# python3 ./rhapi.py --url=http://localhost:8113 "SELECT p.* from mtd_int2r.parts p where (p.KIND_OF_PART = 'ETROC' or p.KIND_OF_PART = 'LGAD' or p.KIND_OF_PART = 'Module PCB') and p.BARCODE NOT LIKE 'PRE%' and p.BARCODE NOT LIKE 'FK%'"


#initate without lxtunnel
#ssh -f -N -L 50022:dbloader-mtd.cern.ch:22 -L 8113:dbloader-mtd.cern.ch:8113 hswanson@dbloader-mtd.cern.ch
#query parts
# python3 ./rhapi.py --url=http://localhost:8113 "SELECT p.* from mtd_int2r.parts p where (p.KIND_OF_PART = 'ETROC' or p.KIND_OF_PART = 'LGAD' or p.KIND_OF_PART = 'Module PCB') and p.BARCODE NOT LIKE 'PRE%' and p.BARCODE NOT LIKE 'FK%'"


#ssh -f -N -L 8113:localhost:8113 hswanson@dbloader-mtd.cern.ch
