import click
from application import app, db
from application import db_models as mdl
from datetime import datetime, timezone
from config import the_config
from MTDdb import rhapi, db_xml
from fabric import Connection
from functools import update_wrapper
from getpass import getpass

app.app_context().push()

query_example =  """
    SELECT p.* from mtd_int2r.parts p where (p.KIND_OF_PART = 'ETROC' or p.KIND_OF_PART = 'LGAD' or p.KIND_OF_PART = 'Module PCB') and p.BARCODE NOT LIKE 'PRE%'
"""

def make_connection(f):
    """
    Wraps click function to make the ssh connection
    """
    def new_func(*args, **kwargs):
        ssh_conn = Connection(
            host=the_config.MTDDB_SSH_HOST, 
            user=the_config.MTDDB_SSH_USERNAME,
            connect_kwargs={
                "password": getpass(prompt='Enter your CERN password: ')
            }
        )
        with ssh_conn.forward_local(the_config.MTDDB_SSH_PORT):
            print(f"Forwarded Local Port to dbloader on this connection: {ssh_conn}")
            return f(*args, **kwargs)
    return update_wrapper(new_func, f)

@click.group()
def read_db():
    pass

@read_db.command(help='Query the MTD database')
@click.option('--query', help=f'An example: \n {query_example}')
@click.option('-f', '--format', default='csv', help=f"Choose format of ouput: {rhapi.FORMATS}")
@make_connection
def read(query, format):
    print("hello world")
    print(query_example.strip())
    client = rhapi.DBClient(
        query_example.strip(),
        f'http://localhost:{the_config.MTDDB_SSH_PORT}',
        format=format,
    )
    data = client.run()
    click.echo(data)

#--------------------------------------#

@click.group()
def write_db():
    pass

@write_db.command(help='Upload a part to the MTDdb')
@click.option('-sn','--serial_number', help='Give the serial number of the part you wish to upload')
@make_connection
def upload(serial_number):
    part = mdl.Component.get_by_sn(serial_number) or mdl.Module.get_by_sn(serial_number)
    if part is None:
        click.echo("This part does not exist in the ETL database")
        return None
    
    PartOrganizor = db_xml.PartOrganizer()
    PartOrganizor.add_part(part)

    #need to filter the lists by whats in mtddb
    query = """
    SELECT p.* from mtd_int2r.parts p
    WHERE p.BARCODE IN ('SerialNumber1', 'SerialNumber2', 'SerialNumber3');
    """
    client = rhapi.DBClient(
        query.strip(),
        'http://localhost:8113',
        format=format,
    )
    data = client.run()



cli = click.CommandCollection(sources=[read_db])
if __name__ == '__main__':
    #make ssh connection
    cli()
