"""index on type and not deprecated

Revision ID: a11a0bdc2d95
Revises: e8538e5285cc
Create Date: 2024-10-17 10:05:34.900466

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a11a0bdc2d95'
down_revision = 'e8538e5285cc'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('assembly', schema=None) as batch_op:
        batch_op.create_index('idx_is_deprecated_assembly_type', ['type', 'is_deprecated'], unique=False)

    with op.batch_alter_table('test', schema=None) as batch_op:
        batch_op.create_index('idx_is_deprecated_test_type', ['type', 'is_deprecated'], unique=False)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('test', schema=None) as batch_op:
        batch_op.drop_index('idx_is_deprecated_test_type')

    with op.batch_alter_table('assembly', schema=None) as batch_op:
        batch_op.drop_index('idx_is_deprecated_assembly_type')

    # ### end Alembic commands ###
