#WARNING: IMPORTING ANYTHING FROM APPLICATION, so application.<bluepring> WILL SURELY CAUSE CIRCULAR IMPORT, to fix import it inside function.
from application import db
from config import the_config

from sqlalchemy import ForeignKey, Column, Table, event, func
from sqlalchemy.orm import Mapped, mapped_column, relationship, validates, declared_attr
from sqlalchemy.schema import Index
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.dialects.postgresql import ARRAY, JSONB
from typing import List #for type annotations
from application.datetime_handler import now_utc, validate_datetime
from etlup.src.etlup.construction.adapters import ConstrModel, ConstrArrModel
from etlup.src.etlup.upload import get_model
from webdav3.client import Client #for cloud talking webdav is old but it works for CERNbox
from webdav3.exceptions import WebDavException
from flask import url_for
from flask_login import UserMixin
from io import BytesIO
from datetime import datetime
import json
from lxml import etree
from os import path 
import inspect
import hashlib

def hash_it_up(func):
    source_code = inspect.getsource(func)
    hash_code = hashlib.sha256(source_code.encode('utf-8'), usedforsecurity=False)
    return hash_code.hexdigest()

def get_model_by_tablename(tablename: str):
  """Return class reference mapped to table.

  :param tablename: String with name of table.
  :return: Class reference or None.
  """
  for c in db.Model.__subclasses__():
    if hasattr(c, '__tablename__') and c.__tablename__ == tablename:
      return c

#custom mixins
class UtilityMixin:
    @classmethod
    def get_by_id(cls, id_):
        return db.session.query(cls).filter(cls.id == id_).first()

    @classmethod
    def get_all(cls):
        return db.session.query(cls).all()

class TimeStampMixin:
    created_at: Mapped[datetime]  = mapped_column(db.DateTime(timezone=True), nullable=False, default=now_utc())
    last_edited: Mapped[datetime] = mapped_column(db.DateTime(timezone=True), nullable=True)

class ByNameMixin:  
    @classmethod
    def get_by_abbv(cls, abbv):
        return cls.query.filter_by(name_abbv=abbv).first()

    @classmethod
    def get_by_long_name(cls, long_name):
        return cls.query.filter_by(name_long=long_name).first()

    @classmethod
    def get_by_either(cls, either_name):
        return cls.get_by_long_name(either_name) or cls.get_by_abbv(either_name)

class DeprecateMixin:
    """
    Initially set to null, meaning it has never been deprecated. But once set it is either deprecated or not deprecated. \n
    The last user to set the deprication status is stored as well as when it happened.
    """
    #could use this to do relationships for user too
    # https://stackoverflow.com/questions/67738904/sqlalchemy-mixins-how-to-inherit-from-them-and-variable-relationships
    is_deprecated: Mapped[bool] = mapped_column(unique=False, nullable=False, index=True, default=False)
    deprecated_by_id: Mapped[int] = mapped_column(db.ForeignKey('user.id'), nullable=True)
    deprecated_on: Mapped[datetime] = mapped_column(db.DateTime, nullable=True, default=now_utc())

    @declared_attr
    def deprecated_by(self) -> Mapped["User"]:
        return relationship("User", foreign_keys=[self.deprecated_by_id]) #need the list thing because of other foriegn keys in there...
    
    def not_depricated(self, rel_name: str):
        # Get the relationship property from the instance
        relationship_prop = getattr(self, rel_name, None)
        if relationship_prop is None:
            raise ValueError(f"No relationship named '{rel_name}' found.")
        
        # Filter the relationship objects based on is_deprecated = False or None
        non_deprecated_items = [
            item for item in relationship_prop
            if (item.is_deprecated==False or item.is_deprecated is None)
        ]
        return non_deprecated_items

#add searchable mixin?? try to use the search class you already have???

class MTDdbSyncMixin:
    #Give mixin to tables you want to be synced to the Official MTDdb
    mtddb_synced: Mapped[bool] = mapped_column(default=False, nullable=False)

class MTDdbRequiredMixin:
    #Give to tables so it has to be required in the Official MTDdb
    mtddb_required: Mapped[bool] = mapped_column(default=False, nullable=False)

class MTDdbZZCodeMixin:
    #Give to lookup tables to have zz code 
    mtddb_zz_int: Mapped[int] = mapped_column(db.Integer, unique=False, nullable=True) #numeric string given to each of the component or part types, has to be unique across multiple tables

    @validates("mtddb_zz_int")
    def validate_zz(self, _key, zz):
        if type(zz)==int and zz > 99:
            raise ValueError(f"ZZ value cannot be more than 99 as it can only be two digits in length, your zz={zz} ")
        else:
            return zz

    @hybrid_property #i dont think this counts as another column so you cannot like set it to be something like int
    def mtddb_zz_str(self):
        if self.mtddb_zz_int:
            return str(self.mtddb_zz_int).zfill(2)
        
class MTDdbBarcodeMixin:
    @property
    def mtddb_barcode(self):
        if self.__tablename__ == 'module':
            zz_code = self.module_type.mtddb_zz_str
        elif self.__tablename__ == 'component':
            zz_code = self.component_type.mtddb_zz_str
        else:
            raise NotImplementedError(f'Have not implemented zz codes for {self.__tablename__} yet only module_type and component_type have the mtddb_zz code')
        
        if zz_code is not None:
            if self.id <= 9999999: #right now it is 9,999,999 as the max
                return the_config.MTD_BARCODE_PREFIX + zz_code + str(self.id).zfill(6)
            else:
                raise ValueError(f"ID exceeded 1 million which is incompatable with MTDdb Barcode requirements of only 6 digits")
        else:
            raise AttributeError(f"This object's part type does not have a corresponding ZZ code")
        
#-------------DEFINED TABLES FOR ORM----------------------#

class User(db.Model, UserMixin, UtilityMixin, TimeStampMixin):
    __tablename__ = "user"

    #auto increment not needed but labeled to be explicit
    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    username: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    email: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    site_loc_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id', name="site_loc_fk_constraint"), nullable=True) #user doesnt need to have one but should set one
    is_admin: Mapped[bool] = mapped_column(default=False, nullable=False)
    is_active: Mapped[bool] = mapped_column(default=True, nullable=False)

    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    #Relationships
    user_edited = relationship("User", foreign_keys=[user_edited_id], remote_side=[id], uselist=False, post_update=True)
    site: Mapped["Location"] = relationship(back_populates="users", foreign_keys=[site_loc_id]) 

    def to_dict(self):
        #https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xxiii-application-programming-interfaces-apis

        data = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'site_name': self.site.name_long if self.site else None, #obviously dont let this be updated
            'is_admin': self.is_admin,
            'is_active': self.is_active,
            'has_logged_in': self.has_logged_in,
        }
        return data
    
    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'is_admin', 'is_active']: #dont allow the ability to set the site yet, will require other work
            if field in data:
                setattr(self, field, data[field]) #sets any of fields like user.username = data[field]
        if 'site_name' in data:
            #if the site name doesn't exist handle that in the routes
            #otherwise here it is just None
            self.site = Location.get_by_either(data['site_name'])
        if new_user and 'created_at' not in data:
            #TODO DO THIS BETTER
            self.created_at = now_utc()
    
    @staticmethod
    def get_by_username(username):
        return User.query.filter_by(username=username).first()
    
    def __repr__(self) -> str:
        return f'<User {self.username}>'

class ConstructionBaseMixin:
    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)   
    _display_data_cache: Mapped[JSONB] = mapped_column(JSONB, nullable=True) # never use this directly
    data: Mapped[JSONB] = mapped_column(JSONB,nullable=True) #10KB Tested limit for full production
    cache_update_hash: Mapped[str] = mapped_column(db.String(64), unique=False, nullable=True) #hash of function that makes the data_cache
    measurement_date: Mapped[datetime] = mapped_column(db.DateTime(timezone=True), nullable=True)
    component_pos: Mapped[int] = mapped_column(db.Integer,unique=False, nullable=True)
    data_version: Mapped[str] = mapped_column(db.String(9), unique=False, nullable=False)
    uuid: Mapped[str] = mapped_column(db.String, unique=False, nullable=False)
    type: Mapped[str] = mapped_column(db.String(50), unique=False, nullable=False)#, index=True)
    
    @property
    def display_data_cache(self) -> dict:
        """
        Gets cached data that is calculated off the data column. If the calculation of they pydantic model changes it recalculates the data.
        """
        # Check if hash in db is the same as the hashed method in model
        DataModel = get_model(self.data_version, self.type)
        calc_display_data = getattr(DataModel, 'calc_display_data', None)

        if calc_display_data is None:
            # if it has no calc_display_data method then it does not use this feature.
            return None
        elif not callable(calc_display_data):
            raise AttributeError(f"This data model (calc_display_data) does not have the callable method 'calc_display_data'")
        elif self.cache_update_hash == hash_it_up(calc_display_data):
            # if the function is the same, don't calculate new hash!
            return self._display_data_cache
        #---UPDATE CACHE WITH NEW CALCULATION---#
        # 1. Grab full Pydantic Data Model
        data_model = self.to_data_model()
        # 2. Calculate new data cache via the method in the pydantic model
        self._display_data_cache = data_model.calc_display_data()
        # 3. Update the hash
        self.cache_update_hash = hash_it_up(data_model.calc_display_data)
        db.session.commit()
        return self._display_data_cache

    def display(self):
        """
        Gives the display data to the pydantic models for conversion to html
        Cached data takes precedence over raw data. 
        Ex: if display_data_cache and data both have the same key, display_data_cache overwrites data's value
        """
        DataModel = get_model(self.data_version, self.type)
        cached_data = self.display_data_cache if self.display_data_cache is not None else {}
        return DataModel.html_display(self.data | cached_data)

    def to_dict(self, loc_abbv=False):
        return {
            'module': self.module.serial_number if self.module else None,
            'component': self.component.serial_number if self.component else None, 
            'component_pos': self.component_pos, 
            'type': self.type,
            'version': self.data_version,
            'location': self.location.name_abbv if loc_abbv else self.location.name_long,
            'measurement_date': self.measurement_date,
            'user_created': self.user_created.username,
            'data': self.data
        }
    def to_data_model(self, loc_abbv=False):
        #will do unnecessary db checks
        full_dict = {k: v for k, v in self.to_dict(loc_abbv=loc_abbv).items() if v is not None}
        return ConstrModel.validate_python(full_dict)

    @classmethod
    def from_dict(cls, data):
        from application.errors.handlers import RecordConstructionError
        kwargs = {}
        if 'module' in data and data['module'] is not None:
            kwargs['module'] = Module.get_by_sn(data['module'])
            if kwargs['module'] is None:
                raise RecordConstructionError(f"The given module ({data['module']}) was not found in the database.")
        if 'component' in data and data['component'] is not None:
            kwargs['component'] = Component.get_by_sn(data['component'])
            if kwargs['component'] is None:
                raise RecordConstructionError(f"The given component ({data['component']}) was not found in the database.")
        if 'component_pos' in data and data['component_pos'] is not None:
            kwargs['component_pos'] = int(data['component_pos'])
        if 'measurement_date' in data and data['measurement_date'] is not None:
            kwargs['measurement_date'] = data['measurement_date']
        if 'location' in data and data['location'] is not None:
            kwargs['location'] = Location.get_by_either(data['location'])
            if kwargs['location'] is None:
                raise RecordConstructionError(f"The given location ({data['location']}) was not found in the database.")
        if 'type' in data and data['type'] is not None:
            kwargs['type'] = data['type']
        if 'user_created' in data and data['user_created'] is not None:
            kwargs['user_created'] = User.get_by_username(data['user_created'])
            if kwargs['user_created'] is None:
                raise RecordConstructionError(f"The given user ({data['user_created']}) was not found in the database.")
        if 'version' in data:
            kwargs['data_version'] = data['version']
        return cls(**kwargs)

    @validates("measurement_date")
    def validate_db_datetime(self, key, measurement_date):
        return validate_datetime(measurement_date)

class Assembly(db.Model, UtilityMixin, TimeStampMixin, MTDdbSyncMixin, ConstructionBaseMixin, DeprecateMixin):
    __tablename__ = "assembly"
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True, index=True)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    module_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('module.id', ondelete="CASCADE"), nullable=True, index=True)
    component_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('component.id', ondelete="CASCADE"), nullable=True, index=True)
    location_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False, index=True)

    #Relationships
    module: Mapped["Module"] = relationship(back_populates="assembly") #parent (module) in child class (assembly), backpopulates should have same name as relationship in parent(modules)
    component: Mapped[List["Component"]] = relationship(back_populates="assembly")
    location: Mapped["Location"] = relationship(back_populates="assembly")

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])
    #files: Mapped[List["File"]] = relationship(secondary=file_assembly_list, back_populates="assemblies", cascade="all, delete", passive_deletes=True)
    
    def __repr__(self) -> str:
        return f'<Assembly {self.id}>'

class Test(db.Model, UtilityMixin, TimeStampMixin, MTDdbSyncMixin, ConstructionBaseMixin, DeprecateMixin):
    __tablename__ = "test"
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True, index=True)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    module_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('module.id', ondelete="CASCADE"), nullable=True, index=True)
    component_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('component.id', ondelete="CASCADE"), nullable=True, index=True)
    location_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False, index=True)

    #Relationships
    module: Mapped["Module"] = relationship(back_populates="test") #parent (module) in child class (test), backpopulates should have same name as relationship in parent(modules)
    component: Mapped["Component"] = relationship(back_populates="test") #parent (module) in child class (test), backpopulates should have same name as relationship in parent(modules)
    location: Mapped["Location"] = relationship(back_populates="test")

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])

    def __repr__(self) -> str:
        return f'<Test {self.id}>'    

    #static method to render the step in html -f strings or have a template file
Index("idx_is_deprecated_test_type", Test.type, Test.is_deprecated)
Index("idx_is_deprecated_assembly_type", Assembly.type, Assembly.is_deprecated)


#-------------SENSOR COMPONENTS------------------#

#join table for the shipment of parts
shipping_list_modules = Table(
    #table for all the items getting shipped for each shipment
    "shipping_list_modules",
    db.Model.metadata,
    Column("module_id", ForeignKey("module.id"), primary_key=True), #just need component ids because you can infer module id if it is associated!
    Column("shipment_id", ForeignKey("shipment.id"), primary_key=True),
)

#module pcb table to track the module pcb
class Module(db.Model, UtilityMixin, TimeStampMixin ,MTDdbSyncMixin, MTDdbBarcodeMixin, DeprecateMixin): #hopefully the design is this is the parent to many other tables
    __tablename__ = "module"
    #-----IMPORTANT!!!!!
    #---------FOR MTDDB INTEGRATION ADD A ID MIRROR COLUMN THAT CAN BE SAVED BETWEEN DB START UPS!!!!!!!!!!!

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)    
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    module_type_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('module_type.id'), nullable=False, index=True)
    serial_number: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    assembly_location_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False)
    current_location_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False)

    #Relationships
    #bidirectional one to many relationship (this is in parent class with children relationship defined here)
    #many assemblies/tests can have one module and one module can have many assembly/tests
    #    children: Mapped[List["Child"]] = relationship(back_populates="parent")
    assembly: Mapped[List["Assembly"]] = relationship(back_populates="module", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    test: Mapped[List["Test"]] = relationship(back_populates="module", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    observations: Mapped[List["Observation"]] = relationship(back_populates="module", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    components: Mapped[List["Component"]] = relationship(back_populates="module", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    #dont want cascade delete on component, if a module is deleted you still might want the components!
    #Many to Many
    shipments: Mapped[List["Shipment"]] = relationship(secondary=shipping_list_modules, back_populates="modules")
    
    # Module Type relationship - Module type is parent and module is child
    #    parent: Mapped["Parent"] = relationship(back_populates="children")
    module_type: Mapped["ModuleType"] = relationship(back_populates="modules")
    assembly_location: Mapped["Location"] = relationship(back_populates="module_assemblies", foreign_keys=[assembly_location_id])
    current_location: Mapped["Location"] = relationship(back_populates="modules", foreign_keys=[current_location_id])

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])
    
    @property
    def is_recieved(self) -> bool:
        from application.shipments.logistics import calc_current_location
        return not (calc_current_location(self) == Location.get_by_either('Transit'))
    
    @staticmethod
    def get_by_sn(serial_number: str):
        return Module.query.filter_by(serial_number=serial_number).first()
    
    def comp_by_pos(self, pos:str):
        for comp in self.components:
            if comp.component_pos == pos:
                return comp

    def to_dict(self):
        data = {
            'id': self.id,
            'user_created': self.user_created.username,
            'serial_number': self.serial_number,
            'module_type': self.module_type.name_long,
            'assembly_location': self.assembly_location.name_long,
            'components': [comp.serial_number for comp in self.components],
            'current_location': self.current_location.name_long,
            'created_at': self.created_at,
            'data': {
                'assemblies': [assy.id for assy in self.assembly],
                'tests': [test.id for test in self.test],
                'observations': [obs.id for obs in self.observations],
                'shipments': [ship.id for ship in self.shipments]                
            }
        }
        return data
        
    def to_mtddb_xml(self):
        part = etree.Element('PART', mode='auto')
        etree.SubElement(part, 'KIND_OF_PART').text = self.module_type.name_long
        etree.SubElement(part, 'BARCODE').text = self.mtddb_barcode
        etree.SubElement(part, 'RECORD_INSERTION_USER').text = self.user_created.username
        etree.SubElement(part, 'LOCATION').text = self.current_location.name_long

        children = etree.SubElement(part,'CHILDREN')
        for comp in self.components:
            child_part = etree.SubElement(children,'PART')
            etree.SubElement(child_part,'BARCODE').text = comp.mtddb_barcode

            attributes = etree.SubElement(child_part, 'PREDEFINED_ATTRIBUTES')
            attribute_element = etree.SubElement(attributes, 'ATTRIBUTE')
            etree.SubElement(attribute_element, 'NAME').text = 'Module Position on PCB'
            etree.SubElement(attribute_element, 'VALUE').text = comp.component_pos
        return part #etree.tostring(part, pretty_print=True, xml_declaration=False, encoding='UTF-8').decode()

    def __repr__(self) -> str:
        return f'<Module {self.serial_number}>'

#join table for the shipment of parts
shipping_list_components = Table(
    #table for all the items getting shipped for each shipment
    "shipping_list_components",
    db.Model.metadata,
    Column("component_id", ForeignKey("component.id"), primary_key=True),
    Column("shipment_id", ForeignKey("shipment.id"), primary_key=True),
)

#ETROCs, LGADs, Baseplates etc... 
class Component(db.Model, UtilityMixin, TimeStampMixin, MTDdbSyncMixin, MTDdbBarcodeMixin, DeprecateMixin):
    __tablename__ = "component"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)  

    module_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('module.id', ondelete="CASCADE"), nullable=True)
    parent_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('component.id', ondelete="CASCADE"), nullable=True) #this gives the parent part in the component table if it has one
    current_location_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False)

    #this gets if its an ETROC, LGAD, etc, any version or type you want
    component_type_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('component_type.id'), nullable=False, index=True)

    #unique serial number for component
    serial_number: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    #4,3,2,1 positions on the module for example and 0 for the Module PCB in the final state module
    component_pos: Mapped[int] = mapped_column(db.Integer,unique=False, nullable=True)
    #the time when this component became associated with a module
    associated_on: Mapped[datetime] = mapped_column(db.DateTime,nullable=True)
    meta_data: Mapped[JSONB] = mapped_column(JSONB,nullable=True)
    vendor: Mapped[str] = mapped_column(db.String(50), unique=False, nullable=True)

    #Relationships
    #parent relationship in child class
    module: Mapped["Module"] = relationship(back_populates="components") #parent(module) in child class(component), backpopulates should have same name as relationship in parent(modules)
    components: Mapped[List["Component"]] = relationship("Component", back_populates="parent", cascade="all, delete", passive_deletes=True)
    parent: Mapped["Component"] = relationship("Component", back_populates="components", remote_side=[id])

    #bidirectional one to many relationship (this is in parent class with children relationship defined here)
    assembly: Mapped[List["Assembly"]] = relationship(back_populates="component", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    test: Mapped[List["Test"]] = relationship(back_populates="component", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    observations: Mapped[List["Observation"]] = relationship(back_populates="component", cascade="all, delete", passive_deletes=True) #children relationship in parent class
    component_type: Mapped["ComponentType"] = relationship(back_populates="components")

    current_location: Mapped["Location"] = relationship(back_populates="components")

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])
    #many to many relationships
    shipments: Mapped[List["Shipment"]] = relationship(secondary=shipping_list_components, back_populates="components")    

    @property
    def is_recieved(self):
        from application.shipments.logistics import calc_current_location
        return not (calc_current_location(self) == Location.get_by_either('Transit'))
    
    @property
    def parent_sn(self):
        #Mostly just for admin view of the tables
        parent_id = self.parent_id
        parent_comp = Component.get_by_id(parent_id)
        if parent_comp:
            return parent_comp.serial_number
    
    def to_mtddb_xml(self):
        part_elem = etree.Element('PART', mode='auto')
        etree.SubElement(part_elem, 'KIND_OF_PART').text = self.component_type.name_long
        etree.SubElement(part_elem, 'BARCODE').text = self.mtddb_barcode
        etree.SubElement(part_elem, 'RECORD_INSERTION_USER').text = self.user_created.username
        etree.SubElement(part_elem, 'LOCATION').text = self.current_location.name_long

        #these two might not be allowed for subassembly registration but I hope it allows it!
        etree.SubElement(part_elem, 'BATCH_NUMBER').text = "5" #TODO: IMPLEMENT THIS AS SOMEHTING???? Not sure if it is needed yet...
        etree.SubElement(part_elem, 'MANUFACTURER').text = self.vendor
        if self.components: #meaning it is probably a subassembly with components as children
            children = etree.SubElement(part_elem,'CHILDREN')
            for comp in self.components:
                child_part = etree.SubElement(children,'PART')
                etree.SubElement(child_part,'BARCODE').text = comp.mtddb_barcode
        return part_elem #.tostring(part, pretty_print=True, xml_declaration=False, encoding='UTF-8').decode()
    
    def to_dict(self): #add created at!
        data = {
            'id': self.id,
            'user_created': self.user_created.username,
            'serial_number': self.serial_number,
            'component_type': self.component_type.name_long,
            'associated_module': self.module.serial_number if self.module else None,
            'parent': self.parent.serial_number if self.parent else None,
            'subcomponents': [comp.serial_number for comp in self.components],
            'component_pos': self.component_pos,
            'current_location': self.current_location.name_long,
            'created_at': self.created_at,
            'metadata': self.meta_data,
            'vendor': self.vendor,
            'data': {
                'assemblies': [assy.id for assy in self.assembly],
                'tests': [test.id for test in self.test],
                'observations': [obs.id for obs in self.observations],
                'shipments': [ship.id for ship in self.shipments]                
            }
        }
        return data

    @staticmethod
    def get_by_sn(serial_number):
        return Component.query.filter_by(serial_number=serial_number).first()
    
    def __repr__(self) -> str:
        return f'<Component {self.serial_number}>'
    
#---------------------------------#     #~~~~~#     #------------------SHIPMENT TABLE----------------------#

class Shipment(db.Model, UtilityMixin, TimeStampMixin, DeprecateMixin):
    __tablename__ = "shipment"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)  
    origin_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False)
    destination_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False)

    tracking_number: Mapped[str] = mapped_column(db.String(50), unique=False, nullable=True)
    shipment_date: Mapped[datetime] = mapped_column(db.DateTime,nullable=False)
    reception_date: Mapped[datetime] = mapped_column(db.DateTime,nullable=True) #has to be nullable because it wont be given at first
    shipment_method: Mapped[str] = mapped_column(db.String(50), unique=False, nullable=True) #do i reallllly need to make a look up table for these? I say no

    #relationships
    origin = relationship("Location", foreign_keys=[origin_id])
    destination = relationship("Location", foreign_keys=[destination_id])

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])

    components: Mapped[List["Component"]] = relationship(secondary=shipping_list_components)
    modules: Mapped[List["Module"]] = relationship(secondary=shipping_list_modules)
    
    def to_dict(self): #add created at!
        data = {
            'id': self.id,
            'user_created': self.user_created.username,
            'origin': self.origin.name_long if self.origin else None,
            'destination': self.destination.name_long,
            'tracking_number': self.tracking_number,
            'shipment_method': self.shipment_method,
            'shipment_date': self.shipment_date,
            'reception_date': self.reception_date,
            'modules': [m.serial_number for m in self.modules],
            'components': [c.serial_number for c in self.components]
        }
        return data
    @property
    def is_recieved(self):
        if self.reception_date:
            return True
        else:
            return False

    def __repr__(self) -> str:
        return f'<Shipment {self.tracking_number}>'
    

#event listener for after commit of a shipment to calc curr loc of parts and set it?
#-------------------------------------------------#

class Observation(db.Model, UtilityMixin, TimeStampMixin):
    __tablename__ = "observation"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)  
    module_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('module.id', ondelete="CASCADE"), nullable=True)
    component_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('component.id', ondelete="CASCADE"), nullable=True)
    location_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('location.id'), nullable=False)

    comment: Mapped[str] = mapped_column(db.String, nullable=True)
    #obs_date: Mapped[datetime] = mapped_column(db.DateTime, nullable=True)

    #class property or method to split image blob into image sizes?

    #Relationships
    module: Mapped["Module"] = relationship(back_populates="observations") #parent (module) in child class (observation), backpopulates should have same name as relationship in parent(modules)
    component: Mapped["Component"] = relationship(back_populates="observations") #parent (module) in child class (obsservation), backpopulates should have same name as relationship in parent(component)
    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])
    observation_location: Mapped["Location"] = relationship(back_populates="observation")
    files: Mapped[List["File"]] = relationship(back_populates="observation", cascade="all, delete", passive_deletes=True)
    
    def to_dict(self): #add created at!
        data = {
            'id': self.id,
            'user_created': self.user_created.username,
            'module': self.module.serial_number if self.module else None,
            'component': self.component.serial_number if self.component else None,
            'observation_location': self.observation_location.name_long,
            'comment': self.comment,
            'files': [f.id for f in self.files],
            'created_at': self.created_at
        }
        return data

    def __repr__(self) -> str:
        return f'<Observation {self.id}>'

#----------File Uploads------------#

class File(db.Model, UtilityMixin, TimeStampMixin):
    __tablename__ = "file"
    #try to not use but for some cases and developement we support direct file uploads to db

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)   
    filename: Mapped[str] = mapped_column(db.String, unique=False, nullable=False)
    uuid_filename: Mapped[str] = mapped_column(db.String, unique=False, nullable=False)
    mime_type: Mapped[str] = mapped_column(db.String, unique=False, nullable=False)
    data: Mapped[bytes] = mapped_column(db.LargeBinary, unique=False, nullable=True, deferred=True) #deffered makes it not look at the files when pulling files, cuz it could be big and slow!
    storage_type: Mapped[str] = mapped_column(db.String, default=the_config.FILE_STORE, unique=False, nullable=False) #DB, LOCAL, or CLOUD -> where it is currently stored, if LOCAL file will be gone...
    
    #keeping many to one for observation. Many files for one observation
    observation_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('observation.id', ondelete="CASCADE"), nullable=True)

    #Relationships
    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])
    observation: Mapped["Observation"] = relationship(back_populates="files")

    @staticmethod
    def get_by_uuid(uuid_filename):
        return File.query.filter_by(uuid_filename=uuid_filename).first()
    
    def delete_remote(self):
        uuid_filename = self.uuid_filename
        if the_config.FILE_STORE == 'CLOUD':
            options = {
            'webdav_hostname': the_config.WEBDAV_HOSTNAME,
            'webdav_login': the_config.WEBDAV_LOGIN,
            'webdav_password': the_config.WEBDAV_PASSWORD
            }
            client = Client(options)
            #remote path
            remote_path = the_config.WEBDAV_DIRECTORY + uuid_filename
            try:
                client.clean(remote_path)
            except WebDavException as e:
                return e
            
    def get_remote(self):
        uuid_filename = self.uuid_filename
        if the_config.FILE_STORE == 'CLOUD':
            options = {
            'webdav_hostname': the_config.WEBDAV_HOSTNAME,
            'webdav_login': the_config.WEBDAV_LOGIN,
            'webdav_password': the_config.WEBDAV_PASSWORD
            }
            client = Client(options)
            #remote path
            remote_path = path.join(the_config.WEBDAV_DIRECTORY, uuid_filename)
            buff = BytesIO()
            try:
                client.download_from(remote_path=remote_path, buff=buff)
                buff.seek(0)
                return buff
            except WebDavException as e:
                return e  
  

    def to_dict(self):
        #https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xxiii-application-programming-interfaces-apis

        data = {
            'id': self.id,
            'user_created': self.user_created.username if self.user_created else None,
            'filename': self.filename, #obviously dont let this be updated
            'mime_type': self.mime_type,
            'uuid_filename': self.uuid_filename,
            'data': self.data,
            'storage_type': self.storage_type,
            'created_at': self.created_at,
            'assemblies': [assy.id for assy in self.assemblies],
            'tests': [test.id for test in self.tests],
            'observation': self.observation_id
        }
        return data
    
    @classmethod
    def from_dict(cls, data, new_file=False):
        kwargs = {}
        for field in ['filename', 'uuid_filename', 'mime_type', 'data', 'storage_type']: #dont allow the ability to set the site yet, will require other work
            if field in data:
                kwargs[field] = data[field]
        if 'user_created' in data:
            kwargs['user_created'] = User.get_by_username(data['user_created'])
        if 'created_at' not in data and new_file: 
            kwargs['created_at'] = now_utc()
        elif 'created_at' in data and new_file:
            #TODO HANDLE DATETIME OBJ AND CONVERTING TO DATETIME OBJECT FROM INPUTTED JSON FORM
            kwargs['created_at'] = data['created_at']
        return cls(**kwargs)
    
    def __repr__(self) -> str:
        return f'<File {self.id}>'

#add on delete event that checks the storage
@event.listens_for(File,'after_delete')
def after_delete_listener(_, __, target):
    if hasattr(target, "delete_remote"):
        target.delete_remote()



if the_config.PROFILE_SQL:
    #from sqlalchemy import event
    #from sqlalchemy.engine import Engine
    import time
    import logging
    from application import app
    app.app_context().push()

    logging.basicConfig()
    logger = logging.getLogger("myapp.sqltime")
    logger.setLevel(logging.DEBUG)

    @event.listens_for(db.engine, "before_cursor_execute")
    def before_cursor_execute(conn, cursor, statement,
                                parameters, context, executemany):
        conn.info.setdefault('query_start_time', []).append(time.time())
        logger.debug("Start Query: %s", statement)

    @event.listens_for(db.engine, "after_cursor_execute")
    def after_cursor_execute(conn, cursor, statement,
                                parameters, context, executemany):
        total = time.time() - conn.info['query_start_time'].pop(-1)
        logger.debug("Query Complete!")
        logger.debug("Total Time: %f", total)



#----------Lookup Tables-----------#

class ModuleType(db.Model, UtilityMixin, ByNameMixin, TimeStampMixin, MTDdbZZCodeMixin, MTDdbRequiredMixin):
    __tablename__ = "module_type"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)    
    name_long: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    name_abbv: Mapped[str] = mapped_column(db.String(4), unique=True, nullable=False)
    number_of_components: Mapped[int] = mapped_column(db.Integer, unique=False, nullable=False)
    description: Mapped[str] = mapped_column(db.String, unique=False, nullable=True)

    #Relationships
    #the children relationships
    modules: Mapped[List["Module"]] = relationship(back_populates="module_type") #One to Many: Parent(Module Type), Child(Module)

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])

    def to_dict(self):
        return {
            'name_long':self.name_long,
            'name_abbv':self.name_abbv,
            'number_of_components':self.number_of_components,
            'description': self.description
        }
    
    def __repr__(self) -> str:
        return f'<{self.name_long}>'


class ComponentType(db.Model, UtilityMixin, ByNameMixin, TimeStampMixin, MTDdbZZCodeMixin, MTDdbRequiredMixin):
    __tablename__ = "component_type"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True) 
    user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True) 
    name_long: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    name_abbv: Mapped[str] = mapped_column(db.String(4), unique=True, nullable=False)
    required_keys: Mapped[ARRAY] = mapped_column(ARRAY(db.String),nullable=True)
    description: Mapped[str] = mapped_column(db.String, unique=False, nullable=True)
    is_subassembly: Mapped[bool] = mapped_column(default=False, nullable=True)

    #Relationships
    components: Mapped[List["Component"]] = relationship(back_populates="component_type") #One to Many: Parent(Component Type), Child(Component)

    user_created = relationship("User", foreign_keys=[user_created_id])
    user_edited = relationship("User", foreign_keys=[user_edited_id])

    def to_dict(self):
        return {
            'name_long':self.name_long,
            'name_abbv':self.name_abbv,
            'required_keys':self.required_keys,
            'description': self.description

        }

    def __repr__(self) -> str:
        return f'<{self.name_long}>'

#---------------------------------#

class Location(db.Model,UtilityMixin, ByNameMixin, TimeStampMixin):
    __tablename__ = "location"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)
    #removed for now due to inability to resolve the relationship directions between user and location
    # user_created_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    # user_edited_id: Mapped[int] = mapped_column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    name_long: Mapped[str] = mapped_column(db.String(50), unique=True, nullable=False)
    name_abbv: Mapped[str] = mapped_column(db.String(3), unique=True, nullable=False)
    description: Mapped[str] = mapped_column(db.String, unique=False, nullable=True)

    #Relationships
    assembly: Mapped[List["Assembly"]] = relationship(back_populates="location") #One to Many: Parent(Location), Child(Assembly)
    test: Mapped[List["Test"]] = relationship(back_populates="location") #One to Many: Parent(Location), Child(Assembly)
    module_assemblies: Mapped[List["Module"]] = relationship(back_populates="assembly_location", foreign_keys=[Module.assembly_location_id]) #One to Many: Parent(Location), Child(Component)
    modules: Mapped[List["Module"]] = relationship(back_populates="current_location", foreign_keys=[Module.current_location_id]) #One to Many: Parent(Location), Child(Component)
    components: Mapped[List["Component"]] = relationship(back_populates="current_location")

    # shipment_origin: Mapped[List["Shipment"]] = relationship(back_populates="origin") #One to Many: Parent(Location), Child(Shipment)
    # shipment_destination: Mapped[List["Shipment"]] = relationship(back_populates="destination") #One to Many: Parent(Location), Child(Shipment)
    observation: Mapped[List["Observation"]] = relationship(back_populates="observation_location")
    users: Mapped[List["User"]] = relationship(back_populates="site", foreign_keys=[User.site_loc_id])
    
    # user_created = relationship("User", foreign_keys=[user_created_id])
    # user_edited = relationship("User", foreign_keys=[user_edited_id])
    
    def to_dict(self):
        return {
            'name_long':self.name_long,
            'name_abbv':self.name_abbv,
            'description': self.description
        }

    def __repr__(self) -> str:
        return f'<Location {self.name_long}>'


class Event(db.Model, UtilityMixin):
    __tablename__ = "event"

    id: Mapped[int] = mapped_column(db.Integer, autoincrement=True, primary_key=True)  
    timestamp: Mapped[datetime] = mapped_column(default=now_utc, nullable=False)
    category: Mapped[str] = mapped_column(db.String(50), unique=False, nullable=False)
    severity: Mapped[str] = mapped_column(db.String(50), unique=False, nullable=False)
    message: Mapped[str] = mapped_column(db.String, unique=False, nullable=False)

    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"

    LOW="LOW"
    MEDIUM="MEDIUM"
    HIGH="HIGH"

    @staticmethod
    def record_event(category: str, severity: str, message: str):
        new_event = Event(
            category=category,
            severity=severity,
            message=message,
        )
        db.session.add(new_event)
        return message

    @staticmethod
    def record_exception(exception: Exception):
        return Event.record_event('LOGGED_EXCEPTION', Event.ERROR, str(exception))




#eventually I wonder if you can use events to automatically sync db?? Probably is too slow to want to do
