from application.auth import auth_bp #get flask app
from application import db, oauth, the_config, yag, login_manager
from application import db_models as mdl
from application.datetime_handler import now_utc
from flask import render_template, flash, redirect, url_for, request, session
from .forms import UpdateProfileForm
from flask_login import current_user, login_user, logout_user, login_required

@login_manager.user_loader
def load_user(id_):
    return mdl.User.query.get(int(id_))

@auth_bp.route('/login')
def login():
    #make previous url the url they came from if it was inside the website, if not make it home page
    if request.referrer is not None and request.url_root in request.referrer:
        session["previous_url"] = request.referrer
    else:
        session["previous_url"] = url_for('main.home')
        
    if current_user.is_authenticated: #handy Flask-Login functions
        #if logged in user navigates to login page, send them back home
        return redirect(session['previous_url'])
    
    if the_config.DISABLE_CERN_SSO:
        user = db.session.scalar(db.select(mdl.User).where(mdl.User.username=='local_user'))
        if not user:
            user = mdl.User(email='email', username = 'local_user', is_admin = True)
            db.session.add(user)
            db.session.commit()
        login_user(user)
        return redirect(url_for('main.home'))
    redirect_uri = url_for('auth.authenticate', _external=True)
    return oauth.cern.authorize_redirect(redirect_uri)

@auth_bp.route('/')
def authenticate():
    #This exchanges the authorization code for an access token?
    token = oauth.cern.authorize_access_token()
    profile = token.get('userinfo', None)
    if profile is None:
        flash("Profile did not come in request from CERN, try signing in again", category="warning")
        return redirect(session["previous_url"])
    roles = profile.get('resource_access', {}).get(the_config.CLIENT_ID, {}).get('roles', None)
    if roles is None:
        flash("User role did not come in request from CERN", category="warning")
        return redirect(session["previous_url"])
    elif 'app-users' not in roles:
        flash("You are not part of the app egroup and therefore cannot authorize login. Please send an email to the admins for acces at: cms-etl-construction-web-app@cern.ch", category="warning")
        return redirect(session["previous_url"])
    
    #need a way to shut down an account, through admin and flag
    username = profile.get('preferred_username', None)
    user = db.session.scalar(db.select(mdl.User).where(mdl.User.username==username))
    if user is None:
        user = mdl.User(
            email=profile.get('email', None), 
            username = username,
            is_admin = ('root-user' in roles)
        )
        #add admin logic here...
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('auth.update_profile'))
    login_user(user)
    return redirect(session["previous_url"])

@auth_bp.route('/logout')
def logout():
    #removes them from current users
    logout_user()
    return redirect(session.get("previous_url", url_for("main.home")))

@auth_bp.route('/update-profile', methods=['GET','POST'])
@login_required
def update_profile():
    form = UpdateProfileForm()
    undefined_loc = mdl.Location.get_by_long_name('Undefined')
    transit_loc= mdl.Location.get_by_long_name('Transit')
    location_choices = [(str(loc.id), loc.name_long) for loc in mdl.Location.query.order_by('id') if loc.id != transit_loc.id]
    first_choice = [('None','-- Select A Location --')]
    form.site.choices = first_choice + location_choices
    if form.validate_on_submit():
        email = form.email.data
        site = mdl.Location.get_by_id(form.site.data)
        if email:
            current_user.email = email
        if site:
            current_user.site = site

        current_user.user_edited = current_user
        current_user.last_edited = now_utc()

        mdl.Event.record_event(mdl.Event.INFO,mdl.Event.LOW,f'{current_user.username} updated their profile')
        db.session.commit()
        
        return redirect(url_for('auth.login'))

    return render_template('update_profile.html', title='Update Profile', form=form)