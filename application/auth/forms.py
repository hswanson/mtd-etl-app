from flask_wtf import FlaskForm

from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import Email, Optional
from application import db_models as mdl
from application.form_utils import ValidateDB, coerce_none


#-------Forms---------#
class UpdateProfileForm(FlaskForm):
    email = StringField('Email', validators=[Optional(), Email(), ValidateDB(mdl.User,'email', in_db=False, optional=True)], render_kw={'placeholder':'Email','class': "form-control"})
    site = SelectField('Site', validators=[Optional(), ValidateDB(mdl.Location,'id',in_db=True, optional=True)], coerce=coerce_none, render_kw={'class': "form-control"})
    submit = SubmitField('Update')