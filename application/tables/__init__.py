from flask import Blueprint

tables_bp = Blueprint('tables', __name__, template_folder='templates',static_folder='static')

from application.tables import routes