from application import db
from application import db_models as mdl
from flask import url_for
import json
from datetime import datetime
from typing import Union


def get_nested_attr(obj, attrs: list):
    for attr in attrs:
        obj = getattr(obj, attr, None)
    return obj


def get_module(db_obj):
    if db_obj.module:
        return db_obj.module.serial_number
    if db_obj.parent:
        return get_nested_attr(db_obj, ['parent', 'module', 'serial_number'])

def construction_row(db_obj: Union[mdl.Test, mdl.Assembly]) -> dict:
    # file_url = url_for('main.get_file', file_ids = [db_obj.file.id])
    # files_html = f'<a href="{file_url}">{db_obj.file.filename}</a>'
    return {
        'id':               db_obj.id,
        'username':         get_nested_attr(db_obj, ['user_created', 'username']),
        'type':             db_obj.type,
        'location':         get_nested_attr(db_obj, ['location', 'name_long']),
        'measurement_date': db_obj.measurement_date,
        'module_type':      get_nested_attr(db_obj, ['module', 'module_type', 'name_long']),
        'module_sn':        get_nested_attr(db_obj, ['module', 'serial_number']),
        'component_pos':    db_obj.component_pos,
        'component_sn':     get_nested_attr(db_obj, ['component', 'serial_number']),
        'component_type':   get_nested_attr(db_obj, ['component', 'component_type', 'name_long']),
    }

def module_row(db_obj: mdl.Module) -> dict:
    return {
        'id': db_obj.id,
        'username':          get_nested_attr(db_obj, ['user_created', 'username']),
        'module_sn':         db_obj.serial_number,
        'module_type':       get_nested_attr(db_obj, ['module_type','name_long']),
        'current_location':  get_nested_attr(db_obj, ['current_location', 'name_long']),
        'assembly_location': get_nested_attr(db_obj, ['assembly_location', 'name_abbv']),
        'created_at':        db_obj.created_at
    }
    
def component_row(db_obj: mdl.Component) -> dict:
    return {
        'id':               db_obj.id,
        'username':         get_nested_attr(db_obj, ['user_created', 'username']),
        'component_sn':     db_obj.serial_number,
        'component_type':   get_nested_attr(db_obj, ['component_type','name_long']),
        'parent_sn':        get_nested_attr(db_obj, ['parent', 'serial_number']),
        'subcomponents':    ', '.join([comp.serial_number for comp in db_obj.components]),
        'module_sn':        get_module(db_obj),
        'meta_data':        json.dumps(db_obj.meta_data),
        'current_location': get_nested_attr(db_obj, ['current_location', 'name_long']),
        'created_at':       db_obj.created_at
    }

def shipment_row(db_obj: mdl.Shipment) -> dict:
    all_parts = ', '.join([comp.serial_number for comp in db_obj.components] + [mod.serial_number for mod in db_obj.modules])
    parts = f'<p title="{all_parts}">{len(db_obj.components)} Components and {len(db_obj.modules)} Modules</p>'
    return {
        'id': db_obj.id,
        'username':        get_nested_attr(db_obj, ['user_created', 'username']),
        'origin':          get_nested_attr(db_obj, ['origin', 'name_long']),
        'destination':     get_nested_attr(db_obj, ['destination', 'name_long']),
        'shipment_date':   db_obj.shipment_date,
        'reception_date':  db_obj.reception_date,
        'parts':           parts,
        'tracking_number': db_obj.tracking_number,
        'is_recieved':     db_obj.is_recieved
    }

def table_data(tablename:str, results) -> list[dict]:        
    if tablename == 'test' or tablename=='assembly':
        return [construction_row(q) for q in results]
    elif tablename == 'shipment':
        return [shipment_row(q) for q in results]
    elif tablename == 'module':
        return [module_row(q) for q in results]
    elif tablename == 'component':
        return [component_row(q) for q in results]
    else:
        return NotImplementedError("This table is not implemented sorry!")