from application.tables import tables_bp
from application import db
from application import db_models as mdl
from application.datetime_handler import now_utc
from application.tables import search, loader
from application.tables.paginate import paginate
from flask_login import login_required, current_user
from flask import request, abort, jsonify, session, render_template, flash, redirect, url_for
from sqlalchemy import text, func, literal_column
from sqlalchemy.exc import DataError
from sqlalchemy.orm import aliased, load_only
import json

@tables_bp.route('/component-table')
def component_table():
    all_components = [comp.serial_number for comp in mdl.Component.query]
    return render_template('component_table.html', title='Component Inventory', db_tablename='component', all_components=all_components)

@tables_bp.route('/module-table')
def module_table():    
    all_modules = [mod.serial_number for mod in mdl.Module.query]
    return render_template('module_table.html', title='Module Table', db_tablename='module', all_modules=all_modules, default_search='')

@tables_bp.route('/<constr_table>')
def construction_table(constr_table):
    return render_template('construction_table.html',title=f'{constr_table} Table'.capitalize(), constr_table=constr_table)

@tables_bp.route('/shipment-table/')
def shipment_table():
    return render_template('shipment_table.html', title='Shipment Table', db_tablename='shipment')


@tables_bp.route('/api/table_data/<db_tablename>')
def table_data(db_tablename):
    # Ref: https://blog.miguelgrinberg.com/post/beautiful-flask-tables-part-2
    Table = mdl.get_model_by_tablename(db_tablename)
    if db_tablename not in search.builder_map:
        flash('This table does not exists returning home', category='danger')
        return redirect(url_for('main.home'))
    
    query_director = search.QueryDirector(request.args.get('search'))
    query_builder = search.builder_map[db_tablename]()
    query_director.builder = query_builder 
    # these next 2 lines let people query by deprecation, overrides the default to filter by deprecated!
    if not any(search_key.lower() in search.DEPRECATION_QUERY_ALIASES for search_key, _ in query_director.grouped_searches):
        query_builder.query_deprecation(['False'], '=')
    query = query_director.build_query()

    #Store the raw SQL Query in the session, higher performance and space than storing ids
    session['table_query'] = str(query.compile(db.engine, compile_kwargs={"literal_binds": True}))
    starting_offset = request.args.get('start', type=int, default=-1) #-1 is for no query str params
    page_length = request.args.get('length', type=int, default=-1)

    if 'available_pages' not in session:
        session['available_pages'] = []
    
    try:
        ONE = literal_column("1")
        total_rows = db.session.execute(
            db.select(func.count(ONE)).select_from(query.subquery()).order_by(None)
        ).scalar_one()
        requested_rows, available_pages = paginate(Table, query, starting_offset, page_length, total_rows, session['available_pages'])
    except DataError:
        total_rows = 0
        requested_rows, available_pages = [], []
    session['available_pages'] = [page.model_dump() for page in available_pages]

    return {
        'data': loader.table_data(db_tablename, requested_rows),
        'total': total_rows,
    }

@tables_bp.route('/api/table_data/<db_tablename>', methods=['POST'])
@login_required
def update(db_tablename):
    data = request.get_json()
    if db_tablename == 'component':
        if 'id' not in data:
            abort(400)
        component = mdl.Component.get_by_id(data['id'])
        required_keys = component.component_type.required_keys
        try:
            meta_data = json.loads(data['meta_data'])
        except json.decoder.JSONDecodeError:
            return jsonify(message=f"Incorrect formatting for meta data ({data['meta_data']}), should be JSON formatted"), 400
        if set(required_keys) != set(meta_data.keys()):
            #there is the case where required keys changes for a component type after upload... but forcing it to conform to that is ok
            #abort(400, {'message': f'Cannot set metadata to be: {meta_data}, the allowed keys are: {set(required_keys)}'})
            return jsonify(message=f'Cannot set metadata to be: {meta_data}, the allowed keys are: {set(required_keys)}'), 400
        component.meta_data = meta_data
        db.session.commit()
    return '', 204

@tables_bp.route('/api/table_data/<db_tablename>/depricate', methods=['POST'])
@login_required
def depricate(db_tablename):
    query = session.get('table_query')
    if query is None:
        return jsonify(message=f'Try refreshing the table and search again. The query was not saved in the session.'), 400
    
    Table = mdl.get_model_by_tablename(db_tablename)
    orm_sql = db.select(Table).from_statement(text(query))

    for obj in db.session.execute(orm_sql).scalars():
        print(obj)
        obj.is_deprecated = not obj.is_deprecated #works even for null cases :)
        obj.deprecated_on = now_utc() #have it be a date, could be use to look at recently deprecated items to undepricate
        obj.deprecated_by = current_user
    db.session.commit()
    return '', 204
    