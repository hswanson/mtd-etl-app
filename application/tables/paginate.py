from application import db
from flask import session
import logging
from config import the_config
from pydantic import BaseModel, TypeAdapter

# logger = logging.getLogger(__name__)
# logging.basicConfig(level=the_config.LOGGING_MODE)

def chunk_list(db_rows, page_limit):
    """Yield successive n-sized chunks from lst."""
    #https://stackoverflow.com/questions/312443/how-do-i-split-a-list-into-equally-sized-chunks
    for i in range(0, len(db_rows), page_limit):
        yield db_rows[i:i + page_limit]

class Page(BaseModel, frozen=True):
    page_number: int
    start_id: int
    end_id: int

Pages = TypeAdapter(set[Page])

def get_final_page(query, table, total_entries: int, page_length: int):
    _, n_entries_overflow = divmod(total_entries, page_length)
    last_page_entries = db.session.execute(
        query.order_by(table.id.desc()).limit(page_length)
    ).scalars().all()
    last_page_entries = last_page_entries[::-1]

    last_page, left_over = divmod(total_entries, page_length)
    if left_over == 0 and last_page != 0: 
        last_page = last_page - 1
    return Page(
        page_number = last_page, #if left_over != 0 else last_page - 1
        start_id    = last_page_entries[-n_entries_overflow].id if last_page_entries else 0, 
        end_id      = last_page_entries[-1].id if last_page_entries else 0
    )

def get_prev_pages(query, table, start_id:int, page_number:int, num_pages:int=1, page_length:int=10) -> set[Page]:
    query_page = (
        query
        .where(table.id < start_id)
        .order_by(table.id.desc())
        .limit(page_length * num_pages)
    )
    pages_rows = db.session.execute(query_page).scalars().all()[::-1]
    next_pages = []
    for i, rows in enumerate(chunk_list(pages_rows, page_length)):
        page = Page(
            page_number=page_number - i,
            start_id=rows[0].id,
            end_id=rows[-1].id
        )
        next_pages.append(page)
    return Pages.validate_python(next_pages)

def get_next_pages(query, table, end_id:int, page_number:int, num_pages:int=1, page_length:int=10) -> set[Page]:
    query_page = (
        query
        .where(table.id > end_id)
        .order_by(table.id.asc())
        .limit(page_length * num_pages)
    )
    pages_rows = db.session.execute(query_page).scalars().all()
    next_pages = []
    for i, rows in enumerate(chunk_list(pages_rows, page_length)):
        page = Page(
            page_number=page_number + i,
            start_id=rows[0].id,
            end_id=rows[-1].id
        )
        next_pages.append(page)
    return Pages.validate_python(next_pages)

def get_requested_rows(query, table, requested_page):
    return db.session.execute(
        query.where(table.id.between(requested_page.start_id, requested_page.end_id)).order_by(table.id.asc())
    ).scalars().all()

def get_page(available_pages, page_number):
    for page in available_pages:
        #print(f"Checking page: {page} for page_number: {page_number}")
        if page.page_number == page_number:
            return page

def paginate(table, query, starting_offset:int, page_length:int, total_entries:int, available_pages:list[dict]):
    from application.errors.handlers import PaginationError
    requested_page_number = starting_offset // page_length

    available_pages = Pages.validate_python(available_pages)
    if starting_offset == 0 or not available_pages:
        available_pages = set()
        next_pages = get_next_pages(query, table, end_id=0, page_number=0, num_pages=3, page_length=page_length)
        available_pages = available_pages | next_pages
        available_pages.add(get_final_page(query, table, total_entries, page_length))    
    
    #print(f"AFTER NEW OFFSET: Available Pages: {available_pages}, requested_page_number: {requested_page_number}")
    current_page = get_page(available_pages, requested_page_number)
    if current_page is None:
        #print(f"NO CURRENT PAGE: Available Pages: {available_pages}, requested_page_number: {requested_page_number}")
        raise PaginationError(f"Requested page number {requested_page_number} does not exist in the current pages")

    if get_page(available_pages, requested_page_number + 1) is None:
        next_pages = get_next_pages(query, table, end_id=current_page.end_id, page_number=current_page.page_number + 1, num_pages=2, page_length=page_length)
        #print(f'Getting next pages: {next_pages}')
        available_pages = available_pages | next_pages
        if old_page := get_page(available_pages, requested_page_number - 3):
            #print(f"Removing old page: {old_page}")
            available_pages.remove(old_page)

    if get_page(available_pages, requested_page_number - 1) is None and requested_page_number-1 > 0:
        prev_pages = get_prev_pages(query, table, start_id=current_page.start_id, page_number=current_page.page_number - 1, num_pages=2, page_length=page_length)
        #print(f'Getting previous pages: {prev_pages}')
        available_pages = available_pages | prev_pages
        if old_page := get_page(available_pages, requested_page_number + 3):
            #print(f"Removing old page: {old_page}")
            available_pages.remove(old_page)

    requested_rows = get_requested_rows(query, table, current_page)
    return requested_rows, available_pages


#----end of table search functions-----#