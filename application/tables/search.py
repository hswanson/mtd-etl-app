from application import db_models as mdl
from application import db
from sqlalchemy.orm import aliased
from collections import defaultdict
from sqlalchemy.sql.selectable import Select
from sqlalchemy.sql.elements import BinaryExpression
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy import func as sql_func
from sqlalchemy import exists, select
from typing import Union, Literal
from functools import partial
import re

ID_QUERY_ALIASES = ('id')
DEPRECATION_QUERY_ALIASES = ('dep', 'depricated', 'deprecate')

DATA_QUERY_ALIASES = ('data')
METADATA_QUERY_ALIASES = ('meta', 'metadata')
ASSEMBLY_LOCATION_QUERY_ALIASES = ('assembly_loc', 'assembly_location', 'assy_loc', 'assy_location', 'assyloc')
CURRENT_LOCATION_QUERY_ALIASES = ('loc', 'location', 'current_location', 'curr_loc')

# Part type
COMPONENT_TYPE_QUERY_ALIASES = ('comptype', 'component type')
MODULE_TYPE_QUERY_ALIASES = ('modtype', 'module_type')

# Construction
COMPONENT_QUERY_ALIASES = ('comp', 'component', 'component_serial_number', 'comp_sn', 'compsn', 'component_sn')
MODULE_QUERY_ALIASES = ('mod', 'module', 'module_serial_number', 'mod_sn', 'modsn', 'module_sn')
CONSTR_TYPE_QUERY_ALIASES = ('type', 'test_type', 'assembly_type', 'assy_type', 'testtype', 'assytype', 'test', 'assembly', 'assy')

# Shipment
ORIGIN_QUERY_ALIASES = ('origin', 'origin_location', 'origin_loc', 'org')
DESTINATION_QUERY_ALIASES = ('dest', 'destination', 'destination_location', 'dest_loc')
TRACKING_NUMBER_QUERY_ALIASES = ('tracknum', 'tracking_number')
IN_TRANSIT_QUERY_ALIASES = ('transit','intransit', 'in_transit')

# MODULE
HAS_ASSEMBLY_QUERY_ALIASES = ('has_assembly','with_assembly', 'has_assy', 'with_assy')
HAS_TEST_QUERY_ALIASES = ('has_test','with_test')
LACKS_ASSEMBLY_QUERY_ALIASES = ('lacks_assembly', 'without_assembly', 'no_assembly', 'lacks_assy', 'without_assy', 'no_assy')
LACKS_TEST_QUERY_ALIASES = ('lacks_test', 'without_test', 'no_test')

def represents_float(s) -> bool:
    try: 
        float(s)
    except ValueError:
        return False
    else:
        return True

def is_all_same_type(search_values:list) -> bool:
    return all(isinstance(val, type(search_values[-1])) for val in search_values)

def convert_search_type(search_values:list[str]) -> tuple[Literal[db.String, db.Float], list]:   
    """
    When querying on json columns, we need to cast the type to bool or float, float might work for ints too
    Not confident...
    """ 
    db_type = db.String
    converted_searach_values = []
    for val in search_values:
        if represents_float(val):
            converted_searach_values.append(float(val))
            db_type = db.Float
        else:
            converted_searach_values.append(val)
            db_type = db.String

    if is_all_same_type(converted_searach_values):
        return db_type, converted_searach_values
    else:
        # default "safe" behaviour
        return db.String, search_values

def generate_json_column_conditions(column: InstrumentedAttribute, search_values: list[str], search_type: Literal['<','<=','=','>=','>']) -> BinaryExpression:
    db_type, search_values = convert_search_type(search_values)
    conditions = []
    for search_value in search_values:
        if isinstance(search_value, str) and '~' in search_value:
            search_value_split = search_value.split('~')
            if len(search_value_split) != 2:
                continue
            if search_type == '=':
                range_db_type, range_search_values = convert_search_type(search_value_split)
                if range_db_type == db.Float:
                    conditions.append(
                        db.or_(
                            column.cast(range_db_type).between(range_search_values[0], range_search_values[-1])
                        )
                    )
                elif range_db_type == db.String:
                    conditions.append(
                        db.or_(
                            column.cast(range_db_type).between(range_search_values[0].strip(), range_search_values[-1].strip())
                        )
                    )
                else:
                    continue
            else:
                continue
        elif search_type == '=':
            conditions.append(column.cast(db_type) == search_value)
        elif search_type == '>':
            conditions.append(column.cast(db_type) > search_value)
        elif search_type == '<':
            conditions.append(column.cast(db_type) < search_value)
        elif search_type == '>=':
            conditions.append(column.cast(db_type) >= search_value)
        elif search_type == '<=':
            conditions.append(column.cast(db_type) <= search_value)
    return db.or_(*conditions)

def generate_column_condition(columns: Union[InstrumentedAttribute,list[InstrumentedAttribute]], search_values: list[str], search_type: Literal['<','<=','=','>=','>']) -> BinaryExpression:
    """
    Takes in the database column, and the search values in the form of a list, and the search condition
    These are what go in the where clause of the query
    Assumes OR logic between all the values in the list
    """
    # TODO?: implement <, >, <=, >= for ending numeric strings or not haha just say that means alphabetical order
    conditions = []
    if not isinstance(columns, list):
        columns = [columns]

    for search_value in search_values:
        # GAURD CONDITIONS :)
        # Although weird you can give raw stings to <, >, <=, >= I think its an alphabetical order thing
        # if using <, >, <=, >= with boolean value 
        if ('false' in search_value.lower() or 'true' in search_value.lower()) and search_type in ['<','<=','>=','>']:
            continue
        if '~' in search_value:
            search_value_split = search_value.split('~')
            if len(search_value_split) != 2:
                continue
            if search_type == '=':
                conditions.append(
                    db.or_(
                        col.between(search_value_split[0].strip(), search_value_split[-1].strip()) for col in columns
                    )
                )
            else: #stops from appending another check
                continue
        elif search_type == '=':
            conditions.append(db.or_(col == search_value for col in columns))
        elif search_type == '>':
            conditions.append(db.or_(col > search_value for col in columns))
        elif search_type == '<':
            conditions.append(db.or_(col < search_value for col in columns))
        elif search_type == '>=':
            conditions.append(db.or_(col >= search_value for col in columns))
        elif search_type == '<=':
            conditions.append(db.or_(col <= search_value for col in columns))
    return db.or_(*conditions)


class BaseQueryBuilder:
    def __init__(self, query):
        self.query = query
        self.column_conditions = []

    def apply_joins(self, joins: list[Union[InstrumentedAttribute, tuple[InstrumentedAttribute, BinaryExpression]]]) -> Select:
        """
        Applies joins from left to right. If the join has a join condition make sure it is a tuple with the join condition as the second element
        """
        for join in joins:
            if join is None:
                continue
            elif isinstance(join, tuple):
                self.query = self.query.join(*join)
            else:
                self.query = self.query.join(join) 
        return self
    
    def query_factory(self, column, search_values: list[str], search_type: Literal['<','<=','=','>=','>'], joins=None) -> Select:
        """
        Adds joins and finds column conditions
        """
        if joins is not None:
            self.apply_joins(joins)

        # get all column conditions and append them to the bookeeper of conditions
        self.column_conditions.append(
            generate_column_condition(column, search_values, search_type)
        )
        return self
    
    def build(self) -> Select:
        # joins already applied! and it is ok if there are no column conditions
        return self.query.where(*self.column_conditions)



class GeneralQueryMixin:
    def query_id(self, search_values:str, search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            self.db_model.id,
            search_values,
            search_type
        )
    def query_deprecation(self, search_values:str, search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            self.db_model.is_deprecated,
            search_values,
            search_type
        )
    
    def add_general_search_keys(self):
        self.search_keys.update({
            ID_QUERY_ALIASES: self.query_id,
            DEPRECATION_QUERY_ALIASES: self.query_deprecation,
        })
    
class ConstructionExistsMixin:
    """
    Mixin to check if part has construction step or not.
    """
    def query_construction_exists(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>'], construction_model: Union[mdl.Assembly, mdl.Test], construction_exists: bool):
        """
        Lets break down what the query is doing. First what does exists() do?
        It is always in the form `exists().where(condition)` so a mini example is:
        ## Exists example
        ```
        exists_query = session.query(exists().where(User.email == 'example@example.com')).scalar()
        if exists_query print('User exists!') else print('User does not exist.')
        ```
        ## Putting it together:
        Following https://docs.sqlalchemy.org/en/20/core/selectable.html#sqlalchemy.sql.expression.exists,
        """
        LackConstruction = aliased(construction_model)
        if search_type == '=':
            # find modules that have X assembly step
            constr_part_id = LackConstruction.component_id if self.db_model == mdl.Component else LackConstruction.module_id
            parts_with_tests = select(LackConstruction.type).where(
                (constr_part_id == self.db_model.id) &
                (LackConstruction.type.in_(search_values))
            ).exists()
            self.query = self.query.where(parts_with_tests) if construction_exists else self.query.where(~parts_with_tests)
        return self
    
    def add_construction_exists_search_keys(self):
        self.search_keys.update({
            HAS_ASSEMBLY_QUERY_ALIASES:      partial(self.query_construction_exists, construction_model=mdl.Assembly, construction_exists=True),
            HAS_TEST_QUERY_ALIASES:          partial(self.query_construction_exists, construction_model=mdl.Test, construction_exists=True),
            LACKS_ASSEMBLY_QUERY_ALIASES:    partial(self.query_construction_exists, construction_model=mdl.Assembly, construction_exists=False),
            LACKS_TEST_QUERY_ALIASES:        partial(self.query_construction_exists, construction_model=mdl.Test, construction_exists=False)
        })
    
class ConstructionQueryBuilder(BaseQueryBuilder, GeneralQueryMixin):
    def __init__(self, db_model: Union[mdl.Test, mdl.Assembly], query: Select=None):
        super().__init__(query)
        self.query = query if query is not None else db.select(db_model)
        self.db_model = db_model
        self.search_keys = {
            CONSTR_TYPE_QUERY_ALIASES:  self.query_type,
            COMPONENT_QUERY_ALIASES:    self.query_component_serial_number,
            MODULE_QUERY_ALIASES:       self.query_module_serial_number,
            DATA_QUERY_ALIASES:         self.query_data,
        }
        self.add_general_search_keys()

    def query_type(self, search_values: list[str], search_type:Literal['<','<=','=','>=','>']) -> Select:
        return self.query_factory(
            self.db_model.type, 
            search_values, 
            search_type
        )
    def query_data(self, data_col, search_values: list[str], search_type:Literal['<','<=','=','>=','>']) -> Select:
        self.column_conditions.append(
            generate_json_column_conditions(
                sql_func.jsonb_extract_path_text(self.db_model.data, data_col), 
                search_values, 
                search_type
            )
        )
        return self
    def query_component_serial_number(self, search_values: list[str], search_type:Literal['<','<=','=','>=','>']) -> Select:
        return self.query_factory(
            mdl.Component.serial_number,
            search_values,
            search_type,
            joins=[self.db_model.component]
        )
    def query_module_serial_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']) -> Select:
        return self.query_factory(
            mdl.Module.serial_number,
            search_values,
            search_type,
            joins=[self.db_model.module]
        )


class ShipmentQueryBuilder(BaseQueryBuilder, GeneralQueryMixin):
    def __init__(self, query: Select=None):
        super().__init__(query)
        self.db_model = mdl.Shipment
        self.query = query if query is not None else db.select(mdl.Shipment)
        self.DestinationLocation = aliased(mdl.Location)
        self.OriginLocation = aliased(mdl.Location) 
        self.search_keys = {
            ORIGIN_QUERY_ALIASES:          self.query_origin,
            DESTINATION_QUERY_ALIASES:     self.query_destination,
            MODULE_QUERY_ALIASES:          self.query_module_serial_number,
            COMPONENT_QUERY_ALIASES:       self.query_component_serial_number,
            COMPONENT_TYPE_QUERY_ALIASES:  self.query_component_type,
            MODULE_TYPE_QUERY_ALIASES:     self.query_module_type,
            TRACKING_NUMBER_QUERY_ALIASES: self.query_tracking_number,
            IN_TRANSIT_QUERY_ALIASES:      self.query_in_transit
        }  
        self.add_general_search_keys()

    def query_origin(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [self.OriginLocation.name_long, self.OriginLocation.name_abbv],
            search_values,
            search_type,
            joins=[(self.OriginLocation, mdl.Shipment.origin_id == self.OriginLocation.id)]
        )
    def query_destination(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [self.DestinationLocation.name_long, self.DestinationLocation.name_abbv],
            search_values,
            search_type,
            joins=[(self.DestinationLocation, mdl.Shipment.destination_id == self.DestinationLocation.id)]
        )
    def query_module_serial_number(self,search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Module.serial_number,
            search_values,
            search_type,
            joins=[mdl.Shipment.modules]
        )
    def query_component_serial_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Component.serial_number,
            search_values,
            search_type,
            joins=[mdl.Shipment.components]
        )
    def query_component_type(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        # .where((mdl.ComponentType.name_long == search_value) | (mdl.ComponentType.name_abbv == search_value))
        return self.query_factory(
            [mdl.ComponentType.name_long, mdl.ComponentType.name_abbv],
            search_values,
            search_type,
            joins=[mdl.Shipment.components, mdl.Component.component_type]
        )
    def query_module_type(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [mdl.ModuleType.name_long, mdl.ModuleType.name_abbv],
            search_values,
            search_type,
            joins=[mdl.Shipment.modules, mdl.Module.module_type]
        )
    def query_tracking_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Shipment.tracking_number,
            search_values,
            search_type
        )
    
    def query_in_transit(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        lowered_search_values = [val.lower() for val in search_values]
        if 'true' in lowered_search_values[-1] and search_type == '=':
            self.query = self.query.where(mdl.Shipment.reception_date == None)
        return self
    
    
class ModuleQueryBuilder(BaseQueryBuilder, ConstructionExistsMixin, GeneralQueryMixin):
    def __init__(self, query: Select=None, disable_current_location=False):
        super().__init__(query)
        self.db_model = mdl.Module
        self.query = query if query is not None else db.select(mdl.Module)
        self.AssemblyLocation = aliased(mdl.Location)
        self.search_keys = {
            MODULE_TYPE_QUERY_ALIASES:       self.query_module_type,
            COMPONENT_QUERY_ALIASES:         self.query_component_serial_number,
            MODULE_QUERY_ALIASES:            self.query_serial_number,
            ASSEMBLY_LOCATION_QUERY_ALIASES: self.query_assembly_location,
        }
        if not disable_current_location:
            self.search_keys[CURRENT_LOCATION_QUERY_ALIASES] = self.query_current_location
        self.add_construction_exists_search_keys()
        self.add_general_search_keys()

    def query_module_type(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [mdl.ModuleType.name_long, mdl.ModuleType.name_abbv],
            search_values,
            search_type,
            joins=[mdl.Module.module_type]
        )
    def query_component_serial_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Component.serial_number,
            search_values,
            search_type,
            joins=[mdl.Module.components]
        )
    def query_serial_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Module.serial_number,
            search_values,
            search_type
        )
    def query_current_location(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        self.CurrentLocation = aliased(mdl.Location)        
        return self.query_factory(
            [self.CurrentLocation.name_long, self.CurrentLocation.name_abbv],
            search_values,
            search_type,
            joins=[(self.CurrentLocation, mdl.Module.current_location_id==self.CurrentLocation.id)]
        )
         
    def query_assembly_location(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [self.AssemblyLocation.name_long, self.AssemblyLocation.name_abbv],
            search_values,
            search_type,
            joins=[(self.AssemblyLocation, mdl.Module.assembly_location_id==self.AssemblyLocation.id)]
        )

class ComponentQueryBuilder(BaseQueryBuilder, ConstructionExistsMixin, GeneralQueryMixin):
    def __init__(self, query: Select=None, disable_current_location=False):
        super().__init__(query)
        self.db_model = mdl.Component
        self.query = query if query is not None else db.select(mdl.Component)

        self.search_keys = {
            COMPONENT_TYPE_QUERY_ALIASES: self.query_component_type,
            COMPONENT_QUERY_ALIASES:      self.query_serial_number,
            MODULE_QUERY_ALIASES:         self.query_module_serial_number,
            METADATA_QUERY_ALIASES:       self.query_meta_data,
        }
        if not disable_current_location:
            self.search_keys[CURRENT_LOCATION_QUERY_ALIASES] = self.query_current_location
        self.add_construction_exists_search_keys()
        self.add_general_search_keys()

    def query_component_type(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [mdl.ComponentType.name_long, mdl.ComponentType.name_abbv],
            search_values,
            search_type,
            joins=[mdl.Component.component_type]
        )
    def query_serial_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Component.serial_number,
            search_values,
            search_type,
        )
    def query_module_serial_number(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            mdl.Module.serial_number,
            search_values,
            search_type,
            joins=[mdl.Component.module]
        )
    def query_current_location(self, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        return self.query_factory(
            [mdl.Location.name_long, mdl.Location.name_abbv],
            search_values,
            search_type,
            joins=[mdl.Component.current_location]
        )
    def query_meta_data(self, data_col:str, search_values:list[str], search_type:Literal['<','<=','=','>=','>']):
        self.column_conditions.append(
            generate_json_column_conditions(
                sql_func.jsonb_extract_path_text(mdl.Component.meta_data, data_col), 
                search_values, 
                search_type
            )
        )
        return self


class QueryDirector:
    def __init__(self, total_search_string):
        self._builder = None
        self.total_search_string = total_search_string #''.join(search_string.split()) #remove whitespace from search string
        self.grouped_searches = defaultdict(list)
        if self.total_search_string:
            self.group_search_by_key_and_symbol()
        
    def group_search_by_key_and_symbol(self):
        #https://stackoverflow.com/questions/50182237/regex-to-parse-comma-separated-key-value-pairs-with-commas-in-value -> has to be converted to python regex though, thx chat gpt
        kv_csv_re = r"(?P<key>.+?)(?:(=|<|>|!=|<=|>=))(?P<value>[^=|<|>|!=|<=|>=]+)(?:,|$)"
        for search_key, search_symbol, search_value in re.findall(kv_csv_re, self.total_search_string):
            search_key = search_key.strip() #removes leading and ending spaces
            search_value = search_value.strip()
            search_symbol = search_symbol.strip()
            for srch in search_value.split(','):
                srch = srch.strip() #remove leading and ending spaces
                if not srch:
                    continue
                self.grouped_searches[(search_key, search_symbol)].append(srch)

    # this just explicitly says you need to set builder??
    @property
    def builder(self):
        return self._builder
    
    @builder.setter
    def builder(self, builder):
        self._builder = builder

    def build_query(self) -> Select:
        for (search_key, symbol), search_values in self.grouped_searches.items():
            for valid_search_keys in self.builder.search_keys:
                if search_key.lower() in valid_search_keys:
                    self.builder.search_keys[valid_search_keys](search_values, symbol)
                    break
                elif ':' in search_key:
                    table_col, json_col = search_key.split(':')
                    table_col = table_col.strip()
                    json_col = json_col.strip()
                    if table_col in valid_search_keys:
                        self.builder.search_keys[valid_search_keys](json_col, search_values, symbol)
        return self.builder.build()

builder_map = {
    'test': partial(ConstructionQueryBuilder,mdl.Test),
    'assembly': partial(ConstructionQueryBuilder, mdl.Assembly),
    'shipment': ShipmentQueryBuilder,
    'module': ModuleQueryBuilder,
    'component': ComponentQueryBuilder
}