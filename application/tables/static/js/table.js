
function generate_popover(allowed_search_keywords){
    const keywordMappings = {
        'ASSEMBLY_EXISTS': `
            <li>
                <strong>ASSEMBLY EXISTS</strong> 
                <ul>
                    <li> 
                        Filter part if the it has had an assembly step or not. <br>
                    </li>
                    <li>
                        <strong>VALID KEYWORDS for HAS ASSEMBLY</strong>: 
                        <ul>
                            <li> 
                                has_assembly,with_assembly, has_assy, with_assy
                            </li>
                        </ul>   
                        <strong>VALID KEYWORDS for LACKS ASSEMBLY</strong>: 
                        <ul>
                            <li> 
                                lacks_assembly, without_assembly, no_assembly, lacks_assy, without_assy, no_assy
                            </li>
                        </ul>                   
                    </li>
                </ul>                
            </li>
        `,
        'TEST_EXISTS': `
        <li>
            <strong>TEST EXISTS</strong> 
            <ul>
                <li> 
                    Filter part if the it has had an test performed or not. <br>
                </li>
                <li>
                    <strong>VALID KEYWORDS for HAS TEST</strong>: 
                    <ul>
                        <li> 
                            has_test, with_test
                        </li>
                    </ul>       
                    <strong>VALID KEYWORDS for LACKS TEST</strong>: 
                    <ul>
                        <li> 
                            lacks_test, without_test, no_test
                        </li>
                    </ul>               
                </li>
            </ul>                
        </li>
    `,
        'ASSEMBLY_LOC': `
            <li>
                <strong>ASSEMBLY LOCATION</strong> 
                <ul>
                    <li> 
                        The location where the module was assembled <br>
                    </li>
                    <li>
                        <strong>VALID KEYWORDS</strong>: 
                        <ul>
                            <li> 
                                assembly_loc, assembly_location, assy_loc, assy_location, assyloc
                            </li>
                        </ul>                    
                    </li>
                </ul>                
            </li>
        `,
        'DEPRECATED': `
            <li>
                <strong>Deprecation</strong> 
                <ul>
                    <li> 
                        This flag hides the queried results from all views on default in the website but is searchable if set to true.
                    </li>
                    <li>
                        <strong>VALID KEYWORDS</strong>: 
                        <ul>
                            <li> 
                                dep, depricated, deprecate
                            </li>
                        </ul>                    
                    </li>
                </ul>                
            </li>
        `,
        'ID': `
            <li>
                <strong>ID</strong> 
                <ul>
                    <li> 
                        Every row in a table is given a unique identifier (ID) that is searchable
                    </li>
                </ul>                
            </li>
        `,
        'ORIGIN': `
            <li>
                <strong>origin, origin_location, org, origin_loc</strong> 
                <ul>
                    <li> 
                        This is the location where the shipment originated from
                    </li>
                </ul>                
            </li>
        `,
        'DEST': `
            <li>
                <strong>dest, destination, destination_location, dest_loc</strong> 
                <ul>
                    <li> 
                        This is the location where the shipment was sent to
                    </li>
                </ul>                
            </li>
        `,
        'TRANSIT': `
        <li>
            <strong>TRANSIT, INTRANSIT, IN_TRANSIT</strong> 
            <ul>
                <li> 
                    Check if the shipment is in transit
                </li>
            </ul>                
        </li>
        `,
        'TRACKNUM': `
            <li>
                <strong>tracknum, tracking_number</strong> 
                <ul>
                    <li> 
                        This is the tracking number of the given shipment
                    </li>
                </ul>                
            </li>
        `,
        'MOD': `
            <li>
                <strong>mod, module, module_serial_number, mod_sn, modsn, module_sn</strong> 
                <ul>
                    <li> 
                        Every module has a unique serial number based of its module type and its assembly location
                    </li>
                </ul>                
            </li>
        `,
        'MODTYPE': `
            <li>
                <strong>modtype, module_type</strong> 
                <ul>
                    <li> 
                        This tells you what kind of module it is, digital, production, pre production, etc...
                    </li>
                </ul>
            </li>
        `,
        'COMP': `
            <li>
                <strong>comp, component, component_serial_number, comp_sn, compsn, component_sn</strong> 
                <ul>
                    <li> 
                        Every component has a unique serial number that is searchable
                    </li>
                </ul>
            </li>
        `,
        'METADATA': `
            <li>
                <strong>metadata, meta_data</strong> 
                <ul>
                    <li> 
                        These are the unique fields for a component. But is used slightly diffrent from the other keywords.<br>
                        You give <code>metadata:Field = VALUE</code>, for example (metadata:Row = 1), the field <strong>IS CASE SENSITIVE</strong>
                    </li>
                </ul>
            </li>
        `,
        'DATA': `
        <li>
            <strong>data</strong> 
            <ul>
                <li> 
                    Search on the specific fields in the data column. But is used slightly diffrent from the other keywords.<br>
                    You give <code>data:Field = VALUE</code>, for example (data:vgl = 45.1~45.7), <strong>the field IS CASE SENSITIVE</strong>
                </li>
            </ul>
        </li>
    `,
        'COMPTYPE': `
            <li>
                <strong>comptype, component_type</strong> 
                <ul>
                    <li> 
                        This tells you what kind of component it is, Prototype LGAD, Production ETROC, etc...
                    </li>
                </ul>
            </li>
        `,
        'LOC':`
            <li>
                <strong>loc, location, current_location, curr_loc</strong> 
                <ul>
                    <li> 
                        The current location of a part
                    </li>
                </ul>
            </li>
        `,
        'TEST':`
            <li>
                <strong>type, test, test_type, testtype</strong> 
                <ul>
                    <li> 
                        The type of test
                    </li>
                </ul>
            </li>
        `,
        'ASSEMBLY':`
            <li>
                <strong>assembly_type, assy_type, assytype, assembly, assy</strong> 
                <ul>
                    <li> 
                        The type of assembly
                    </li>
                </ul>
            </li>
        `
    };

    let html = `
        <h5>Table Search Guide</h5>
        <p>
            This is a key-value and comma seperated value search. The supported keywords are defined below
        </p>
        <strong>Suported Conditions: </strong><code>=, <, >, <=, >=</code>  *note* <,> work on strings too!<br>
        <strong>Range Search: </strong> <code> ~ </code> <br>
        <strong>KEYWORDS (not case sensitive!): </strong>
        <ul>
    `;
    allowed_search_keywords.forEach(keyword => {
        if (keywordMappings[keyword]) {
            html += keywordMappings[keyword];
        }
    });
    html += `</ul>
    
    <strong>EXAMPLES: </strong>
    <ul>
        <li>    
            KEY =VAL1~VAL10, KEY2<60
        </li>
        <li>
            KEY = VAL1, VAL2, VAL3
        </li>
        <li>
            KEY = true, KEY2 = false
        </li>
        <li>
            MOD = MODSN1~MODSN~30
        </li>
    </ul>

    <strong>TIPS: </strong>
    If the search is slow consider narrowing your search more. For example: <br>
    <strong>FAST: </strong><code>TEST=Sensor Test Array CV - ETL Site, data:vgl=45.1~45.8</code> <br>
    <strong>SLOW: </strong><code>data:vgl=45.1~45.8</code>    
    `;
    return html;
    
}

// EVENT LISTENERS FOR THIS JS
function clearSearchEventListener(grid_js_search_input) {
    grid_js_search_input.on('input', function() {
        // Get the value of the input field
        var inputValue = $(this).val();
        // Check if the input value is empty
        if (inputValue === "") {
            var url = new URL(window.location.href);
            var params = new URLSearchParams(url.search); // get all query search parameters
            params.delete('search'); //remove search if the input was cleared
            url.search = params.toString();
            window.history.replaceState({}, document.title, url.toString());
        }
    });    
}

// Create a function that generates the HTML for the radio buttons
function generateSettingsHTML(columns) {
    let htmlContent = `
      <div id="table-settings-bar">
        <div class="all-column-toggle-groups">
    `;
    columns.forEach(column => {
        if (column.id != 'id'){
            htmlContent += `
                <div class="form-group column-toggle-group">
                    <label class="form-check-label earth-green switch-label" for="${column.id}">${column.name}</label>
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="column-toggle-${column.id}" data-column-id="${column.id}" checked>
                    </div>
                </div>
            `;            
        }
    });
    htmlContent += `
        </div>
        <div class="input-group page-length-group">
          <input min="1" max="200" type="number" id="page-length-input" placeholder="Page length" class="form-control" aria-describedby="page-length-btn" data-1p-ignore data-bwignore data-lpignore="true" autoComplete="off" />
          <button class="btn btn-earth" type="button" id="page-length-btn">Enter</button>
        </div>
    </div>`;
    return htmlContent;
}

// TABLE GENERATION FUNCTION
function Table(output_div_id, columns, url, depraction_url, debounceTimeout, search_keywords){  
    const output_div = "#"+output_div_id

    //add settings html (page length, and radios)
    $(output_div).css({"background-color":"#2f6a69", "border-color": "#2f6a69", "border-style": "solid"})
    $(output_div).append(generateSettingsHTML(columns));

    //deprication button and modal
    $("#table-settings-bar").append(
        `<button type="button" class="btn btn-danger ms-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Toggle Deprication
        </button>`
    );
    $(output_div).append(
        `
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you really sure you would like to depricate these queried results?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    By clicking confirm, the queried results will be hidden from all views in the website. <i>If the queried results are already depricated, then it will unhide them from all views.</i>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="confirm-deprication-btn" class="btn btn-primary" data-bs-dismiss="modal">Confirm</button>
                </div>
                </div>
            </div>
        </div>
        `
    )
    $("#confirm-deprication-btn").on("click", function(){
        $.ajax({
            url: depraction_url,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: (result) => {
                $("#ajax-flashes").append(`
                    <div class="alert alert-success alert-dismissible fade show" role="alert"> 
                        "Success!"
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                `);
            },
            error: (result) => {
                var error = result?.responseJSON?.message;
                $("#ajax-flashes").append(`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert"> 
                        ${error}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                `);
            }
        });

        location.reload();
    });


    //add popover question mark with inputted popover content to grid
    popoverContent = generate_popover(search_keywords)
    $("#table-settings-bar").append(
        `<button type="button" class="btn rounded-btn base-popover" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="right" data-bs-content="${popoverContent}"  data-bs-html="true" style="margin-left: 0px;">
            <i class="bi bi-question-circle-fill" style="color: #2f6a69"></i>
        </button>`
    );

    $(output_div).append(`<div id="gridjs-main-table"></div>`)

    saved_search = new URLSearchParams(window.location.search).get('search');
    // if (saved_search == null || saved_search==undefined) {
    //     saved_search = default_saved_search;
    // }
    const grid = new gridjs.Grid({
        columns: columns,
        server: {
            url: url,
            then: results => results.data,
            total: results => results.total,
        },
        search: {
            debounceTimeout: debounceTimeout,
            keyword: saved_search,
            server: {
                url: (prev, keyword, search) => {     
                    return updateUrl(prev, {search:updateSearch(keyword,search)});
                }
            },
        },
        pagination: {
            server: {
                url: (prev, page, limit) => {
                    return updateUrl(prev, {start: page * limit, length: limit})}
            },
        },
        language: {
            'search': {
              'placeholder': '🔍 Search...'
            },
        },
        resizable: true,
    }).render(document.getElementById("gridjs-main-table"));

    clearSearchEventListener($(".gridjs-input.gridjs-search-input"));
    
    //event listener on the radios to hide columns
    $("input[id^='column-toggle-']").on("change", function() {
        for (var col of grid.config.columns) {
            const columnId = $(this).data('column-id')
            if (col.id == columnId){
                col.hidden = !$(this).is(':checked');
                grid.forceRender();
            };
        }
    });

    //change length of pages
    $("#page-length-btn").on("click", function(){
        const page_length = $("#page-length-input").val();
        grid.config.pagination.limit = page_length;
        grid.forceRender();
    })

    return grid;



}

//INTERNAL FUNCTIONS
const updateUrl = (prev, query) => {
    const url = new URL(window.location);
    for (const search_type of Object.keys(query)){
        if (search_type=='on_load_search'){
            //we can leave these out of appending to the main url since they already have them!
            continue;
        }
        const search = query[search_type];
        if (search_type=='search'){
            url.searchParams.set('search',search);
            window.history.pushState(null, '', url.toString());
            continue;
        }

        url.searchParams.set(search_type, search);
        window.history.pushState(null, '', url.toString());
    }
    //prev is the previus url
    // console.log('------------------------')
    // console.log(prev)
    // console.log(prev.indexOf('?')) //if this is greater than or equal
    //this is an inline conditional statement
    // prev.indexOf('?') >= 0 ? '&' : '?'), the condition: prev.indexOf('?') >= 0 , '&' : '?' what to do if true or false
    //if there is a ? return & and if not return ? since that would mean it is the first query string param
    
    //this takes a dictionary object as the query string paramaters and stringifies them for the url
    // console.log(new URLSearchParams(query).toString())
    // console.log(prev + (prev.indexOf('?') >= 0 ? '&' : '?') + new URLSearchParams(query).toString())
    //$('input.gridjs-input.gridjs-search-input').keypress(); //on load it will do keypress to trigger search

    return prev + (prev.indexOf('?') >= 0 ? '&' : '?') + new URLSearchParams(query).toString();
  };

const updateSearch = (previous_search,current_search) => {
    if (previous_search=='' || previous_search==undefined) {
        //dont need comma split if there is no search
        return current_search;
    }
    if (previous_search==current_search) {
        return '';
    }
    if (current_search=='' || current_search==undefined) {
        //if there hasnt been anything search just return previous search, stops the comma from being appended on every reload to the page url
        return previous_search
    }
    return previous_search+','+current_search;
}