#The only lasting truth is change -Octavia Butler

#The application:
from flask import Flask
#import the configuration file for Flask
from config import the_config
#get LoginManager class from flask_login
from flask_login import LoginManager
#four required items: is_authenticated, is_active, is_anonymous, get_id()
#have User class inherit UsrMixin class which does it for you
#for dates and tim
from flask_moment import Moment
#for powerful styling tools
from flask_bootstrap import Bootstrap5
from flask_admin import Admin
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
from flask_migrate import Migrate
from flasgger import Swagger 
import yagmail

#initiate flask app
app = Flask(__name__,template_folder='./admin/templates', static_folder='./admin/static') #gross way to do this admin should be base of app then idk

#load the configuration file (get env variables)
app.config.from_object(the_config) #tells flask to read config file

#setup ORM for database
db.init_app(app)

#Database migrations
migrate = Migrate(app, db)

#initialize extension login
login_manager = LoginManager(app)
login_manager.login_view = 'main.home' #sbring them to the endpoint for loggin in, ie the one named 'login'
login_manager.login_message_category = "warning"
login_manager.login_message = u"Please login to access this page. Contact a user to be added to an egroup for access: cms-etl-construction-web-app@cern.ch"

from authlib.integrations.flask_client import OAuth
oauth = OAuth(app)
oauth.register(
    name='cern',
    server_metadata_url = the_config.CERN_SSO,
    client_id = the_config.CLIENT_ID,
    client_secret = the_config.CLIENT_SECRET,
    client_kwargs = {
        'scope': 'openid email profile'
    }
)

app.config['SWAGGER'] = {
    'title': 'Construction API',
    'uiversion': 3
}

swagger_config = Swagger.DEFAULT_CONFIG
# swagger_config['swagger_ui_bundle_js'] = '//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js'
# swagger_config['swagger_ui_standalone_preset_js'] = '//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js'
# swagger_config['jquery_js'] = '//unpkg.com/jquery@2.2.4/dist/jquery.min.js'
# swagger_config['swagger_ui_css'] = '//unpkg.com/swagger-ui-dist@3/swagger-ui.css'

base_template = {
  "securityDefinitions": {
    "basicAuth": {
      "type": "basic"
    },
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header",
      "default": "Bearer ",
      "description": 'PLEASE READ, correct format is "Bearer your_token", without quotes and your_token is the whole token, so you need to put Bearer with a space before the token!!!! To get the token you need to hit up the tokens endpoint to get a token and authenticate using Basic Authentication from the fields below!'
    }
  },
  "info": {
      "title": "ETL Construction API UI",
      "description": f"""
          Welcome, this is the Construction API documentation. The purpose of the API is to automate the upload of tests and assembly steps for the sites so they do not have to manually input them into the website. Each section will tell you how to curl to different parts of the api and let you try it out! 
          
          To get started you need to generate a token to get access to the endpoints. You can click authorize and sign in with your username and password, then hit up the tokens endpoint, then copy paste the token like so: 'Bearer <your token>' and put the token in the token authorization spot.
          
          Tokens last {the_config.TOKEN_EXPIRATION} seconds or {the_config.TOKEN_EXPIRATION / 60} minutes or {the_config.TOKEN_EXPIRATION / (60*60)} hours
          """,
      "version": "0.0.1"
  },
}

swagger = Swagger(app, template=base_template, config=swagger_config)#, template=base_template)

#open email connection
yag = None #for each email send do a check if yag is None do not do anything
if the_config.EMAIL_ADDRESS:
    yag = yagmail.SMTP(the_config.EMAIL_ADDRESS, the_config.EMAIL_APP_PASSWORD) 

#initialize admin extenstion
admin_bp = Admin(name='Admin', template_mode='bootstrap4')#, static_url_path="application/admin/static")

#get blueprints
from application.main import main_bp
app.register_blueprint(main_bp)

from application.errors import errors_bp
app.register_blueprint(errors_bp, url_prefix='/errors')

from application.auth import auth_bp
app.register_blueprint(auth_bp, url_prefix='/auth')

from application.parts import parts_bp
app.register_blueprint(parts_bp, url_prefix='/parts')

from application.shipments import shipments_bp
app.register_blueprint(shipments_bp, url_prefix='/shipments')

from application.construction import construction_bp
app.register_blueprint(construction_bp, url_prefix='/construction')

from application.tables import tables_bp
app.register_blueprint(tables_bp, url_prefix='/tables')

from application.admin.routes import SecureAdminIndexView
admin_bp.init_app(app,index_view=SecureAdminIndexView())

from application.api import api_bp
app.register_blueprint(api_bp, url_prefix='/api')

#times and styling
moment = Moment(app)
bootstrap = Bootstrap5(app)

from flask_login import current_user
@app.context_processor
def inject_user():
  if current_user.is_authenticated:
    return {'base_current_user': current_user, 'max_content_length': the_config.MAX_CONTENT_LENGTH}
  else:
    return {'base_current_user': None, 'max_content_length': the_config.MAX_CONTENT_LENGTH}

@app.template_filter('sort_by_reception_date')
def sort_by_reception_date(shipments):
    return sorted(shipments, key=lambda x: (x.reception_date is None, x.reception_date))

app.jinja_env.filters['sort_by_reception_date'] = sort_by_reception_date

# inject the lookup tables 
def inject_table(table):
  with db.session.no_autoflush:
    return {f"base_page_{table.__tablename__}": db.session.execute(db.select(table)).scalars().all()}

@app.context_processor
def inject_component_types():
  from application import db_models as mdl
  return inject_table(mdl.ComponentType)
   
@app.context_processor
def inject_module_types():
  from application import db_models as mdl
  return inject_table(mdl.ModuleType)
   
@app.context_processor
def inject_locations():
  from application import db_models as mdl
  return inject_table(mdl.Location)

@app.context_processor
def inject_constr_types():
  from etlup.src.etlup.construction.adapters import get_first_and_later_model_versions

  first_models, later_models = get_first_and_later_model_versions()
  all_models = first_models + later_models

  all_tests = []
  all_assemblies = []
  for c in all_models:
    if c.get_schema_val(c,'table') == 'test':
      all_tests.append(c.get_summarized_model())
    elif c.get_schema_val(c,'table') == 'assembly':
      all_assemblies.append(c.get_summarized_model())

  return {
    'base_page_test_types': all_tests,
    'base_page_assembly_types': all_assemblies
  }

#import routes and models
from application.admin import routes