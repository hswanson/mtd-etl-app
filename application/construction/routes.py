from application.construction import construction_bp #get flask app
from application import db #get database ORM object
from application import db_models as mdl
from application.construction import forms
from flask.views import View
from application.errors.handlers import RecordConstructionError
from pydantic import ValidationError
from etlup.src.etlup.construction.adapters import ConstrArrModel, get_first_and_later_model_versions

from application.file_handler import FileUploader
from application.datetime_handler import now_utc
#basic flask functionalities
from flask import render_template, flash, redirect, url_for, request, jsonify

#login functionalities form flask_login
from flask_login import current_user, login_required

import json
from json import JSONDecodeError
from os.path import splitext
from config import the_config

from io import BytesIO
from zipfile import ZipFile
from werkzeug.datastructures.file_storage import FileStorage

#----------------------------------------HELPER FUNCTIONS---------------------------------------#

def zip_files(files):
    zipped_files = BytesIO()
    fnames = []
    with ZipFile(zipped_files, 'w') as zipf:
        for file in files:
            file.seek(0) # reset file pointer to start
            fnames.append(splitext(file.filename)[0])
            zipf.writestr(file.filename, file.read())
    zipped_files.seek(0)
    file_storage_zipped = FileStorage(zipped_files, filename='_'.join(fnames), content_type='application/zip')
    return file_storage_zipped

def make_table_data_pretty(data: str) -> str:
    data = str(data)
    truncated = data[:10] + '...' if len(data) > 10 else data
    q_safe = data.replace('"', '\'')
    return f'<p class="mb-0" title="{q_safe}">{truncated}</p>' 

def dict_to_display_table(data):
    if not isinstance(data, dict):
        raise ValueError("Input must be a dictionary")

    table = '''
    <table class="table table-striped table-bordered table-sm">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Key</th>
                <th scope="col">Value</th>
            </tr>
        </thead>
        <tbody>
    '''
    for key, value in data.items():
        table += f'''
            <tr>
                <td class="text-start">{key}</td>
                <td class="text-start">{make_table_data_pretty(value)}</td>
            </tr>
        '''
    
    table += '''
        </tbody>
    </table>
    '''
    
    return table

#--------ROUTES FOR TEST RECORDING-------------------#

class RecordJSONConstruction(View):
    init_every_request = False
    methods = ['GET', 'POST']
    decorators = [login_required]
    
    def __init__(self, model, form):
        self.model = model
        self.form = form

    def dispatch_request(self):
        form = self.form()
        if form.validate_on_submit():
            #validation done in wtforms form! to avoid double hits of the db, I added the pydantic model to the form as an attribute
            db_steps = form.ConstrUploader.add_all_to_db()
            db.session.commit()
            id_search = [str(db_constr.id) for db_constr in db_steps]
            return redirect(url_for(
                f'tables.construction_table', 
                constr_table=self.model.__tablename__, 
                search=f"ID={id_search[0]}~{id_search[-1]}"
            ))
        
        first_models, later_models = get_first_and_later_model_versions()
        return render_template(
            'json_construction.html', 
            title='Construction Upload', 
            form=form, 
            constr_name = self.model.__tablename__, 
            data_models = tuple(first_models+later_models)
        )
    
construction_bp.add_url_rule(
    "/record-test/",
    view_func=RecordJSONConstruction.as_view("record_test", mdl.Test, forms.ConstructionJSONForm)
)
construction_bp.add_url_rule(
    "/record-assembly/",
    view_func=RecordJSONConstruction.as_view("record_assembly", mdl.Assembly, forms.ConstructionJSONForm)
)
#have api take upload file and convert it to table
#have table have data field, but go to ... if its long and have hover, check formatting of course

def is_key_always_empty(dict_list, key):
    return all(not dictionary.get(key) for dictionary in dict_list)

@construction_bp.route('/api/preview-construction/', methods=['GET','POST'])
@login_required
def preview_construction():
    #table can take json so try giving it the dictionary from pydantic
    error_msg = None
    try:
        file = request.files['file']
        constr_steps = json.load(file)
        ConstrCong = ConstrArrModel.validate_python(constr_steps)
        file.seek(0) #sneeky, since json.load() reads the file if I want to use it again later, need to seek back to 0!
    except UnicodeDecodeError as e:
        error_msg = "Could not decode your file from bytes to ASCII, are you sure what you are uploading is a valid JSON file?"
    except JSONDecodeError as e:
        error_msg = "Your JSON file could not be decoded properly"
    except RecordConstructionError as e:
        error_msg = list(e.args)
    except ValidationError as e:
        error_msg = str(e)[0:1000]
    if error_msg:
        return {'columns':[],'data':[], 'error': error_msg}
    elif ConstrCong == []:
        return {'columns':[],'data':[], 'error': "Your JSON file was found to be empty!"}
    else:
        constr_routine = [constr.model_dump() for constr in ConstrCong]        
        columns = list(constr_routine[0].keys())
        # Find all columns that are always empty and remove them from table
        empty_columns = [col for col in columns if is_key_always_empty(constr_routine, col)]
        rows = []
        for constr in constr_routine:
            #first drop empty columns from constr and columns
            for e_col in empty_columns:
                #pop empty column from constr
                constr.pop(e_col)
                if e_col in columns:
                    columns.remove(e_col)
            
            #pretty format data
            if isinstance(constr["data"], dict):
                #add the keys as columns to table
                constr["data"] = dict_to_display_table(constr["data"])
            else:
                constr["data"] = make_table_data_pretty(constr["data"])
            rows.append(constr)


        gridjs_cols = [{'id': col, 'name': col.title()} for col in columns]
        return jsonify({'columns': gridjs_cols, 'data': rows, 'error': None})
    

@construction_bp.route('/observation/', methods=['GET','POST'])
@login_required
def observation():
    form = forms.ObservationForm()
    location_choices = [(loc.id, loc.name_long) for loc in mdl.Location.query.order_by('id') if loc.name_long != 'Transit']
    form.location.choices = location_choices

    if form.validate_on_submit():
        if form.module.data:
            module = mdl.Module.get_by_sn(form.module.data)
            component = None
        elif form.component.data:
            module = None
            component = mdl.Component.get_by_sn(form.component.data)
        else:
            #probably will never happen but just in case
            flash("Form submitted without module or component sn", "danger")
            return redirect(url_for('construction.observation'))
        
        location = mdl.Location.get_by_long_name(form.location.data)
        
        #LOOP THROUGH ALL IMAGES FILES, VALIDATE, ADD TO STORAGE, GET UUID and commit :)
        files_db = []
        image_files = []
        for img in form.images.data:
            if img.filename == '':
                continue
            ImgFile = FileUploader(img)
            ImgFile.stage_file('CLOUD')

            db_file = ImgFile.add_to_db()
            files_db.append(db_file)
            image_files.append(ImgFile)

        #---------------------------------------------------------------------------#
            
        observation = mdl.Observation(
            user_created = current_user,
            module = module,
            component = component,
            comment = form.message.data,
            observation_location = location,
            created_at = now_utc(),
            files = files_db,
        )
        db.session.add(observation)

        #this is so you commit all at the end, could try to make FileUploader take a list of files...
        for img_file in image_files:
            img_file.storage_commit()
        db.session.commit()
        
        if form.module.data:
            return redirect(url_for('parts.part_summary', part_table='module', part_sn=module.serial_number))
        #could add component filter to an inventory page or something but prob wont have component summary pages idk
        else:
            return redirect(url_for('parts.part_summary', part_table='component', part_sn=component.serial_number))

    return render_template('observation.html',title='Observation', form=form)

#---------------------------------------------------------------------------------------------#

@construction_bp.route('/<string:constr_table>/<int:constr_id>')
def view_construction(constr_table, constr_id):

    if constr_table == 'test':
        constr_step = mdl.Test.get_by_id(constr_id)
    elif constr_table == 'assembly':
        constr_step = mdl.Assembly.get_by_id(constr_id)
    elif constr_table == 'observation':
        #will slightly special handeling, not type or anything like this just pictures!
        constr_step = mdl.Observation.get_by_id(constr_id)
    else:
        raise NotImplementedError("This construction table does not exist!")

    if constr_step is None:
        flash("Whoops! This assembly, test or observation does not exist in the database!", category="warning")
        return redirect(request.referrer)
            
    return render_template('view_construction.html', title=f'{constr_table}'.capitalize(), constr_table=constr_table, constr_step = constr_step)
    