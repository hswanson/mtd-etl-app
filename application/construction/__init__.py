from flask import Blueprint

construction_bp = Blueprint('construction', __name__, template_folder='templates',static_folder='static')

from application.construction import routes