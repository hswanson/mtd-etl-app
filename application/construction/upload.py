from application import db
from application import db_models as mdl
from etlup.src.etlup.construction.adapters import ConstrArrModel, ConstrModel
from application.file_handler import FileUploader, gen_uuid
from application.errors.handlers import RecordConstructionError
from werkzeug.datastructures.file_storage import FileStorage
from config import the_config
from typing import Union
from io import BytesIO
import uuid
import json
import inspect
#-------------File Routine-------------------#

def get_db_table(constr: type[ConstrModel]):
    tablename = constr.get_schema_val('table')
    if tablename is None:
        raise RecordConstructionError("Alerted maintainer that they forgot to put tablename in the test model!")
    all_tables = inspect.getmembers(mdl, inspect.isclass)
    for _, table in all_tables:
        if hasattr(table, '__tablename__') and table.__tablename__ == tablename:
            return table
    raise RecordConstructionError("Alerted maintainer that upload table was not found!")

def validate_model_with_db_config(constr):
    """
    Validates tests and assemblies coming from the pydantic dictionary

    Checks:
    - If a module specifier, check it is of the correct module type
    - If a component specifier, check it is of the correct component type
    - If module and component specifiers, check component is associated with the module
         - also check if the provided module matches the component module if it does have a module
    - If all three specifiers, check component position is the correct one for that component on the module
    """
    valid_mod_types = constr.get_schema_val('module_types')
    valid_comp_types = constr.get_schema_val('component_types')
    if hasattr(constr, 'module') and constr.module is not None:
        mod = mdl.Module.get_by_sn(constr.module)
        if mod is None:
            raise RecordConstructionError(f"This module does not exist in the database: {constr.module}")
        elif mod.module_type.name_long not in valid_mod_types:
            raise RecordConstructionError(f"The provided module's ({mod.serial_number}) module type {mod.module_type.name_long} is not one of the valid module types ({valid_mod_types}) for this construction type ({constr.type})")
    #   CHECKS IF THE COMPONENT HAS THE CORRECT COMPONENT TYPE (construction type also dictates this)
    if hasattr(constr,'component') and constr.component is not None:
        comp = mdl.Component.get_by_sn(constr.component)
        if comp is None:
            raise RecordConstructionError(f"This component does not exist in the database: {constr.component}")
        elif comp.component_type.name_long not in valid_comp_types:
            raise RecordConstructionError(f"The provided components's ({comp.serial_number}) component type {comp.component_type.name_long} is not one of the valid component types ({valid_comp_types}) for this construction type ({constr.type})")
        
        #   Sets component for calculations to be the parent component as that one would be associated with the module
        ref_comp = comp
        if comp.parent is not None: #if comp has parent, use that for calculations
            ref_comp = comp.parent #works for digital modules too I think  
        #   Checks further logic if the component has a module, if that component has been assembled into a module, if component position matches the one in the database
        if hasattr(constr, 'module') and hasattr(constr,'component'): #correct types and existence have been checked
            if ref_comp.module is None:
                raise RecordConstructionError(f"This component ({comp.serial_number}) of component type ({comp.component_type.name_long}) has not been associated with a module.")
            #if mod and comp in the required (shown to match provided from first if), check they belong to eachother
            if ref_comp.module != mod:
                raise RecordConstructionError(f"The component you provided ({comp.serial_number}) is not apart of the module you provided ({mod.serial_number}). In the database, the component belongs to this module: {comp.module.serial_number}")
        if hasattr(constr, 'module') and hasattr(constr,'component') and hasattr(constr, 'component_pos'):
            #check if component position matches the one in the db
            if ref_comp.component_pos != constr.component_pos:
                raise RecordConstructionError(f"Component position in the db does not match the one provided. The correct position is {ref_comp.component_pos} for the component {comp.serial_number} in module {mod.serial_number}")


class ConstrUploader: #cong for conglomerate
    """
    Wraps pydantic model of list of construction steps. So it will do ALL the validation needed for upload
    """
    def __init__(self, constrs: list[dict]):
        self.constrs = ConstrArrModel.validate_python(constrs)
        for constr in self.constrs:
            validate_model_with_db_config(constr)

    def add_all_to_db(self) -> list[db.Model]: 
        db_objects = []
        for constr in self.constrs:
            db_obj = get_db_table(constr).from_dict( constr.model_dump() )
            db_obj.uuid = gen_uuid()
            db_obj.data = constr.data.model_dump()
            db.session.add(db_obj)
            db_objects.append(db_obj)
        return db_objects
