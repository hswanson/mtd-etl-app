from application import db_models as mdl
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FileField, TextAreaField, MultipleFileField
from wtforms.validators import DataRequired, Optional
from application.construction.upload import ConstrUploader
from application.form_utils import ValidateDB, validate_file
from config import the_config
import json

class ConstructionJSONForm(FlaskForm):
    json_file = FileField("JSON Data File", validators=[DataRequired()], render_kw={'class': "form-control form-control-lg"})
    submit = SubmitField('Submit')

    def validate(self,extra_validators=None):
        if not super(ConstructionJSONForm, self).validate(extra_validators):
            return False
        try:
            file = self.json_file.data
            constrs = json.load(file)
            file.seek(0) #sneeky, since json.load() reads the file if I want to use it again later, need to seek back to 0!

            #VALIDATE CONSTRUCTION STEPS and Give Pydantic model to form to use elsewhere
            self.ConstrUploader= ConstrUploader(constrs)
        except UnicodeDecodeError as e:
            self.json_file.errors.append("Could not decode your file from bytes to ASCII, are you sure what you are uploading is a valid JSON file?")
            return False
        except json.decoder.JSONDecodeError as e:
            self.json_file.errors.append("Your JSON file could not be decoded properly")
            return False
        return True


class ObservationForm(FlaskForm):
    module = StringField('Module', validators=[Optional(), ValidateDB(mdl.Module,'serial_number', in_db=True)], render_kw={"placeholder": "Module Serial Number", 'class': "form-control"})
    component = StringField('Component', validators=[Optional(), ValidateDB(mdl.Component,'serial_number', in_db=True)], render_kw={"placeholder": "Component Serial Number",'class': "form-control"})
    location = StringField('Location', validators=[DataRequired(), ValidateDB(mdl.Location,'name_long',in_db=True)], render_kw={"placeholder": "Where did this test take place?",'class': "form-control"})
    message = TextAreaField('Message', id="input_text",validators=[Optional()], render_kw={"placeholder": "Written observation",'class': "form-control"})
    images = MultipleFileField('Image', validators=[Optional(), validate_file(the_config.UPLOAD_IMAGE_EXTENSIONS,multiple=True)], render_kw={'class': "form-control form-control-lg"}) #include videos?
    submit = SubmitField('Submit')

    def validate(self,extra_validators=None):
        #not really sure how this bit works
        if not super(ObservationForm, self).validate(extra_validators):
            return False
        #this I get, makes sure that you input at least one module or component serial number
        if not self.module.data and not self.component.data:
            msg = 'At least one serial number must be selected to make an observation'
            self.module.errors.append(msg)
            self.component.errors.append(msg)
            return False
        
        if not self.message.data and len(self.images.data)==1:
            #if no file and no message raise error
            msg = 'Please write a message or upload an image to make an observation'
            self.message.errors.append(msg)
            self.images.errors.append(msg)
            return False
        return True