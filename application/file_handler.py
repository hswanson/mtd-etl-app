from application import db
from application import db_models as mdl
from flask_login import current_user
import os
from werkzeug.datastructures.file_storage import FileStorage
from werkzeug.utils import secure_filename
from webdav3.client import Client #for cloud talking webdav is old but it works for CERNbox
from config import the_config
import uuid
import magic

#-------------File Routine-------------------#

def gen_uuid():
    return str(uuid.uuid4())

def calc_uuid_filename():
    #very stupid but collision check because I am silly human
    passed_collision = False
    while passed_collision == False:
        uni_id = gen_uuid()
        if not mdl.File.query.filter_by(uuid_filename=uni_id).first():
            passed_collision = True
        else:
            mdl.Event.record_event(mdl.Event.INFO,mdl.Event.HIGH,f'A UUID collided lol, here it is: {uni_id}')
    return uni_id

class FileUploader():
    def __init__(self, file: FileStorage):
        self.file = file
        self._staged = False
        if not isinstance(self.file, FileStorage):
            raise TypeError(f"Inputted file type is {type(self.file)} and should be {type(FileStorage)}")
        if not hasattr(self.file, 'read'):
            raise ValueError("Inputted Filestorage object is not readable. Make sure the object passed into Filestorage can be read. Ex: not bytes but a BytesIO objects")
    def is_empty(self):
        if self.file.filename == '':
            return True
        return False
    
    def stage_file(self, storage_type: str):
        #filename and data
        if not self.is_empty():
            self.storage_type = storage_type
            file_ext = os.path.splitext(self.file.filename)[1].lower()
            self.actual_filename = secure_filename(self.file.filename) #secure filename removes malicious stuff like /../../bashrc etc...
            self.unique_filename = the_config.FILE_STORE_NAME+'-'+calc_uuid_filename() + file_ext
            self.mime_type = magic.from_buffer(self.file.read(2048), mime=True)
            self.file.seek(0)

            self.data = self.file.read()
            self.file.seek(0)

            self._staged = True

    def storage_commit(self):
        if not self._staged:
            raise ValueError("File has not been staged for commit, aborting commmit...")

        elif self.storage_type == 'CLOUD':
            options = {
                'webdav_hostname': the_config.WEBDAV_HOSTNAME,
                'webdav_login': the_config.WEBDAV_LOGIN,
                'webdav_password': the_config.WEBDAV_PASSWORD
            }
            client = Client(options)
            #remote path
            remote_path = the_config.WEBDAV_DIRECTORY + self.unique_filename
            client.upload_to(remote_path=remote_path, buff=self.data)

        elif self.storage_type == 'DB':
            #done outside this
            pass
        elif self.storage_type == 'LOCAL':
            file_path = the_config.LOCAL_UPLOAD_PATH
            if not os.path.exists(file_path):
                #check to see if the path even exists
                os.makedirs(file_path)
            self.file.save(os.path.join(file_path, self.unique_filename))
        else:
            raise NotImplementedError(f'The storage type provided has not been implemented, STORAGE_TYPE={self.storage_type}')
        
    def add_to_db(self):
        file = mdl.File(
            user_created = current_user if current_user else mdl.User.get_by_id(1),
            filename = self.actual_filename,
            uuid_filename = self.unique_filename,
            mime_type = self.mime_type,
            storage_type = self.storage_type,
            #data = self.data,
        )
        db.session.add(file)
        return file