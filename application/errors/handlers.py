from flask import render_template, flash, request, redirect, url_for, jsonify
from application.errors import errors_bp
from application import db #get database ORM object
from application import yag
from application import db_models as mdl
from config import the_config
from application.api.errors import error_response as api_error_response
import traceback
from werkzeug.exceptions import HTTPException
from pydantic import ValidationError
from webdav3.exceptions import WebDavException

class RecordConstructionError(Exception):
    pass

class ShipmentLogicError(Exception):
    pass

class PaginationError(Exception):
    pass

def wants_json_response():
    return request.accept_mimetypes['application/json'] >= \
        request.accept_mimetypes['text/html']

# @errors_bp.app_errorhandler(404)
# def not_found_error(error):
#     flash(f"Page was not found", category="danger")
#     return render_template('404.html'), 404

@errors_bp.app_errorhandler(500)
def internal_error(error):
    #could be caused after a database error, resets session to a clean state
    db.session.rollback()
    error_str = f"""
    URL: {request.url}

    DESCRIPTION: {error.description}

    TRACEBACK: 
    {traceback.format_exc()}
    """
    if yag:
        yag.send(the_config.ADMIN_EMAIL_ADDRESS, f'ETLAPP ERROR', f"{error_str}")  

    if wants_json_response():
        return api_error_response(500)
    else:
        return render_template('500.html'), 500

@errors_bp.app_errorhandler(413) #MAX_CONTENT_LENGTH throws error if a file is bigger than that
def too_large(error):
    if wants_json_response():
        return api_error_response(413)
    flash(f"The file or one of the files exceed the maximum size of {the_config.MAX_CONTENT_LENGTH / (1024*1024)} MBs, aborted upload", "danger") #show max size
    if request.referrer:
        return redirect(request.referrer) #takes you back to the last page, for some reason if there isnt one take them to the home page
    else:
        return redirect(url_for('main.home')), 413


@errors_bp.app_errorhandler(405)
def method_not_allowed(e):
    #return jsonify(error=str(e)), 405
    if wants_json_response():
        return api_error_response(405, message="Method Not Allowed Error, are you making the correct POST, PUT, DELETE, GET etc... to the correct endpoint?")
    else:
        flash(f"Method Not Allowed Error: {405}, redirecting home", "danger") #show max size
        return redirect(url_for('main.home')), 405


@errors_bp.app_errorhandler(RecordConstructionError)
def record_construction(e):
    error_message = list(e.args)
    if wants_json_response():
        return api_error_response(400, message=error_message)
    else:
        flash(list(e.args), category="danger")
        return redirect(request.referrer)

@errors_bp.app_errorhandler(ShipmentLogicError)
def shipment_logic(e):
    flash(str(e), category="danger")
    return redirect(request.referrer)

@errors_bp.app_errorhandler(ValidationError)
def pydantic_validation_error(e):
    error_message = str(e)
    if wants_json_response():
        return api_error_response(400, message=error_message)
    else:
        flash(error_message[0:1000], category="danger")
        return redirect(request.referrer)

@errors_bp.app_errorhandler(WebDavException)
def failed_upload(e):
    flash(f"Uploading a the file to the {the_config.FILE_STORE} failed, aborted any insertion into the database")
    return redirect(request.referrer)

# @errors_bp.app_errorhandler(405)
# def method_not_allowed(error):
#     # if wants_json_response():
#     return api_error_response(error, message="Method Not Allowed Error, are you making the correct POST, PUT, DELETE, GET etc... for the endpoint?")
    # else:
    #     flash(f"Method Not Allowed Error: {error}, redirecting home", "danger") #show max size
    #     return redirect(url_for('main.home')), error


# @errors_bp.app_errorhandler(Exception) #MAX_CONTENT_LENGTH throws error if a file is bigger than that
# def catch_all(error):
#     if wants_json_response():
#         return api_error_response(error)
#     else:
#         flash(f"An HTTP Error Occured:  {error}, redirecting home", "danger") #show max size
#         if request.referrer:
#             return redirect(request.referrer) #takes you back to the last page, for some reason if there isnt one take them to the home page
#         else:
#             return redirect(url_for('main.home')), error