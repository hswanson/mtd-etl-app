from application.main import main_bp
from application.db_models import File
#from application.db import db
from flask_login import current_user, login_required
from flask import render_template, request, send_file, redirect, url_for, send_from_directory, session, flash
from application import yag, datetime_handler
from config import root_dir
from config import the_config
from webdav3.client import Client #for cloud talking webdav is old but it works for CERNbox
from webdav3.exceptions import WebDavException
from io import BytesIO
from zipfile import ZipFile
from os import path
from datetime import timedelta
import jwt

@main_bp.route('/')
@main_bp.route('/home')
def home():
    return render_template('home.html',title='Home', current_user=current_user)

@main_bp.route('/download-template/<filename>')
def download_template(filename):
    #these files come from example data 
    try:
        return send_from_directory(path.join(root_dir,'example_data'),filename, as_attachment=True)
    except Exception as e:
        return str(e)

@main_bp.route('/send_feature_request', methods=['GET','POST'])
def send_feature_request():
    feature_text = request.get_data().decode()
    if yag:
        yag.send(the_config.ADMIN_EMAIL_ADDRESS, f'({request.url})ETLAPP FEATURE REQUEST/CONCERN', f"User {current_user.username} requests: \n {feature_text}")  
    return 'Feature or concern successfully sent!'


@main_bp.route('/api_token', methods=['GET', 'POST'])
def api_token():
    """
    jwt encode takes in 3 arguments
    1. Payload: json information you want to store in the token
        -> exp meaning experiration, is a jwt special field
    2. Secret Key: String used in the algorithm to generate the cryptographic key signature
        -> key must only be known to the application
    3. Signing algorithm: protects payload against tampering
    """
    if current_user.is_authenticated:
        token_expiration_date = datetime_handler.now_utc() + timedelta(seconds=the_config.TOKEN_EXPIRATION)
        token = jwt.encode({
            'user_id': current_user.id,
            'exp': token_expiration_date
        }, the_config.API_SECRET_KEY, algorithm='HS256')
        return render_template('api_token.html', title='API Token', token=token, token_expiration_date=the_config.TOKEN_EXPIRATION)
    else:
        flash("You need to be logged in to reach this page.", category="warning")
        return redirect(session.get("previous_url", url_for("main.home")))
    

#--------------------_EVERYTHING BELOW IS SOMETHING THAT WOULD BE GOOD TO PUT IN API / OR PART OF SOMETHING TO BE MERGED WTIH API
#DOWNLOAD/GET FILE API
@main_bp.route('/get-file/', methods=['GET','POST'])
def get_file():
    file_ids = request.args.getlist('file_ids')
    files = [File.get_by_id(f_id) for f_id in file_ids if f_id] #change I just made
    # If there's only one file, return it
    if len(files) == 1:
        return send_file(files[0].get_remote(), mimetype=files[0].mime_type, as_attachment=True, download_name=files[0].filename)
    elif len(files) > 1:  #zip all files and return
        data = BytesIO()
        with ZipFile(data, 'w') as zipf:
            for file in files:
                buff = file.get_remote()
                zipf.writestr(file.filename, buff.read())
        data.seek(0)
        return send_file(data, mimetype='application/zip', as_attachment=True, download_name='files.zip')
    else:
        return  redirect(request.referrer or url_for('main.home'))
    
    #else return nothing, meaning it was given empty file list