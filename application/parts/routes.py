from application.parts import parts_bp #get flask app
from application import db #get database ORM object
from application import db_models as mdl
from application.parts import register, serial_numbers, forms
from application.datetime_handler import now_utc
from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import current_user, login_required
import pandas as pd

@parts_bp.route('/register-components/', methods=['GET','POST'])
@login_required
def register_components():
    form = forms.RegisterComponentsForm()
    location_choices = [(str(loc.id), loc.name_long) for loc in mdl.Location.query.order_by('id') if loc.name_long!='Transit' and loc.name_long!='Undefined'] #convert id int to string for form default coerce is none
    first_choice = [('None','-- Select A Location --')]
    form.origin.choices = first_choice + location_choices

    if form.validate_on_submit():
        if not hasattr(form, 'components') or not hasattr(form, 'shipment') or not hasattr(form, 'already_uploaded_components'):
            flash(f'Error in form, please try again. Contact an admin for help, this should not happen.', category='error') 
            return redirect(url_for('tables.component_table'))
        mdl.Event.record_event(mdl.Event.INFO,mdl.Event.HIGH,f'Register component shipment registered by {current_user.username} for {form.shipment.destination.name_long}')
        if form.already_uploaded_components:
            message = f"These components have already been registered: {', '.join(form.already_uploaded_components)}"
            flash(message, category='info')   
        db.session.commit()
        flash(f'Successful registration of component(s)', category='success') 
        return redirect(url_for('tables.component_table'))
    return render_template('register_components.html',title='Add Components', form=form)

@parts_bp.route('/remove_components/', methods=['GET','POST'])
@login_required
def remove_components():
    form = forms.DeleteComponentsForm()
    if form.validate_on_submit():
        #parse file and get SerialNumber via excel stuff
        component_file = form.component_file.data #type is
        df = register.read_component_file(component_file)
        df = df.where(pd.notnull(df), None) #replaces null with None
        for _, row in df.iterrows():
            #required input
            comp_sn = row.get('SerialNumber')

            comp = mdl.Component.get_by_sn(comp_sn)
            if not comp:
                flash(f"This component ({comp_sn}) does not exist in the database. Remove it from the file and reupload.", category="danger")
                return redirect(url_for('parts.remove_components'))
            
            if comp.module:
                flash(f"This component ({comp.serial_number}) is already a part of a module ({comp.module.serial_number}). Either remove the module or contact an admin for approval.", category="danger")
                return redirect(url_for('parts.remove_components'))
            if comp.parent:
                flash(f"This component ({comp.serial_number}) has a parent component ({comp.parent.serial_number}). Either remove the parent component or contact an admin for approval.", category="danger")
                return redirect(url_for('parts.remove_components'))   
            if comp.test or comp.assembly or comp.observations:
                flash(f"Observations, tests or assemblies have been recorded for this component ({comp.serial_number}). Please get admin approval for deletion.", category="danger")
                return redirect(url_for('parts.remove_components'))         

            if len(comp.shipments) > 1:
                flash(f"This component ({comp.serial_number}) has been part of a shipment since it being registered. This requires admin approval to delete.", category="danger")   
                return redirect(url_for('parts.remove_components'))       

            db.session.delete(comp) #does not cascade on deleting subcomponents!
        
        db.session.commit()
        return redirect(url_for('tables.component_table'))
    return render_template('remove_components.html',title='Remove Components', form=form)


@parts_bp.route('/api/render-component-file', methods=['GET','POST'])
@login_required
def render_component_file():
    file = request.files['file']
    df = register.read_component_file(file)
    if not df.empty:
        return {'columns':df.columns.to_list(),'data':df.values.tolist()}
    else:
        return {'columns':[],'data':[]}
    

    #-------------------------------ROUTES FOR Preperation Page-----------------------------------#
@parts_bp.route('/register-modules/', methods=['GET','POST'])
@login_required
def register_modules():
    preform = forms.PreRegisterModuleForm()
    mod_type_choices = [(t.id, t.name_long) for t in mdl.ModuleType.query.order_by('id')]
    transit_loc= mdl.Location.get_by_long_name('Transit')
    location_choices = [(str(loc.id), loc.name_long) for loc in mdl.Location.query.order_by('id') if loc.id != transit_loc.id] #need ids because of weird different object thing <obj id=1> != <obj Nebraska>
    preform.assembly_location.choices = location_choices
    preform.module_type.choices = mod_type_choices
    if preform.validate_on_submit():
        mod_type_name = preform.module_type.data
        assy_loc_name = preform.assembly_location.data
        selected_sns = serial_numbers.get_mod_sns(preform.number_modules.data, mod_type_name, assy_loc_name)
        #could apply a filter here to order the serial numbers, but I will just assume they are in order...
        return redirect(url_for('parts.associate_components', selected_sns=selected_sns, module_type=mod_type_name, assembly_location=assy_loc_name))

    return render_template('register_modules.html',title='Register Modules', preform=preform)

@parts_bp.route('/api/gen-sns', methods=['GET','POST'])
@login_required
def gen_mod_sns():
    req_n_mods = request.form['n_modules']
    req_mod_type = request.form['module_type']
    req_assy_loc = request.form['assembly_location']
    
    return jsonify( serial_numbers.get_mod_sns(req_n_mods, req_mod_type, req_assy_loc) )

@parts_bp.route('/associate-components', methods=['GET','POST'])
@login_required
def associate_components():
    form = forms.RegisterModuleForm()
    selected_sns_request = request.args.getlist('selected_sns')
    module_type_request = request.args.get('module_type')
    assembly_location_request = request.args.get('assembly_location')
    
    if not selected_sns_request or not module_type_request or not assembly_location_request:
        flash("Please fill out this form before associating components", category='warning')
        return redirect(url_for('parts.register_modules'))

    module_sns = [entry['module_sn'] for entry in form.module_prep.data] #if not submitted data is just none! so its ok
    module_type = mdl.ModuleType.get_by_long_name(module_type_request)
    assembly_location = mdl.Location.get_by_long_name(assembly_location_request)
    if not module_type or not assembly_location:
        flash(f"Not a known module type ({module_type_request}) and/or assembly location ({assembly_location_request}), please start the form over", category="danger")
        return redirect(url_for('parts.register_modules'))
    
    number_of_components = module_type.number_of_components
    positions_number = list(range(number_of_components))[::-1]

    if form.validate_on_submit():
        #form.module_prep.data = list of dictionaries, each dictionary is a module w associated components, list of entries
        for entry in form.module_prep.data:
            #EACH ENTRY IS A MODULE and has a list of components: entry -> {mod_sn: mod_sn, positions:[1,2,3,4,5]}
            module_to_associate = mdl.Module(
                module_type=module_type, 
                serial_number=entry['module_sn'],
                assembly_location=assembly_location,
                current_location=assembly_location, # can do this because of enforced assembly location is component current location below
                user_created=current_user
            )
            db.session.add(module_to_associate)
            #remove these so its just the component position fields!
            del entry['module_sn']
            del entry['csrf_token']
            if len(entry['position']) != len(positions_number):
                flash('Something went terribly wrong contact admin, number of positions assigned for module type does not match number of fields(components) submitted', category='danger')
                return redirect(url_for('parts.register_modules'))
            
            for comp_sn, comp_pos in zip(entry['position'], positions_number):
                comp = mdl.Component.get_by_sn(comp_sn)
                if comp is None:
                    #for case of empty position
                    continue
                elif comp.module:
                    flash(f'This component {comp.serial_number} is already associated with a module {comp.module.serial_number}, contact an admin if this is undesirable (overriding component association is to be implemented!)', category='danger')
                    return redirect(url_for('parts.register_modules'))
                if comp.current_location != assembly_location:
                    #not sure if this could even happen, but just in case, for redudancy...
                    flash(f"This component ({comp.serial_number})'s current location, {comp.current_location.name_long} does not match the location {assembly_location.name_long}. Either a misclick or update the component's location", category='danger')
                    return redirect(url_for('parts.register_modules'))
                comp.module = module_to_associate
                comp.component_pos = int(comp_pos)
                comp.associated_on = now_utc() #do i instead ask for when they did this association?
                
        flash('Successful component association with modules!', category='success')
        mdl.Event.record_event(mdl.Event.INFO,mdl.Event.MEDIUM,f'Modules associated with components: {[{"module_sn":entry["module_sn"], "position":entry["position"]} for entry in form.module_prep.data]}')
        db.session.commit()
        return redirect(url_for('tables.module_table')) #for modules only, when doing just component uploads you will need this!! 

    if selected_sns_request and selected_sns_request != module_sns: #only do this if the module_sns are not the same as the selected_sns_request, which happens here
        #filters the associated ones based on module type and location inputted by the form
        possible_components = [(comp.id, comp.serial_number) for comp in mdl.Component.query.filter(mdl.Component.module_id==None).order_by('id') if register.valid_comp_for_user(comp,assembly_location)]

        for i,sn in enumerate(selected_sns_request):
            #this adds each module form, does it for how many sns were selected (info from register modules route)
            form.module_prep.append_entry({'module_sn':sn})
            for _ in positions_number:
                #this adds each position for ComponentPositionForm, builds the second nested fieldlist
                form.module_prep[i].position.append_entry()
            #add the choices
            for pos_field in form.module_prep[i].position: #position is the Field>ist, so looping over it is for position string field and then we add the choices to it :)
                pos_field.choices = possible_components

    return render_template(
        'associate_components.html',
        title='Register Modules', 
        form=form, 
        module_type = module_type,
        module_sns=selected_sns_request, 
        positions_number=positions_number, 
        zip=zip
    ) #can pass zip function into jinja template!


@parts_bp.route('/module_summary/api/delete_module/<module_sn>', methods=['GET','POST'])
@login_required
def delete_module(module_sn):
    module = mdl.Module.get_by_sn(module_sn)
    if not module:
        flash(f"This module no longer exists, {module_sn}", category="danger")
        return redirect(url_for('tables.module_table'))
    
    if module.shipments or module.observations or module.test or module.assembly:
        flash(f"This module ({module_sn}) cannot be deleted because it has been a part of a test, assembly, observation or shipment", category="danger")
        return redirect(url_for('parts.module_summary', module_sn=module_sn))
    else:
        flash(f"{module_sn} succesfully deleted", category="success")
        db.session.delete(module)
        db.session.commit()
    return redirect(url_for('tables.module_table'))

#---------------MODULE SUMMARY PAGES AND ASSEMBLY/TEST/MODULE TABLES---------------------#

@parts_bp.route('/<string:part_table>/<string:part_sn>')
def part_summary(part_table, part_sn):
    if part_table == 'component':
        part = mdl.Component.get_by_sn(part_sn)
    elif part_table == 'module':
        part = mdl.Module.get_by_sn(part_sn)
    else:
        raise NotImplementedError("Sorry no summary page for this part table!")
    if part is None:
        flash(f"This serial number ({part_sn}) for this table ({part_table}) was not found, aborting...", category="danger")
        return redirect(request.referrer)
    elif part_table == 'component':
        part_type = part.component_type
    elif part_table == 'module':
        part_type = part.module_type

    return render_template(
        'part.html', 
        title=part_table.capitalize(), 
        part_table=part_table, 
        part=part, 
        part_type = part_type
    )
