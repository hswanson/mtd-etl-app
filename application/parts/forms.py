from flask_wtf import FlaskForm

from wtforms import StringField, IntegerField, SubmitField, FileField, FormField, FieldList, HiddenField, DateField, SelectField
from wtforms.validators import Optional, NumberRange, DataRequired
from wtforms.validators import ValidationError as wtfValidationError

from application import db_models as mdl
from application.form_utils import ValidateDB, coerce_none
from application.datetime_handler import now_utc, convert_date_to_datetime
from application.shipments import logistics

from etlup.src.etlup.components.base_model import ComponentBase
from application import db #get database ORM object
from flask_login import current_user
import pandas as pd
from application.parts import register
from typing import Union, Literal

from collections import Counter

class RegisterComponentsForm(FlaskForm):
    origin = SelectField('Origin', validators=[DataRequired(), ValidateDB(mdl.Location,'id',in_db=True)], coerce=coerce_none, render_kw={'class': "form-control form-control-md"})
    registration_date = DateField('Registration Date', validators=[DataRequired()], render_kw={'class': "form-control form-control-md"})
    component_file = FileField('Component File', validators=[DataRequired()], render_kw={'class': "form-control form-control-md"})
    submit = SubmitField('Register Component')

    def validate(self,extra_validators=None):
        #not really sure how this bit works
        if not super(RegisterComponentsForm, self).validate(extra_validators):
            return False

        try:
            self._validate_component_file(self.component_file)
        except wtfValidationError as e:
            self.component_file.errors.append(str(e))
            return False
        
        return True

    def validate_registration_date(self, field):
        if field.data > now_utc().date():
            raise wtfValidationError("Date cannot be in the future")
        
    def _validate_component_file(self, field):
        self.already_uploaded_components = []
        self.components = []

        df = register.read_component_file(field.data)
        df = df.where(pd.notnull(df), None) #replaces null with None
        if df.empty:
            raise wtfValidationError("Failed to read file, aborting upload")
        for _, row in df.iterrows():
            with db.session.no_autoflush:
                component = self.validate_component(row.to_dict())
                if component == 'already uploaded' or component is None:
                    continue
                self.components.append(component)
                db.session.add(component)

        if not self.components:
            wtfValidationError("No new components were uploaded!")
        
        db.session.flush()
        with db.session.no_autoflush:
            self.shipment = self.validate_shipment(self.components)
            db.session.add(self.shipment)
    
    def validate_component(self, row:dict) -> Union[mdl.Component, Literal['already uploaded']]:
        """Validates a single component with the database"""
        row = ComponentBase.model_validate(row)
        if component:=mdl.Component.get_by_sn(row.serial_number): 
            self.already_uploaded_components.append(component.serial_number)
            return 'already uploaded'

        component_type = self.validate_component_type(row.component_type)
        required_fields = component_type.required_keys
        meta_data = row.model_extra

        if Counter(meta_data.keys()) != Counter(required_fields):
            # https://stackoverflow.com/questions/8866652/determine-if-2-lists-have-the-same-elements-regardless-of-order
            raise wtfValidationError(f"You do not have all the required fields or they were mispelled for the Component Type = {row.component_type}, Required Fields: {required_fields} and you provided: {list(meta_data.keys())}")
        
        new_comp = mdl.Component(
            component_type=self.validate_component_type(row.component_type),
            serial_number=row.serial_number,
            user_created=current_user,
            vendor = row.vendor,
            meta_data = row.model_extra,
            current_location = self.get_origin()
        )
        # =======> Subassembly Registration
        if component_type.is_subassembly:
            if row.etroc_serial_number is None and row.lgad_serial_number is None:
                raise wtfValidationError("Subassembly registration requires etroc and lgad serial number columns")
            etroc = self.validate_subassembly_component(row.etroc_serial_number, self.get_origin())
            lgad = self.validate_subassembly_component(row.lgad_serial_number, self.get_origin())
            etroc.parent = new_comp
            lgad.parent = new_comp

        return new_comp
    
    def validate_shipment(self, registered_components: list[mdl.Component]) -> mdl.Shipment:
        origin = self.get_origin()
        registration_date = convert_date_to_datetime(self.registration_date.data, tz_info=None)
        shipment = mdl.Shipment(
            user_created = current_user,
            origin = origin,
            destination = origin,
            tracking_number = None,
            shipment_date = registration_date,
            reception_date = registration_date,
            shipment_method = None,
            components = registered_components,
            modules = [],
            created_at = now_utc() # this is needed or otherwise shipment wont have a created at date and would end up causing errors in shipment logic!
        )
        comp_passed, comp_msg = logistics.verify_shipment_parts(shipment.components,shipment)
        if not comp_passed:
            raise wtfValidationError(comp_msg)
        for comp in shipment.components:
            sub_comp_passed, sub_comp_msg = logistics.verify_shipment_parts(comp.components,shipment)
            if not sub_comp_passed:
                raise wtfValidationError(
                    'NOTE: The part that is referred to here is for the subcomponent that you are registering. There shipment dates have to make sense between the subassemblies and the shipments from ETROC and LGADs that make up the subassembly. '+sub_comp_msg)
            #check if it is in transit
            not_in_transit, error_message = logistics.check_in_transit(comp.components)
            if not not_in_transit: # I am so sorry, I wrote dumb code when starting this...
                raise wtfValidationError(error_message)
        return shipment

    def validate_component_type(self, component_type:str) -> mdl.ComponentType:
        comp_type = mdl.ComponentType.get_by_either(component_type)
        if comp_type is None:
            raise wtfValidationError(f'This "{component_type}" Component Type does not exist in the database please change it or register it') 
        return comp_type
    
    def validate_subassembly_component(self, serial_number, origin: mdl.Location) -> mdl.Component:
        """Validates either ETROC or LGAD that is part of a subassembly"""
        component = mdl.Component.get_by_sn(serial_number)
        if not component:
            raise wtfValidationError(f'This subassembly sub-component (ETROC or LGAD) "{serial_number}" does not exist in the database. To upload a bump bonded subassembly you first need to upload its ETROC and LGAD!')
        if component.parent: 
            raise wtfValidationError(f'This Child subassembly sub-component (ETROC or LGAD), (Child -> Parent), ({component.serial_number} -> {component.parent.serial_number}) already has this parent component.')
        if component.module:
            raise wtfValidationError(f'This subassembly sub-component (ETROC or LGAD) {component.serial_number} is already assocaited with a module {component.module.serial_number}. Ask an admin for guidance.')
        if component.current_location != origin:
            raise wtfValidationError(f"This subassembly sub-component ({component.serial_number}) is currently at ({component.current_location}) instead of your selected {origin}. If this was not a mistake, please register a shipment to update the parts current location.")
        return component

    def get_origin(self) -> mdl.Location:
        if self.origin.data is None:
            raise wtfValidationError("Please select a location.")
        origin = mdl.Location.get_by_id(int(self.origin.data)) # see how location choices are made in routes for why this is the id
        return origin
    
#--------Delete Parts Form---------#
    
class DeleteComponentsForm(FlaskForm):
    #file upload - csv with column of serial numbers is the only thing required
    #defualt is that it checks if component has shipment, observation, test or assembly with it
    #if it does, prevent the deletion
    #if it doesnt, delete the top level component and unassociate the children from the parent. If no parent then just delete
    #admin can override that and delete the module

    component_file = FileField('Component File', validators=[DataRequired()], render_kw={'class': "form-control form-control-lg"})
    #delete_data = BooleanField('Delete Data', validators=[Optional()])
    submit = SubmitField('Remove Components')


#-------Create Modules Form----------#
class ComponentPositionForm(FlaskForm):  
    module_sn = HiddenField(validators=[Optional()]) #do i actually use this?
    position = FieldList(StringField('Position',validators=[Optional(), ValidateDB(mdl.Component,'serial_number', in_db=True)], render_kw={'class': "form-control"}), min_entries=0)

class RegisterModuleForm(FlaskForm):
    module_prep = FieldList(FormField(ComponentPositionForm), min_entries=0)
    submit = SubmitField('Submit')


    def validate(self,extra_validators=None):
        #not really sure how this bit works
        if not super(RegisterModuleForm, self).validate(extra_validators):
            return False

        filled_form = True
        for mod_form in self.module_prep.entries:
            if all(i.strip()=='' for i in mod_form.data['position']): #if all are empty strings, you need atleast one component
                mod_form.position[-1].errors.append('Please associate atleast one component with the module')
                filled_form = False
        return filled_form


class PreRegisterModuleForm(FlaskForm):
    module_type = StringField('Module Type', validators=[DataRequired(), ValidateDB(mdl.ModuleType,'name_long', in_db=True)], render_kw={"placeholder": "What kind of module?",'class': "form-control"})
    assembly_location = StringField('Assembly Location', validators=[DataRequired(), ValidateDB(mdl.Location,'name_long',in_db=True)], render_kw={"placeholder": "Assembly Location",'class': "form-control"})
    number_modules = IntegerField('Number of Modules',validators=[DataRequired(),NumberRange(min=1,max=400)], render_kw={"placeholder": "Number of Modules ", 'class': "form-control"}) 
    submit_pre = SubmitField('Submit')