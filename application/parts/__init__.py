from flask import Blueprint

parts_bp = Blueprint('parts', __name__, template_folder='templates',static_folder='static')

from application.parts import routes