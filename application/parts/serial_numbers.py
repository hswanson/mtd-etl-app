from application import db_models as mdl
import re

def last_sn_number(sn_prefix,mod=True):
    matched_parts = mdl.Module.query.filter(mdl.Module.serial_number.contains(sn_prefix)).all()
    if mod == False:
        matched_parts = mdl.Component.query.filter(mdl.Component.serial_number.contains(sn_prefix)).all()
    n_last=0
    for part in matched_parts:
        n = int(re.findall('\d+',part.serial_number)[-1])
        if n>n_last:
            n_last=n    
    return n_last

def gen_sns(n_start, n_parts, sn_prefix):
    n_max = 9999 #otherwise you go over the limit
    suffix = '0000'
    if n_start<=n_max:
        start_suffix = suffix[:-len(str(n_start))] + str(n_start)
    else:
        return None
    SN_start = sn_prefix + start_suffix
    return [SN_start[:-len(str(n))] + str(n) for n in range(n_start,n_start + n_parts) if n<=n_max]  

def get_mod_sns(n_mods, mod_type_name, assy_loc_name): #name_long
    if not n_mods:
        n_mods = 1
    else:
        n_mods = n_mods  
    if int(n_mods) >= 400:
        n_mods = 400
    prefix = mod_type_name + assy_loc_name
    n_start = 1
    module_type = mdl.ModuleType.get_by_long_name(mod_type_name)
    assembly_location = mdl.Location.get_by_long_name(assy_loc_name)
    
    if module_type and assembly_location:
        prefix = module_type.name_abbv+assembly_location.name_abbv
        n_start = last_sn_number(prefix)+1 #plus one so you go one more after the last number :)
    elif module_type and not assembly_location:
        prefix = module_type.name_abbv + assy_loc_name
    elif not module_type and assembly_location:
        prefix = mod_type_name + assembly_location.name_abbv

    return gen_sns(n_start, int(n_mods),prefix)