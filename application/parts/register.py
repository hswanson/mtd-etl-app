from os.path import splitext
import pandas as pd

def read_component_file(file):
    _, file_extension = splitext(file.filename)
    try:
        if file_extension == ".csv":
            df = pd.read_csv(file, index_col=None)
        elif file_extension in [".xlsx", ".xls"]:
            df = pd.read_excel(file, index_col=None)
        elif file_extension == '.ods':
            df = pd.read_excel(file, index_col=None, engine='odf')
        else:
            return pd.DataFrame()
    except UnicodeDecodeError:
        #sometimes if excel is made to csv it cant be done right and this happens.
        return pd.DataFrame()
    return df.applymap(lambda x: None if pd.isna(x) else x) #get rid of pd.NA for parsing and things.   


def valid_comp_for_user(comp, loc):
    return (not comp.parent and comp.current_location==loc and comp.is_recieved)   
