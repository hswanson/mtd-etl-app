from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, DateField, SelectField, BooleanField
from wtforms.validators import DataRequired, Optional
from application import db_models as mdl
from application.form_utils import ValidateDB
from application.datetime_handler import now_utc

from flask_login import current_user
from application import db #get database ORM object
from application import db_models as mdl
from application.shipments import logistics
from application.datetime_handler import convert_date_to_datetime

def form_parts_to_model(form_parts, model):
    if not form_parts: #empty string or what have you
        return []
    elif ',' not in form_parts:
        #check if it is in db and if so make it a list 
        part = model.get_by_sn(form_parts)
        if part:
            #one single sn so just put it in a list!
            return [part]
        else:
            return []
    else:
        part_sns = form_parts.split(',')
        return [model.get_by_sn(sn) for sn in part_sns]


def create_shipment_form(shipment_id:int=None) -> FlaskForm:
    class ShipmentForm(FlaskForm):
        shipment_date = DateField('Shipment Date', validators=[DataRequired()],render_kw={'class': "form-control"}) #try to do default utcnow
        
        #USED ONLY WHEN VERIFYING FORM, FOR SHIPMENT IT IS UNRENDERED AND FOR VERIFY SHIPMENT IT IS RENDERED
        #--------------------------------------------------------------------------------------------------------#
        reception_date = DateField('Reception Date', validators=[Optional()],render_kw={'class': "form-control"})
        #--------------------------------------------------------------------------------------------------------#   
        shipment_method = SelectField('Shipping Company', validators=[DataRequired()], choices=[('courier','-- Choose Courier --'),('fedex','Fedex'),('ups','UPS'),('usps','USPS'),('other','Other')], render_kw={'class': "form-control"})
        tracking_number = StringField('Tracking Number', validators=[Optional()], render_kw={'class': "form-control"})
        origin = SelectField('Origin', validators=[DataRequired(), ValidateDB(mdl.Location,'id',in_db=True)], render_kw={'class': "form-control"}) #, coerce=coerce_none
        destination = SelectField('Destination', validators=[DataRequired(), ValidateDB(mdl.Location,'id',in_db=True)], render_kw={'class': "form-control"}) #coerce=coerce_none,
        destination_components_field = HiddenField(validators=[Optional()])
        destination_modules_field = HiddenField(validators=[Optional()])
        check_parts = BooleanField('Everything look ok?', validators=[DataRequired()])#, widget=HiddenInput())
        submit = SubmitField('Submit')

        def validate(self,extra_validators=None):
            #not really sure how this bit works
            if not super(ShipmentForm, self).validate(extra_validators):
                return False
            
            #this I get, makes sure that you input at least one module or component serial number, last is so you can empty shipments but it has to be in the verify page
            if not self.destination_modules_field.data and not self.destination_components_field.data and not self.reception_date:
                self.destination_modules_field.errors.append('Please select a module or component to be shipped')
                self.destination_components_field.errors.append('Please select a module or component to be shipped')
                return False  
            components = form_parts_to_model(
                self.destination_components_field.data, 
                mdl.Component
            )
            modules = form_parts_to_model(
                self.destination_modules_field.data, 
                mdl.Module
            )
            # MAKE SHIPMENT PAGE
            if shipment_id is None:
                if self.origin.data == self.destination.data:
                    msg="Shipments to self is not allowed"
                    self.origin.errors.append(msg)
                    self.destination.errors.append(msg)
                    return False
                if not self._check_in_transit(modules, components):
                    # this is probably not needed but for redudancy? Probably is dumb
                    return False
                self._create_shipment(components, modules)
            # VERIFY SHIPMENT PAGE
            else:
                self._update_shipment(shipment_id, components, modules)
                if self.reception_date.data: #if reception date was given in verify shipment page, it is optional
                    if self.reception_date.data < self.shipment_date.data:
                        msg="Reception date cannot be before shipment date"
                        self.reception_date.errors.append(msg)
                        return False
                    if self.reception_date.data > now_utc().date():
                        msg="Reception data cannot be in the future"
                        self.reception_date.errors.append(msg)
                        return False
            if not self._verify_shipment_parts():
                return False    
            return True
    
        def _check_in_transit(self, components: list[mdl.Component], modules: list[mdl.Module]) -> None:
            mod_tr_check = logistics.check_in_transit(modules)
            comp_tr_check = logistics.check_in_transit(components)
            if not mod_tr_check[0]:
                self.destination_modules_field.errors.append(mod_tr_check[1])
                return False
            if not comp_tr_check[0]:
                self.destination_components_field.errors.append(comp_tr_check[1])
                return False
            return True

        def _create_shipment(self, components: list[mdl.Component], modules: list[mdl.Module]) -> None:
            self.shipment = mdl.Shipment(
                user_created=current_user,
                origin=mdl.Location.get_by_id(self.origin.data),
                destination=mdl.Location.get_by_id(self.destination.data),
                tracking_number=self.tracking_number.data,
                shipment_date=convert_date_to_datetime(self.shipment_date.data, tz_info=None),
                reception_date=None,
                shipment_method=self.shipment_method.data,
                components=components,
                modules=modules,
                created_at=now_utc()
            )
            db.session.add(self.shipment)

        def _update_shipment(self, shipment_id:int, components: list[mdl.Component], modules: list[mdl.Module]) -> None:
            self.shipment = mdl.Shipment.get_by_id(shipment_id)
            if self.shipment is None:
                raise ValueError("Shipment id does not exist")

            # if part in old shipment is no longer in the form_components
            # ---> update current location
            for comp in self.shipment.components:
                if comp not in components:
                    comp.current_location = self.shipment.origin
            for mod in self.shipment.modules:
                if mod not in modules:
                    mod.current_location = self.shipment.origin

            self.shipment.destination = mdl.Location.get_by_id(self.destination.data)
            self.shipment.components = components
            self.shipment.modules = modules
            self.shipment.shipment_method = self.shipment_method.data
            self.shipment.tracking_number = self.tracking_number.data
            self.shipment.reception_date = convert_date_to_datetime(self.reception_date.data, tz_info=None)
            self.shipment.shipment_date = convert_date_to_datetime(self.shipment_date.data, tz_info=None)
            self.shipment.created_at = self.shipment.created_at

        def _verify_shipment_parts(self):
            mod_passed, mod_msg = logistics.verify_shipment_parts(self.shipment.modules, self.shipment)
            if not mod_passed:
                self.destination_modules_field.errors.append(mod_msg)
                return False

            for mod in self.shipment.modules:
                mod_comp_passed, mod_comp_msg = logistics.verify_shipment_parts(mod.components, self.shipment)
                if not mod_comp_passed:
                    self.destination_components_field.errors.append(mod_comp_msg)
                    return False

            comp_passed, comp_msg = logistics.verify_shipment_parts(self.shipment.components, self.shipment)
            if not comp_passed:
                self.destination_components_field.errors.append(comp_msg)
                return False
            return True
    return ShipmentForm()   
    