from flask import render_template_string
from application import yag, db
from application import db_models as mdl
from os.path import join
from config import app_dir

def email_template(template_path):
    with open(template_path, 'r') as file_content:
        template = file_content.read()
    return template

class ShipmentEmail:
    def __init__(self, template_file: str, shipment: mdl.Shipment, sender: mdl.User, subject: str = None):
        
        # Shipping Info - Can be overwritten
        self.origin: mdl.Location            = shipment.origin
        self.destination: mdl.Location       = shipment.destination
        self.tracking_number: str            = shipment.tracking_number
        self.modules: list[mdl.Module]       = shipment.modules
        self.components: list[mdl.Component] = shipment.components
        self.shipment_method: str            = shipment.shipment_method
        self.sender: mdl.User                = sender #could be user_edited of the shipment potentially one day to simplify further

        # Template path
        self.template_file = template_file
        self.template_dir = join(app_dir,'shipments','templates','email')
        self.subject = subject
    
    def send(self) -> None:
        """
        Sends email to all users at the destination site.
        """
        if self.subject is None:
            raise ValueError("Shipment subject cannot be None")

        active_users_at_dest = db.select(mdl.User).where(mdl.User.site == self.destination, mdl.User.is_active == True)
        for u in db.session.execute(active_users_at_dest).scalars().all():
            message = render_template_string(
                email_template(join(self.template_dir, self.template_file)), 
                email=self,
                recipient=u
            )
            yag.send(u.email, self.subject, message)
