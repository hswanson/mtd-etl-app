from flask import Blueprint

shipments_bp = Blueprint('shipments', __name__, template_folder='templates',static_folder='static')

from application.shipments import routes