from application.shipments import shipments_bp #get flask app
from application import db, yag #get database ORM object
from application import db_models as mdl
from application.shipments import logistics, forms, email
from application.tables.search import ModuleQueryBuilder, ComponentQueryBuilder, QueryDirector
from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import current_user, login_required


@shipments_bp.route('/make-shipment/', methods=['GET','POST'])
@login_required
def make_shipment():
    form = forms.create_shipment_form()
    del form.reception_date

    undefined_loc = mdl.Location.get_by_long_name('Undefined')
    transit_loc= mdl.Location.get_by_long_name('Transit')
    location_choices = [(str(loc.id), loc.name_long) for loc in mdl.Location.query.order_by('id') if loc.id != transit_loc.id] #need ids because of weird different object thing <obj id=1> != <obj Nebraska>
    form.origin.choices = [('','Choose Origin')] + location_choices + [(undefined_loc.id, undefined_loc.name_long)] #+ [(transit_loc.id, transit_loc.name_long)]
    form.destination.choices = [('','Choose Destination')] + location_choices
    
    # FOR SOME REASON IF I DISABLE A FIELD IN THE VERY SHIPMENT FORM IT GETS PROPOGATED TO HERE...
    if 'disabled' in form.origin.render_kw:
        del form.origin.render_kw['disabled']
    if 'disabled' in form.destination.render_kw:
        del form.destination.render_kw['disabled']
        
    if form.validate_on_submit():
        shipment = form.shipment
        # Set Current Location to Transit
        for part in shipment.components + shipment.modules:
            part.current_location = mdl.Location.get_by_either('Transit')
        if yag:
            #grab all users at destination
            subject = f"ETL Shipment ID={shipment.id}: {shipment.origin.name_long} TO {shipment.destination.name_long}"
            new_email = email.ShipmentEmail('shipment.txt', shipment, current_user, subject = subject)
            new_email.send()

        mdl.Event.record_event(mdl.Event.INFO,mdl.Event.HIGH,f'Shipment registered by {current_user.username} for {shipment.origin.name_long} going to {shipment.destination.name_long}')
        db.session.commit()

        flash("Success!", category='success')
        return redirect(url_for('tables.shipment_table'))

    return render_template('shipment.html', make_shipment=True, title='Shipment', form=form, shipment=None)

@shipments_bp.route('/verify-shipment/<shipment_id>', methods=['GET','POST'])
@login_required
def verify_shipment(shipment_id): 
    #function for sending these shipment emails
    shipment = mdl.Shipment.get_by_id(shipment_id)

    undefined_loc = mdl.Location.get_by_long_name('Undefined')
    transit_loc= mdl.Location.get_by_long_name('Transit')
    location_choices = [(str(loc.id), loc.name_long) for loc in mdl.Location.query.order_by('id') if loc.id != transit_loc.id] #need ids because of weird different object thing <obj id=1> != <obj Nebraska>
    first_choice = [('','-- Select A Location --')]

    form = forms.create_shipment_form(shipment_id=shipment_id)
    form.destination.choices = first_choice + location_choices
    form.origin.choices      = first_choice + location_choices

    form.origin.render_kw['disabled'] = 'disabled'
    form.origin.data = str(shipment.origin.id) #manually set the data of the form so it is not None
    # if any part has a new shipment, disable the changing of destination and set the data of the form so it is not None
    if already_shipped_again:=any(shipment!=logistics.find_last_shipment(part.shipments) for part in shipment.components + shipment.modules):
        form.destination.render_kw['disabled'] = 'disabled'
        form.destination.data = str(shipment.destination.id)
    #needs to be above the form.process as that clears the csrf token
    if form.validate_on_submit():
        # FOR EMAILS
        old_destination = shipment.destination
        destination_is_changed = form.destination.data != shipment.destination_id

        shipment = form.shipment
        #---------------Set current location of components and shipments---------------#
        for part in shipment.components + shipment.modules:
            # if this shipment is the latest shipment or if shipment is a new shipment ie in transit          
            if logistics.find_last_shipment(part.shipments) == shipment and shipment.is_recieved:
                part.current_location = shipment.destination
        #------------------------------------------------------------------------------#
        if yag:
            # Cancel shipment when destination changes
            if destination_is_changed:
                # Send Cancellation Email to old destination
                subject_cancelled = f"ETL CANCELLED Shipment ID={shipment_id}: {shipment.origin.name_long} TO {old_destination.name_long}"
                cancelled_email = email.ShipmentEmail('cancelled_shipment.txt', shipment, current_user, subject = subject_cancelled)
                cancelled_email.destination = old_destination # this is so we send the cancellation email to the place BEFORE THE destination changed

                # Send New email to new destination
                subject = f"ETL Shipment ID={shipment_id}: {shipment.origin.name_long} TO {shipment.destination.name_long}"
                new_email = email.ShipmentEmail('shipment.txt', shipment, current_user, subject = subject)

                cancelled_email.send()
                new_email.send()

        db.session.commit()
        return redirect(url_for('tables.shipment_table'))
    
    #load defaults from shipment
    form.origin.default = str(shipment.origin.id)
    form.destination.default = str(shipment.destination.id) #need to string bec of forms
    form.shipment_method.default = shipment.shipment_method
    form.tracking_number.default = shipment.tracking_number
    form.shipment_date.default = shipment.shipment_date
    form.reception_date.default = shipment.reception_date
    form.process()

    return render_template('shipment.html', 
                           make_shipment=False, 
                           title='Verify Shipment', 
                           form=form, 
                           shipment_id = shipment.id,
                           shipment=shipment, 
                           )

@shipments_bp.route('/api/get-parts/')
@login_required
def parts_search():
    tablename = request.args.get('tablename')
    location  = request.args.get('location')
    search    = request.args.get('search', '')
    # ----------------------- GAURD CLAUSES ----------------------- #
    if tablename is None:
        return jsonify(message="API ERROR: Tablename not specified as query string parameter"), 406
    if tablename not in ['component', 'module']:
        return jsonify(message=f"API ERROR: Tablename ({tablename}) is not a valid part table (component or module)"), 406
    if location is None:
        return jsonify(message="API ERROR: Location not specified as query string parameter"), 406
    # catch case where the location is back to default -> return no parts
    if location == '':
        return []
    location = mdl.Location.get_by_id(location)
    if location is None:
        return jsonify(message=f"API ERROR: Location (ID={location}) does not exist in the database"), 406
    # ------------------------------------------------------------- #

    query_director = QueryDirector(search)
    # Disable current location search by string, then do it manually!
    if tablename == 'component':
        query_director.builder = ComponentQueryBuilder(disable_current_location=True)
    else:
        query_director.builder = ModuleQueryBuilder(disable_current_location=True)
    
    query_director.builder.query_current_location([location.name_long], '=') #could add a feature to the search to accept string or list of strings...
    query = query_director.build_query()
    parts = []
    for part in db.session.execute(query.limit(300).order_by(query_director.builder.db_model.id)).scalars().all():
        if   hasattr(part, 'module') and part.module is not None:
            continue # this is a lower level part (component that is a part of a module)
        elif hasattr(part, 'parent') and part.parent is not None:
            continue # this is a lower level part (etroc or lgad apart of a subassembly)
        parts.append(part.serial_number)

    # Removing any duplicates, just in case but is not needed if the above code will always be perfect!
    # https://stackoverflow.com/questions/480214/how-do-i-remove-duplicates-from-a-list-while-preserving-order
    seen = set()
    seen_add = seen.add
    return jsonify([x for x in parts if not (x in seen or seen_add(x))])


@shipments_bp.route('/api/parts-from-shipment/', methods=['GET','POST'])
@login_required
def parts_from_shipment():
    shipment_id = request.get_data().decode()
    
    shipment = mdl.Shipment.get_by_id(shipment_id)
    if not shipment:
        return jsonify(message=f"API ERROR: Shipment ID={shipment_id} does not exist in the database"), 406

    comp_destination_parts = {}
    for comp in shipment.components:
        comp_destination_parts[comp.serial_number] = {'already_shipped':(shipment!=logistics.find_last_shipment(comp.shipments)), 'already_new_part':(comp.module is not None or comp.parent is not None)}
    
    mod_destination_parts = {}
    for mod in shipment.modules:
        mod_destination_parts[mod.serial_number] = {'already_shipped':(shipment!=logistics.find_last_shipment(mod.shipments)), 'already_new_part':False}

    return jsonify({"components": comp_destination_parts, "modules": mod_destination_parts})
