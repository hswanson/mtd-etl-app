from application import db_models as mdl

def find_last_shipment(part_shipments): #part_shipments is for example module.shipments, all shipments for a part
    from application.errors.handlers import ShipmentLogicError
    #looks back at the shipments for a module or component and use that to get the current location
    if not part_shipments:
        #meaning there arent any shipments return None
        return None
    
    latest_shipment = part_shipments[0] #initialize to be first shipment
    for shipment in part_shipments:
        if shipment == latest_shipment:
            #latest shipment will get returned so if this is the only element in the list than it will just get returned!
            continue
        #Case: in transit
        elif not shipment.is_recieved:
            #meaning it is in transit, forced to be latest shipment 
            #due to the constraint of a component not being able to be in more than one shipment at the same time
            return shipment 
        elif shipment.reception_date is None:
            raise ShipmentLogicError("Broken Shipment Tree: Reception date for an old/non latest shipment was removed.")
        #Case: shipment happened after the current latest shipment
        elif shipment.reception_date > latest_shipment.reception_date:
            #if latest shipment is before shipment being iterated make it latest shipment
            latest_shipment = shipment
        #unlikely Case: Shipments happened and received in the same day
        elif latest_shipment.reception_date == shipment.reception_date:
            #unlikely case two shipments happen and are recieved on the same day -> go based off of created at
            if shipment.created_at.timestamp() > latest_shipment.created_at.timestamp(): #maybe we do edited at? but i dont have edited at widely implemented womp womp
                latest_shipment = shipment
    return latest_shipment

def get_shipment_history(part):
    shipment_pool = [shipment for shipment in part.shipments]
    
    sorted_shipments = []
    #selection sort, get me on leetcode
    for i in range(len(shipment_pool)):
        last_shipment = find_last_shipment(shipment_pool)
        sorted_shipments.append(last_shipment)
        shipment_pool.remove(last_shipment)
    return sorted_shipments[::-1] #put it in chronological order
    
def check_in_transit(parts):
    for part in parts:
        if calc_current_location(part) == mdl.Location.get_by_long_name('Transit'):
            msg = f'{part.serial_number} CURRENTLY IN TRANSIT, update shipment if you have recieved this part!!'
            return (False, msg)    
    return (True, '')   

def verify_part_for_shipment(part, current_shipment):
    #compare shipments
    def check_shipment_history(shipment_history,current_shipment):
        def shipments_are_chronological(before_shipment, current_shipment, after_shipment):
            if before_shipment is None and after_shipment is None:
                #very first shipment recorded so dates can be anything
                return (True, "")
            elif before_shipment is None:
                #means the current shipment is the first shipment
                if current_shipment.reception_date < after_shipment.shipment_date:
                    return (True, "")
                elif current_shipment.reception_date == after_shipment.shipment_date:
                    if current_shipment.created_at.timestamp() < after_shipment.created_at.timestamp():
                        return (True, "")
                return (False, f"the reception date ({current_shipment.reception_date}) of your shipment cant be after the shipment date ({after_shipment.shipment_date}) of the part in its next shipment")
            elif after_shipment is None:
                #means the current shipment is the latest shipment
                if current_shipment.shipment_date > before_shipment.reception_date:
                    return (True, "")
                elif current_shipment.shipment_date == before_shipment.reception_date:
                    print("Same date")
                    print(current_shipment.created_at, current_shipment.created_at.timestamp())
                    print(before_shipment.created_at, before_shipment.created_at.timestamp())
                    if current_shipment.created_at.timestamp() > before_shipment.created_at.timestamp():
                        #use timestamp just in case with lost time zone stuff
                        return (True, "")    
                return (False, f"Your shipment date ({current_shipment.shipment_date}) cant be before the parts previous reception date ({before_shipment.reception_date}) (in its past shipment)")          
            else:
                #we have surround shipments in the history
                if current_shipment.reception_date < after_shipment.shipment_date and current_shipment.shipment_date > before_shipment.reception_date:
                    return (True, "")
                elif current_shipment.reception_date == after_shipment.shipment_date or current_shipment.shipment_date == before_shipment.reception_date:
                    #this is important to preserve! because of last shipment calculation uses this created at time and therefore the current location of parts depends on it
                    if current_shipment.created_at.timestamp() > after_shipment.created_at.timestamp():
                        return (False, f"Time of creation of same day shipments need to follow chronological order")
                    elif current_shipment.created_at.timestamp() < before_shipment.created_at.timestamp():
                        return (False, f"Time of creation of same day shipments need to follow chronological order") 
                    else:
                        return (True, "")                                       
                return (False, f"Shipment Dates (SD) and Reception Dates (RD) need to follow choronological order w.r.t the previous and following shipments: (Prev RD: {before_shipment.reception_date}) < (Your SD: {current_shipment.shipment_date} < (Your RD: {current_shipment.reception_date}) < (Next SD: {after_shipment.shipment_date}))") 
        #get before, and after shipments
        #meaning it is a new shipment
        if not shipment_history:
            return (True, "")
        elif current_shipment not in shipment_history:
            return shipments_are_chronological(shipment_history[-1], current_shipment, None)
        else:
            #not a new shipment
            ship_index = shipment_history.index(current_shipment)
            if ship_index == 0:
                #first shipment case
                if len(shipment_history) > 1:
                    return shipments_are_chronological(None, current_shipment, shipment_history[ship_index+1])
                else:
                    #first AND only shipment
                    return shipments_are_chronological(None, current_shipment, None)
            elif ship_index == len(shipment_history)-1:
                #newest shipment
                return shipments_are_chronological(shipment_history[ship_index-1], current_shipment, None)
            else:
                #somewhere in the middle
                return shipments_are_chronological(shipment_history[ship_index-1], current_shipment, shipment_history[ship_index+1])
            
    verified_details = { #(False, message if it failed)
        'reception_date_verification': True, 
        'shipment_date_verification': True,
        'error_message': None
    }
    shipment_history = get_shipment_history(part)
    ship_pass, ship_error = check_shipment_history(shipment_history,current_shipment)
    if not ship_pass:
        #compare shipment with history returns true if the dates make sense and false if it doesnt make sense
        verified_details['reception_date_verification'] = False
        verified_details['error_message'] = f"For part: {part.serial_number} "+ship_error
        return verified_details
    return verified_details

def verify_shipment_parts(parts, shipment):
    def check_verify(verified_details):
        for key in ['reception_date_verification','shipment_date_verification']:
            if not verified_details[key]:
                return verified_details['error_message']
            
    for part in parts:
        comp_details = verify_part_for_shipment(part, shipment)
        if (msg := check_verify(comp_details)):
            return (False, msg)
    return (True, "")

def calc_current_location(part):
    #looks back at the shipments and use that to get the current location
    part_shipments = [s for s in part.shipments] #need to do this otherwise it effects db!

    # these two if statements simply grab all the shipments for the part (and handle 1 edge case for the module)
    if part.__tablename__ == 'module':
        if not part_shipments: #module with no shipments
            mod_components = part.components
            if not mod_components:
                #if a module has no shipments and has no components its location is undefined
                return mdl.Location.get_by_long_name('Undefined')
            
            #check all components have the same current location. Can use it here because components dont have tablename module
            comp_locations = [calc_current_location(comp) for comp in mod_components]
            if comp_locations[:-1] == comp_locations[1:]: #if they are all the same
                return comp_locations[0]
            else:
                #if they are not all equal then set it to be undefined as a module has components in multiple locations
                return mdl.Location.get_by_long_name('Undefined')
            
    elif part.__tablename__ == 'component':
        if parent_comp := part.parent:
            #ONLY GOES THROUGH TREE DEPTH ONCE, IF WE NEED TO GET ALL parents of parents then upgrade this...
            #if comp has a parent then add those shipments too
            part_shipments += parent_comp.shipments    
            #technically this is not needed to get if module has shipments buuuuttt to be safe and thorough        
        if comp_mod := part.module:
            #if the component is part of a module, add the modules shipments
            part_shipments += comp_mod.shipments
    else:
        #does not make sense to call current location on anything else!
        return None

    last_shipment = find_last_shipment(part_shipments) 
    
    if last_shipment is None:
        return mdl.Location.get_by_long_name('Undefined')
    else:
        if last_shipment.is_recieved:
            return last_shipment.destination
        else:
            return mdl.Location.get_by_long_name('Transit')