from flask_wtf import FlaskForm
from wtforms import SubmitField

class ExportLookupsForm(FlaskForm):
    submit = SubmitField('Export Lookups')