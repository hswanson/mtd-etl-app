// <script src="..." switch_id1="" switch_id2="" div_id1="" div_id2="">

//get ids
var left_switch_id = document.currentScript.getAttribute("switch_id1");
var right_switch_id = document.currentScript.getAttribute("switch_id2");
var left_div_id = document.currentScript.getAttribute("div_id1");
var right_div_id = document.currentScript.getAttribute("div_id2");
var output_div_id = document.currentScript.getAttribute("output_div");

//grab elements by id
var left_switch = document.getElementById(left_switch_id);
var right_switch = document.getElementById(right_switch_id);
var left_div = document.getElementById(left_div_id);
var right_div = document.getElementById(right_div_id);
var output_div = document.getElementById(output_div_id);

//default turn left one on
left_div.style.display = "block";
right_div.style.display = "none";

left_switch.checked=true;

left_switch.addEventListener("click", function (){
    left_div.style.display = "block";
    right_div.style.display = "none";
    output_div.innerHTML = "";

});

right_switch.addEventListener("click", function(){
    left_div.style.display = "none";
    right_div.style.display = "block";
    removeUpload();
});