
var output_div_id = document.currentScript.getAttribute("output_div");
var output_div = document.getElementById(output_div_id);

function readURL(input) {
    if (input.files && input.files[0]) {                      // if input is file, files has content
        var inputFileData = input.files[0];                     // shortcut
        var reader = new FileReader();                          // FileReader() : init
        var myFormData = new FormData();
        myFormData.append('file', inputFileData);

        reader.onload = function(e) {                           /* FileReader : set up ************** */
        $('.file-upload-placeholder').hide();                 // call for action element : hide
        $('.file-upload-image').attr('src', e.target.result); // image element : set src data.
        $('.file-upload-preview').show();                     // image element's container : show
        $('.image-title').html(inputFileData.name);           // set image's title
        };
        reader.readAsDataURL(inputFileData);     // reads target inputFileData, launch `.onload` actions

        $.ajax({
            url: '/admin/register_component/api/render-csv',
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: myFormData,
            success: function(result) {
                const mygrid = new gridjs.Grid({
                    columns: result['columns'],
                    data: () => result['data'],
                    pagination: true,
                    resizable: true,
                }).render(output_div);

                mygrid.forceRender();

            }
        }); 

    } else { removeUpload(); }

}

function removeUpload() {
    var $clone = $('.file-upload-input').val('').clone(true); // create empty clone
    $('.file-upload-input').replaceWith($clone);              // reset input: replaced by empty clone
    $('.file-upload-placeholder').show();                     // show placeholder
    $('.file-upload-placeholder').removeClass('image-dropping');
    $('.file-upload-preview').hide();                         // hide preview
    //mygrid.clear();
    output_div.innerHTML = "";
}

// Style when drag-over
$('.file-upload-placeholder').bind('dragover', function () {
$('.file-upload-placeholder').addClass('image-dropping');
});
$('.file-upload-placeholder').bind('dragleave', function () {
$('.file-upload-placeholder').removeClass('image-dropping');
});
