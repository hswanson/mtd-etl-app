#general imports
from flask import redirect, url_for, flash, request, send_file
from flask_login import current_user

from application import db

from application import db_models as mdl


from application.admin.forms import ExportLookupsForm

#flask-admin imports
from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_admin import BaseView, expose
#look into File Admin too for later :)

import json
import io

#----------secure admin page using flask login----------#

def admin_required(cls):
    def is_accessible(self):
        return (current_user.is_authenticated and current_user.is_admin)
    
    def _handle_view(self, name, **kwargs):
        #redirects to login if you try to go to admin directly
        #consider adding a flash for loggin in as admin!
        if not self.is_accessible():
            #doesn't handle the next correctly but that is ok)
            return redirect(url_for('auth.login', next=request.url)) 
        
    #adds these methods to the class its decorating   
    setattr(cls, 'is_accessible', is_accessible)
    setattr(cls, '_handle_view', _handle_view)
    return cls

#-----------ADMIN VIEWS---------------#
@admin_required
class SecureAdminIndexView(AdminIndexView):
    @expose('/')
    def index(self):
        return self.render('index.html')


@admin_required
class SecureBaseView(BaseView):
    pass

class AdminExportLookups(SecureBaseView):
    @expose('/',methods=['GET','POST'])
    def export_lookups(self):
        form = ExportLookupsForm()
        if form.validate_on_submit():
            #for each of the lookup tables make a dictionary 
            models = [mdl.Location, mdl.ModuleType, mdl.ComponentType]
            lookups_dict = {}
            for model in models:
                lookups_dict[model.__tablename__] = [obj.to_dict() for obj in model.get_all()]

            lookups_json = json.dumps(lookups_dict, indent=4)
            mem_lookups = io.BytesIO(lookups_json.encode())
            mem_lookups.seek(0)
            
            return send_file(mem_lookups, mimetype='application/json', as_attachment=True, download_name='BASE_DB_DATA.json')
        return self.render('export_lookups.html', title='Export Lookups', form=form)


class MainPage(BaseView):
    @expose('/', methods=['GET','POST'])
    def return_main(self):
        return redirect(url_for('main.home'))

#@login_required
class SecureLookup(ModelView):
    can_delete = False

    # can_edit = False
    #shared banned columns between all the lookup tables
    form_excluded_columns = [
        'user_edited',
        'last_edited',
        'component',
        'components',
        'module',
        'modules',
        'tests',
        'assembly',
        'test',
        'observation',
        'users', 
        'files', 
        'test_types', 
        'assembly_types'
    ]
    
    column_formatters = {
        'user_created': lambda v, c, m, p: m.user_created.username if m.user_created else None,
    }
    def is_accessible(self):
        #https://stackoverflow.com/questions/63337371/flask-admin-how-to-set-form-edit-rules-or-form-create-rules-based-on-role-of-u 
        if current_user.is_authenticated and current_user.is_admin:
            self.can_delete = False
            self.can_edit = True
            self.can_create = True
            self._refresh_form_rules_cache() #need to do this to change form_edit_rules
            return True
        elif current_user.is_authenticated and not current_user.is_admin:
            self.can_delete = False
            self.can_edit = False
            self.can_create = False
            self._refresh_form_rules_cache() #need to do this to change form_edit_rules
            return True
        else:
            return False
    
    def _handle_view(self, name, **kwargs):
        #redirects to login if you try to go to admin directly
        #consider adding a flash for loggin in as admin!
        if not self.is_accessible():
            #doesn't handle the next correctly but that is ok)
            return redirect(url_for('auth.login', next=request.url)) 


class SecureModType(SecureLookup):
    column_list = ['name_long', 'name_abbv', 'description','mtddb_zz_int', 'mtddb_required']
    column_names = ['Name', 'Abbv', 'Description', 'MTDdb ZZ Code','MTDdb Required']
    column_labels = dict(zip(column_list,column_names))
    def on_model_change(self, form, model, is_created):
        matched_comptype = None
        if form.mtddb_zz_int.data: #if it is None then you dont care and can be passed
            matched_comptype = mdl.ComponentType.query.filter_by(mtddb_zz_int=model.mtddb_zz_int).first()
        
        if matched_comptype:
            raise ValueError('mtddb_zz already exists in ComponentType')

class SecureCompType(SecureLookup):
    column_list = ['name_long', 'name_abbv', 'description', 'required_keys','mtddb_zz_int', 'mtddb_required', 'is_subassembly']
    column_names = ['Name', 'Abbv', 'Description', 'Metadata Keys', 'MTDdb ZZ Code','MTDdb Required', "Is Subassembly"]
    column_labels = dict(zip(column_list,column_names))
    def on_model_change(self, form, model, is_created):
        matched_modtype = None
        if form.mtddb_zz_int.data: #if it is None then you dont care and can be passed
            matched_modtype = mdl.ModuleType.query.filter_by(mtddb_zz_int=model.mtddb_zz_int).first()
        
        if matched_modtype:
            raise ValueError('mtddb_zz_int already exists in ModuleType')

@admin_required
class SecureUser(ModelView):
    column_list = ['username','email','site.name_long','is_admin','is_active','has_logged_in','user_created_id','user_edited_id']
    column_names = ['Username','Email', 'Site', 'Is Admin', 'Is Active', 'Has Logged In','Created By','Edited by']
    column_labels = dict(zip(column_list,column_names))
    can_delete = False
    can_create = False #make this a config variable, remove edit and delete for lookup tables, only create

@admin_required
class SecureModule(ModelView):
    column_list = ['user_created.username','module_type.name_long','serial_number','assembly_location.name_long','user_edited.username', 'mtddb_synced']
    column_names = ['Created By','Module Type', 'Serial Number', 'Assembly Location','Edited by', 'MTDdb Synced']
    column_labels = dict(zip(column_list,column_names))
    can_delete = False
    can_create = False

@admin_required
class SecureComponent(ModelView):
    column_list = ['id','user_created.username','component_type.name_long','serial_number','parent_sn','module.serial_number','component_position','meta_data','user_edited.username', 'mtddb_synced']
    column_names = ['ID','Created By','Component Type', 'Serial Number', 'Parent Component', 'Module SN','Component Position','Meta Data','Edited by', 'MTDdb Synced']
    column_labels = dict(zip(column_list,column_names))
    can_delete = False
    can_create = False
    
@admin_required
class SecureEvent(ModelView):
    column_list = ['timestamp','category','severity','message']


