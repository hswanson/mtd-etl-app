from application.admin import routes as rts
from application import db
from .. import db_models as mdl

from application import admin_bp

admin_bp.add_view(rts.SecureUser(mdl.User, db.session))
#since add views make a blue print causes a naming collision with the assembly one, might add a custom view anyways to delete data
# admin_bp.add_view(SecureModelView(models.Assembly, db.session))
# admin_bp.add_view(SecureModelView(models.Test, db.session))
admin_bp.add_view(rts.SecureComponent(mdl.Component, db.session))

admin_bp.add_view(rts.SecureModule(mdl.Module, db.session))

admin_bp.add_view(rts.SecureCompType(mdl.ComponentType, db.session, category='Lookup Tables'))
admin_bp.add_view(rts.SecureModType(mdl.ModuleType, db.session, category='Lookup Tables'))

admin_bp.add_view(rts.SecureLookup(mdl.Location, db.session, category='Lookup Tables'))

admin_bp.add_view(rts.AdminExportLookups(name='Export Lookups'))

admin_bp.add_view(rts.SecureEvent(mdl.Event, db.session))
admin_bp.add_view(rts.MainPage(name='Return to Main Page'))
