from wtforms.validators import ValidationError
import magic
import os

def coerce_none(value):
    #used in selectfield to let a default value happen!
    if value == 'None':
        return None
    return value

class ValidateDB:
    def __init__(self,model,column,in_db=True,message=None,optional=False):
        #checks if value is in_db or not in database, either requiring it to be new or to already exist 
        self.model = model
        self.column = column
        self.in_db = in_db
        self.optional = optional
        if not message:
            if in_db:
                #we are validating it is IN the database if not, this is the message
                message = f'this {column} does not exist in the database, please register it'
            else:
                message = f'this {column} already exists in the database'
        self.message = message

    def __call__(self,form,field):
        obj = self.model.query.filter_by(**{self.column:field.data}).first()
        if obj is None and self.in_db and not self.optional:
            #if we are validating to see if it is in database but the obj is None then raise error, doesnt exist in db
            raise ValidationError(self.message)
        elif obj is not None and not self.in_db:
            #if we are validating to see if it is not already in the database but the obj exists then raise error, already exist in db
            raise ValidationError(self.message)
 

def validate_file(valid_extensions, multiple=False):
    #valid extensions is a list of the valid extensions ['.txt', '.jpg', etc...]
    def _validate_file(form, field):
        #for multiple file uploads for just one
        if field.data:
            if multiple==True:
                files = field.data 
            else:
                files = [field.data]

            for f in files:
                file_ext = os.path.splitext(f.filename)[-1].lower()
                magic_str = magic.from_buffer(f.read(2048), mime=True) #-> gives a str like "image/png", 2048 is the recommended read amount
                f.seek(0) #need to bring cursor back to the beginning otherwise it loses the content type of the file
                magic_type = magic_str.split('/')[-1] #keep in mind like text file gives text/plain so maybe you want first just note for later
                if magic_type == 'plain':
                    #magic_ext = '.' + 'txt'
                    magic_ext = file_ext #forget it for text files because csv can be seen as text file if just one row
                elif magic_type == 'jpeg':
                    magic_ext = '.' + 'jpg'
                elif magic_type == 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    magic_ext = '.' + 'xlsx'
                elif magic_type == 'vnd.ms-excel':
                    magic_ext = '.' + 'xls'
                else:
                    magic_ext = '.' + magic_type

                message = f'File must have a valid file extension: {valid_extensions} if it does, its mime type does not match the file extension (the mime type: {magic_ext})'
                if magic_ext != '.x-empty' and file_ext not in valid_extensions: 
                    raise ValidationError(message)
                elif magic_ext != '.x-empty' and file_ext != magic_ext:
                    #magic_ext != '.x-empty' this condition means no file was uploaded, or it has no content type so dont consider the magic_ext
                    raise ValidationError(message)

    return _validate_file
#-------------------------------------------------#

