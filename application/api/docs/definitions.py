from application import swagger
#https://github.com/flasgger/flasgger/blob/master/examples/definition_object_test.py#L13


@swagger.definition('Assembly')
class AssemblyDef(object):
    """
    Assembly defintion
    ---
    properties:
        id:
            type: integer
            description: The assembly's id
        user_created:
            type: string
            description: The username of the user who created the assembly
        type:
            type: string
            description: The type of assembly
        module:
            type: string
            description: The serial number of the Module, if it exists
        component:
            type: string
            description: The serial number of the Component, if it exists
        location:
            type: string
            description: The location where the assembly was done
        data:
            type: string
            description: Decoded data, if data exists
        files:
            type: array
            description: An array of file IDs associated with the assembly
            items:
            type: integer
        measurement_date:
            type: string
            description: The date of assembly
        created_at:
            type: string
            description: When the assembly was created

    """

@swagger.definition('Test')
class TestDef(object):
    """
    Test definition
    ---
    properties:
        id:
            type: integer
            description: The test's id
        user_created:
            type: string
            description: The username of the user who created the test
        type:
            type: string
            description: The type of test
        module:
            type: string
            description: The serial number of the Module, if it exists
        component:
            type: string
            description: The serial number of the Component, if it exists
        type:
            type: string
            description: The long name of the location where the test was performed
        data:
            type: string
            description: Decoded data, if it exists
        files:
            type: array
            description: An array of file IDs associated with the test
            items:
                type: integer
        measurement_date:
            type: string
            description: The date of the test
        created_at:
            type: string
            description: When the test was created
        
    """

@swagger.definition('Module')
class ModuleDef(object):
    """
    Module definition
    ---
    properties:
        id:
            type: integer
            description: The module's id
        user_created:
            type: string
            description: The username of who made the module
        serial_number:
            type: string
            description: The serial number of the Module
        module_type:
            type: string
            description: The type of module
        assembly_location:
            type: string
            description: The location where the module was assembled
        components:
            type: array
            description: An array of all the components that make up the module
            items:
                type: string
        current_location:
            type: string
            description: The current location of the module
        created_at:
            type: string
            description: When the module was created
        'data':
            type: object
            properties:
                'assemblies': 
                    type: array
                    description: An array of IDs of all the assemblies associated with the module
                    items:
                        type: integer
                'tests':
                    type: array
                    description: An array of IDs of all the tests associated with the module
                    items:
                        type: integer
                'observations':
                    type: array
                    description: An array of IDs of all the observations associated with the module
                    items:
                        type: integer
                'shipments':
                    type: array
                    description: An array of IDs of all the shipments the module was apart of
                    items:
                        type: integer

    """

@swagger.definition('Component')
class ComponentDef(object):
    """
    Component Definitioin
    ---
    properties:
        id:
            type: integer
            description: The components's id
        user_created:
            type: string
            description: The username of who made the component
        serial_number:
            type: string
            description: The serial number of the Component
        component_type:
            type: string
            description: The type of module
        associated_module:
            type: string
            description: Serial number of the module it is on
        parent:
            type: string
            description: Serial number of the parent component
        subcomponents:
            type: array
            description: An array of all the subcomponents that make up the component
            items:
                type: string
        component_type:
            type: string
            description: The position the component is in on the module, 4, 3, 2, 1, or 0
        current_location:
            type: string
            description: The current location of the component
        created_at:
            type: string
            description: When the component was created
        metadata:
            type: object
            description: Keys are strings (typically) and specify some meta data like geometry of component, and values are strings or integers
            items: 
                type: string
        vendor:
            type: string
            description: The vendor that the component was obtained from
        'data':
            type: object
            properties:
                'assemblies': 
                    type: array
                    description: An array of IDs of all the assemblies associated with the component
                    items:
                        type: integer
                'tests':
                    type: array
                    description: An array of IDs of all the tests associated with the component
                    items:
                        type: integer
                'observations':
                    type: array
                    description: An array of IDs of all the observations associated with the component
                    items:
                        type: integer
                'shipments':
                    type: array
                    description: An array of IDs of all the shipments the component was apart of
                    items:
                        type: integer

    """

@swagger.definition('Observation')
class ObservationDef(object):
    """
    properties:
        id:
            type: integer
            description: The observation's id
        user_created:
            type: string
            description: The username of the user who created the observation
        module:
            type: string
            description: The serial number of the Module, if it exists
        component:
            type: string
            description: The serial number of the Component, if it exists
        observation_location:
            type: string
            description: The long name of the location where the observation was made
        comment:
            type: string
            description: The comment associated with the observation
        files:
            type: array
            description: An array of file IDs associated with the observation
            items:
                type: integer
        created_at:
            type: string
            description: When the observation was created

    """


@swagger.definition('User')
class UserDef(object):
    """
    User Definition
    ---
    properties:
        id:
            type: integer
            description: The user's id
        username:
            type: string
            description: The user's username
        email:
            type: string
            description: The user's email
        site_name:
            type: string
            description: The user's site name
        is_admin:
            type: boolean
            description: Whether the user is an admin or not
        is_active:
            type: boolean
            description: Whether the user account is active or not
        has_logged_in:
            type: boolean
            description: Whether the user has logged into the website before or not
        user_created:
            type: string
            description: The username of the user who created this user

    """


@swagger.definition('UserParams')
class UserParameters(object):
    """
    User Parameters
    ---  
    properties:
        username:
            type: string
            description: username of the user to update
        password:
            type: string
            description: password of the user to update
        email:
            type: string
            description: email of the user to update
        site_name:
            type: string
            description: name of the site the user works at to update
        is_admin:
            type: boolean
            description: choose to grant or revoke admin priviledges
        is_active:
            type: boolean
            description: choose to grant or revoke access to a users account
            
    """
