from application.api import api_bp #get flask app
from flask import request, jsonify
from application import db
from application import db_models as mdl
from application.api.auth import token_auth
from application.tables import search as DBSearch
from sqlalchemy import func, literal_column
from sqlalchemy.exc import DataError
from pydantic import PositiveInt, TypeAdapter


MAX_RESULT_LIMIT = 1000

@api_bp.route('/search/<string:table>', methods=['GET'])
@token_auth.login_required
def search(table: str):
    """
    curl -X GET http://127.0.0.1:5000/api/search/assembly?search_string="ID>20,comptype=Production ETROC" -H "Authorization: Bearer <token>"
    """
    if table and table.endswith('s'):
        table = table[:-1]
    if table not in DBSearch.builder_map:
        return jsonify({"error": f"{table} table is not a valid table for search"}), 400

    limit_raw = request.args.get('limit', MAX_RESULT_LIMIT)
    limit = TypeAdapter(PositiveInt).validate_python(limit_raw)
    if limit > MAX_RESULT_LIMIT:
        return jsonify({"error": f"{MAX_RESULT_LIMIT} is the maximum result limit"}), 400

    search_string = request.args.get('search_string', None)
    if not search_string:
        return jsonify({"error": "To search the database, the query string parameter 'search_string' is required. EX: curl -X GET http://localhost:5000/api/search/components?search_string='ID>4'"}), 400

    query_builder = DBSearch.builder_map[table]()
    Table = query_builder.db_model
    query_director = DBSearch.QueryDirector(search_string)
    query_director.builder = query_builder

    if not any(search_key.lower() in DBSearch.DEPRECATION_QUERY_ALIASES for search_key, _ in query_director.grouped_searches):
        query_builder.query_deprecation(['False'], '=')
    query = query_director.build_query()    
    
    # Cant do this pagination because of offset and limit dont work on the big tables!
    # could just do it for the parts and not the test tables?
    #https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xxiii-application-programming-interfaces-apis

    try:
        ONE = literal_column("1")
        total_rows = db.session.execute(
            db.select(func.count(ONE)).select_from(query.subquery()).order_by(None)
        ).scalar_one()
        search_results = db.session.scalars(query.order_by(Table.id).limit(limit)).all()
    except DataError:
        total_rows = 0
        search_results = []

    return jsonify({
        'results_ids': [r.serial_number if hasattr(r, 'serial_number') else r.id for r in search_results],
        'limit': limit,
        'total_results': total_rows,
        'table': table
    })

@api_bp.route('/search/<string:tablename>/<string:identifier>', methods=['GET'])
@token_auth.login_required
def get_table_row(tablename, identifier):
    if tablename and tablename.endswith('s'):
        tablename = tablename[:-1]
    table =  mdl.get_model_by_tablename(tablename)
    row = None
    if identifier.isnumeric():
        row = table.get_by_id(int(identifier))
    elif table in [mdl.Module, mdl.Component]:
        row = table.get_by_sn(identifier)

    if row is not None:
        return row.to_dict()
    else:
        f"Could not find row in database for ({identifier=}) in ({tablename=}) does not exist in the database.", 404   