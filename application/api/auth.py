from application.api import api_bp #get flask app
from application.api import errors
from application import the_config
from application import db_models as mdl
from application import datetime_handler
import jwt
from datetime import timedelta
from flask_httpauth import HTTPTokenAuth

#basic auth to sign in, then you can get a token to go around the api
token_auth = HTTPTokenAuth()

# FOR A FULL OATH EXAMPLE WITH DATABASE: https://github.com/authlib/example-oidc-server/tree/master

@token_auth.verify_token #decorate endpoints with token_auth.login_required and this will run
def verify_token(token):
    try:
        users_payload = jwt.decode(token, the_config.API_SECRET_KEY, algorithms=['HS256'])
        if 'user_id' in users_payload and users_payload['user_id'] is not None:
            user = mdl.User.get_by_id(users_payload['user_id'])
            if user is None or not user.is_active:
                return None
            return user    
    except jwt.exceptions.InvalidTokenError: #has to return None for the thing to work
        return None

@token_auth.error_handler
def token_auth_error(status):
    return errors.error_response(status, message=f"Unauthorized due to invalid crendentials OR your Token expired, try again. TOKENS LAST {the_config.TOKEN_EXPIRATION} seconds")

@api_bp.route('/refresh-token/', methods=['GET'])
@token_auth.login_required
def refresh_token():
    """
    curl -X GET http://127.0.0.1:5000/api/refresh-token/ -H "Authorization: Bearer <token>"
    """
    token_expiration_date = datetime_handler.now_utc() + timedelta(seconds=the_config.TOKEN_EXPIRATION)
    token = jwt.encode({
        'user_id': token_auth.current_user().id,
        'exp': token_expiration_date
    }, the_config.API_SECRET_KEY, algorithm='HS256')
    return {
        "token":token,
        "expires": str(token_expiration_date),
        "duration_seconds": the_config.TOKEN_EXPIRATION
        }