from werkzeug.http import HTTP_STATUS_CODES #dictionary from werkzeug

from werkzeug.exceptions import HTTPException
from application.api import api_bp #get flask app

def error_response(status_code, message=None):
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    return payload, status_code

def bad_request(message):
    #This is the error that is used when the client sends a request that has invalid data in it.
    return error_response(400, message)

def failed_auth(message):
    return error_response(401, message)

@api_bp.errorhandler(HTTPException)
def handle_exception(e):
    return error_response(e.code, message=e.description)