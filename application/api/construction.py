from application.api import api_bp #get flask app
from flask import request, url_for, abort, jsonify
from application import db
from application import db_models as mdl
from application.api.auth import token_auth

@api_bp.route('/upload/<string:tablename>', methods=['POST'])
@token_auth.login_required
def api_upload_construction(tablename):
    """
    curl -X POST http://127.0.0.1:5000/api/upload/assembly -H "Content-Type: application/json" -H "Authorization: Bearer <token>" -d '{"module": "DIBU0001","component": "HAYD01","component_pos": 4,"version": "0.0","type": "pick and place survey precure","measurement_date": "2023-01-01T12:00:00+01:00","location": "BU","user_created": "local_user","data": {"target": [639.141118, 287.244992, 64.009534, -0.048954],"actual": [639.141118, 287.244992, 64.009534, -0.048954],"delta": [-8, 3, 0.1, -0.048954]}}'
    """
    from application.construction.upload import ConstrUploader
    if tablename and tablename.endswith('s'):
        tablename = tablename[:-1]
    table =  mdl.get_model_by_tablename(tablename)    
    if table not in [mdl.Assembly, mdl.Test]:
        return f"{table} table is not a valid table for construction upload", 400
    constrs = request.json
    if isinstance(constrs, dict):
        constrs = [request.json]
    construction_steps = ConstrUploader(constrs)
    db_steps = construction_steps.add_all_to_db()
    db.session.commit()
    return jsonify({
        "uploaded_ids": [dbs.id for dbs in db_steps],
        "table": tablename
    })