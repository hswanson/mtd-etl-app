from datetime import datetime, timezone, date
import pytz
from typing import Union
#------------Shared Functions---------------#
def now_utc() -> datetime:
    return datetime.now(timezone.utc)

def convert_date_to_datetime(input_date: date, tz_info=pytz.utc):
    """
    Coverts the datetime to the min time (00:00) in the timezone, default is UTC
    """
    #Note, shipment.shipment_date -> datetime.datetime and form.reception_date.data -> datetime.date
    if input_date:
        return datetime.combine(input_date, datetime.min.time(), tz_info)
    else:
        return None

def validate_datetime(meas_date: Union[date, datetime]) -> datetime:
    if type(meas_date) is date: #cannot do isinstance to differentiate between datetime and date objects!
        return convert_date_to_datetime(meas_date)
    #if measurement date has tz information, convert from that TZ to UTC time!
    if not type(meas_date) is datetime:
        raise ValueError(f"Inputted date is not a datetime object it is {type(meas_date)}")
    #is a datetime object...
    if meas_date.tzinfo is None:
        #need to do this otherwise it has a default for no timezone given!
        raise ValueError("Measurement data has no time zone information, see https://en.wikipedia.org/wiki/ISO_8601")

    #converts time to UTC time
    return meas_date.astimezone(pytz.utc)

