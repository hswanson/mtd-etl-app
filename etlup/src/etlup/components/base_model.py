from typing import Any, Optional
from pydantic import BaseModel, Field, AliasChoices, ConfigDict, field_validator, model_validator

class ComponentBase(BaseModel):
    model_config = ConfigDict(
        extra="allow",  # overwrite this for inherited models
    )

    serial_number: str = Field(
        ...,
        validation_alias=AliasChoices("SerialNumber", "Serial Number", "SN"), 
        min_length=3, 
        coerce_numbers_to_str=True # This allows for a better error message of serial numbers that are just numbers
    )
    component_type: str = Field(..., validation_alias=AliasChoices("ComponentType", "Component Type", "Type"))
    vendor: str = Field(..., alias="Vendor")
    etroc_serial_number: Optional[str] = Field(None, validation_alias=AliasChoices("ETROC Serial Number", "ETROCSerialNumber", "ETROC SN"))
    lgad_serial_number:  Optional[str] = Field(None, validation_alias=AliasChoices("LGAD Serial Number", "LGADSerialNumber", "LGAD SN"))

    @field_validator('*', mode='before')
    def empty_str_to_none(cls, v):
        """
        To make it more user friendly and catch unsupplied serial numbers
        """
        if v == '':
            return None
        return v

    @model_validator(mode='after')
    def check_subassembly_registration(self):
        etroc_given = self.etroc_serial_number is not None
        lgad_given = self.lgad_serial_number is not None
        if etroc_given ^ lgad_given: #XOR logic is want, returns true when just one is given!
            raise ValueError("For subassembly component registration you need both etroc and lgad serial number fields.")
        return self
    
    @field_validator('serial_number')
    @classmethod
    def valid_serial_number(cls, v: str) -> str:
        if isinstance(v, str):
            # is enumerated
            if not v[-1].isdigit():
                raise ValueError(
                    f"The serial number '{v}' is not correctly formatted, serial numbers should be enumerated. Ensure the serial number ends with a numerical sequence, such as SN1, SN2, etc")
            # is a number
            if v.isdigit():
                raise ValueError(
                    f"Serial numbers cannot consist solely of numbers. Please include descriptive letters to make them more readable, such as 'MY-SN-123' instead of just '123'.")
            # needs a letter
            if not any(s.isalpha() for s in v):
                raise ValueError(
                    f"Serial numbers must include alphabetic characters to enhance readability. Please incorporate atleast one descriptive alphabetic character (A-Z)")    
        return v
