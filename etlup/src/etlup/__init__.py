# SPDX-FileCopyrightText: 2024-present Hayden Swanson <hayden_swanson22@yahoo.com>
#
# SPDX-License-Identifier: MIT
from jinja2 import Environment, PackageLoader, select_autoescape

from matplotlib import use as pltuse
pltuse("agg")


jinja_env = Environment(
    loader=PackageLoader(__name__),
    autoescape=select_autoescape()
)

try:
    from etlup.upload import ETLup, get_model
except ModuleNotFoundError:
    from etlup.src.etlup.upload import ETLup, get_model








