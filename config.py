from dataclasses import dataclass, field
from typing import Optional
from os import environ
from os.path import dirname, split, join
from typing import Literal


root_dir = dirname(__file__)
app_dir = join(root_dir,'application')

@dataclass(init=False)
class Config:
    SECRET_KEY: str = 'bite my shiny metal ass'
    SQLALCHEMY_DATABASE_URI: str = 'sqlite:///'+join(root_dir, 'tempdb.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False

    CLIENT_ID: str = 'the id'
    CLIENT_SECRET: str = ' womp womp'
    CERN_SSO: str = 'https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration'
    DISABLE_CERN_SSO = False

    SESSION_COOKIE_SECURE = True
    REMEMBER_COOKIE_SECURE = True
    SESSION_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_HTTPONLY = True
    
    APP_DEVELOPING = False #normally set to false but if you are developing you can turn it on so you do not get emails for every error

    #Config File Restrictions
    MAX_CONTENT_LENGTH = 1024 * 1024 * 30 #30MB max file upload
    UPLOAD_IMAGE_EXTENSIONS = ['.jpg', '.png']
    GANTRY_LOG_EXTENSIONS = ['.txt']
    COMPONENT_REGISTRATION_EXTENSIONS = ['.csv'] #later should support excel file extensions :)

    FILE_STORE = 'CLOUD' # DB, LOCAL, CLOUD
    FILE_STORE_NAME: Literal['PROD', 'STAGING', 'DEV'] = 'DEV'
    LOCAL_UPLOAD_PATH = 'file_uploads' #one massive folder with everything and unique filename via hashing

    WEBDAV_HOSTNAME = 'https://cernbox.cern.ch/cernbox/webdav/eos/project/e/etl-construction-app-storage/'
    WEBDAV_DIRECTORY = 'public/'
    WEBDAV_LOGIN = 'login'
    WEBDAV_PASSWORD = 'password'

    #service email for sending out shipping notifications and that sort of thing
    EMAIL_ADDRESS = '' #default empty string for developing purposes so it doesnt send things out
    EMAIL_APP_PASSWORD = ''

    ADMIN_EMAIL_ADDRESS = 'hayden.swanson@cern.ch'

    MTDDB_SSH_HOST = 'dbloader-mtd.cern.ch'
    MTDDB_SSH_USERNAME = 'hswanson'
    MTDDB_SSH_PORT = 8113
    MTDDB_SSH_PRIVATE_KEY = 'you thought'

    #api set up
    TOKEN_EXPIRATION = 8*24*60*60 #seconds     #8 weeks
    API_SECRET_KEY = 'super secret key'

    #LOGGING_MODE: Literal['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'] = 'CRITICAL'
    PROFILE_SQL = False
    
    #321200 is for etl 
    MTD_BARCODE_PREFIX = '32120' #after this is: ZZ XXXXXXX
    @property
    def keys(self):
        #get all keys from this config, do uppercase ones which are the environment variable keys
        keys = []
        for k in dir(self):
            if k[0].isupper():
                keys.append(k)
        return keys

    def __init__(self):
        for k in self.keys:
            #looks for environment variables in:
            #1. env.py, defined personal secrets in env.py file
            #2. environement, defined in the environement it is ran in here
            #3. here, the default, defined here in Config class
            default = getattr(self, k)
            try:
                import env_personal
                val = getattr(env_personal, k)
            except (ImportError, AttributeError):
                val = environ.get(k, default)

            if type(default) == bool:
                if str(val).lower() == "true":
                    val = True
                elif str(val).lower() == "false":
                    val = False
                else:
                    raise ValueError(f"Boolean config \"{k}\" must be True or False, found \"{val}\"")

            else:
                val = type(default)(val)

            setattr(self, k, val)


the_config = Config()

# in another file
# from config import the_config
